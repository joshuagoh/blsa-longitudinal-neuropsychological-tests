option nofmterr; run; * Switch off format errors ;

/* Script combined and modified from blsa_merge_data.sas, blsa_icc.sas, and blsa_mixedmodels.sas by Josh Goh 01032011.

Processes and Sample:
Merge - BLSA, NI
ICC - NI
Mixed Model - NI

*/

/* CREATE DATABASES */

* Setup LEFS library in file system (from blsa_merge_data.sas);
libname lefslib 'Z:\Research\Projects\BLSA\Battery\Data\lefslib';

* COMPILE VARIABLES;
* Trails Making;
data lefslib.trails; set coglib.neutrail;
trba = trbts - trats;
netrats = -trats; netrbts = -trbts; netrba = -trba; * Flip sign so negative is poor performance;
keep id vi NETRATS NETRBTS NETRBA; run;
data lefslib.trails (RENAME = (netrats=trats netrbts=trbts netrba=trba));
set lefslib.trails;
proc sort data = lefslib.trails; by id vi; run;

* Alpha Span;
data lefslib.alphas; set coglib.neuother;
keep id vi alphas; run;
proc sort data = lefslib.alphas; by id vi; run;

* Similarities;
data lefslib.sim; set coglib.sim;
keep id vi simtot; run;
proc sort data = lefslib.sim; by id vi; run;

* CVLT;
data lefslib.cvlt; set coglib.cvl;
p_cvlhit = cvlhit/16;
if cvlhit = 16 then p_cvlhit = 1 - 1/32;
zcvlhit = probit(p_cvlhit);
p_cvlxfp = cvlxfp/28;
if cvlxfp = 0 then p_cvlxfp = 1/56;
zcvlxfp = probit(p_cvlxfp);
cvldis = zcvlhit - zcvlxfp;
cvlrbi = -0.5 * (zcvlhit + zcvlxfp);
necvltpe = -cvltpe; necvltif = -cvltif; necvltic = -cvltic; necvlxfp = -cvlxfp; necvlsv5 = -cvlsv5; necvllv5 = -cvllv5; necvlsvs = -cvlsvs; necvllvl = -cvllvl; * Flip sign so negative is poor performance;
keep id vi dot age ageyrs cvltca cvlca1	cvlca2	cvlca3	cvlca4	cvlca5	cvltcb	cvlfrs	cvlcrs	cvlfrl	cvlcrl	cvlrs1	cvlrs2	cvlrs3	cvlrs4	cvlrs5	cvlrst	cvlrx1	cvlrx2	cvlrx3	cvlrx4	cvlrx5	cvlrxt	cvlbet	necvltpe necvltif necvltic cvlhit necvlxfp cvldis cvlrbi necvlsv5 necvllv5 necvlsvs cvllvs necvllvl;
drop cvlhit cvlxfp necvlxfp;
run;
data lefslib.cvlt (RENAME = (necvltpe=cvltpe necvltif=cvltif necvltic=cvltic necvlsv5=cvlsv5 necvllv5=cvllv5 necvlsvs=cvlsvs necvllvl=cvllvl));
set lefslib.cvlt;
proc sort data = lefslib.cvlt; by id vi; run;

* Verbal Fluency, Script modified by Josh Goh 30 Nov 2010 - Added fluctw flultw flucti fluctp flulti flultp;
data lefslib.vfluency; set coglib.neuflu;
fluctw = flucfw + flucaw + flucvw;
flultw = flulfw + flulaw + flulsw;
flucti = flucfi + flucai + flucvi;
fluctp = flucfp + flucap + flucvp;
flulti = flulfi + flulai + flulsi;
flultp = flulfp + flulap + flulsp;
neflucti = -flucti; nefluctp = -fluctp; neflulti = -flulti; neflultp = -flultp; neflucfi = -flucfi; neflucai = -flucai; neflucvi = flucvi; neflucfp = -flucfp; neflucap = -flucap; neflucvp = -flucvp; neflulfi = -flulfi; neflulai = -flulai; neflulsi = -flulsi; neflulfp = -flulfp; neflulap = -flulap; neflulsp = -flulsp; * Flip sign so negative is poor performance;
keep id vi flucat flulet fluctw flultw neflucti nefluctp neflulti neflultp flucfw neflucfp neflucfi flucaw neflucap neflucai flucvw neflucvp neflucvi flulfw neflulfp neflulfi flulaw neflulap neflulai flulsw neflulsp neflulsi; run;
data lefslib.vfluency (RENAME = (neflucti=flucti nefluctp=fluctp neflulti=flulti neflultp=flultp neflucfp=flucfp neflucfi=flucfi neflucap=flucap neflucai=flucai neflucvp=flucvp neflucvi=flucvi neflulfp=flulfp neflulfi=flulfi neflulap=flulap neflulai=flulai neflulsp=flulsp neflulsi=flulsi));
set lefslib.vfluency;
proc sort data = lefslib.vfluency; by id vi; run;

* PMA;
data lefslib.pma; set coglib.pmavoc;
keep id vi dov pmato3; run; * switched back from pmatat to pmato3 20 Oct 2010;
proc sort data = lefslib.pma; by id vi; run;

* Stroop (NOTE: dropped from LEFS, see below);
data lefslib.stroop; set coglib.neustr; run; * keep all;
proc sort data = lefslib.stroop; by id vi; run;

* Digit Span;
data lefslib.dspan; set coglib.dispan;
keep id vi dov age ageyrs digfor digbac; run;
proc sort data = lefslib.dspan; by id vi; run;

* Calculations (NOTE: dropped from LEFS, see below);
data lefslib.calc; set coglib.neucal; run; * keep all;
proc sort data = lefslib.calc; by id vi; run;

* Card Rotations;
data lefslib.crdrot; set coglib.crdrot;
keep id vi dov crdrot; run;
proc sort data = lefslib.crdrot; by id vi; run;

* Boston Naming;
data lefslib.bnt; set coglib.neudat;
keep id vi boscor; run;
proc sort data = lefslib.bnt; by id vi; run;

* BVRT;
data lefslib.bvrt; set coglib.bvr;
nebvrtot = -bvrtot; * Flip sign so negative is poor performance;
keep id vi dot age ageyrs nebvrtot; run;
data lefslib.bvrt (RENAME = (nebvrtot=bvrtot));
set lefslib.bvrt;
proc sort data = lefslib.bvrt; by id vi; run;

* CSR;
* - included in script 3 Oct 2010, also updated merge and reorder below;
* - excluded from LEFS 20 Oct 2010 - tests not entirely relevant;
data lefslib.csr; set coglib.neucsr;
keep id vi csrdec; run;
proc sort data = lefslib.csr; by id vi; run;

* Still searching for MIS, Digit Symbol data;

* Dementia Diagnosis;
* - included in script 25 Oct 2010;
* - NOTE: no vi variable;
* - Changed database from coglib to dement.;
data lefslib.dementia; set dement.dement2010;
keep id donset dodx dx; run;
proc sort data = lefslib.dementia; by id; run;

* Daily Function (Pfeffer);
* - included in script 25 Oct 2010;
data lefslib.pfeffer; set coglib.neudat;
keep id vi pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10; run;
proc sort data = lefslib.pfeffer; by id vi; run;

* CREATE MAIN DATABASE;
proc sort data = emslib.emsvis; by id vi; run;

data lefslib.main; * CREATE MAIN DATABASE;
merge emslib.emsvis lefslib.trails lefslib.alphas lefslib.sim lefslib.cvlt lefslib.vfluency lefslib.pma lefslib.dspan lefslib.crdrot lefslib.bnt lefslib.bvrt lefslib.pfeffer;
by id vi;

* Sex - includeded in script 1 Nov 2010;
if id < 5000 then sex = 1;
if id >= 5000 then sex = 0;

run;

* CREATE BASELINE AGE VARIABLE;
data lefslib.allbaseline; set lefslib.main; 
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.allbaseline; by id; run;

* Merge dementia onsets modified 14 Dec 2010;
data lefslib.main;
merge lefslib.main lefslib.dementia lefslib.allbaseline;
by id;
interval = age - baseage;
cbaseage = baseage - 17.199218; * Center baseline age to minimum;
run;
proc sort data = lefslib.main; by id vi; run;
proc sort data = emslib.emsvis; by id vi; run;

* Reorder lefslib.main to make more sense;
data lefslib.main;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10; set lefslib.main;
run;

* Export lefslib.main as .csv;
PROC EXPORT data = lefslib.main 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\main.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE ALLDATA DATABASE (MAIN + BASELINE AGE AND INTERVAL);
* Create Baseline Age and Interval variables for main and create allmain. - Added Josh Goh 1 Mar 2011;
data lefslib.allbaseline; set lefslib.main; * CREATE ALLDATA DATABASE;
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.allbaseline; by id; run;
data lefslib.alldata;
merge lefslib.main lefslib.allbaseline;
by id;
interval = age - baseage;
cbaseage = baseage - 17.199218; * Center baseline age to minimum;

* Buffer dummy values to equate to nimain1;
EMSid = id;
emsfn = id;
emsvi = vi;
EMSwave = vi;
run;

* Reorder lefslib.alldata to make more sense;
data lefslib.alldata;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10;
set lefslib.alldata;
run;

* Export lefslib.alldata as .csv;
PROC EXPORT data = lefslib.alldata 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\alldata.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE REDALLDATA DATABASE (REDUCED FROM ALLDATA);
* Reduce tests;
data lefslib.redalldata; set lefslib.alldata; * CREATE REDALLDATA DATABASE;
yr = year(DOV);
keep id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot donset yr;
format alphas trats trba digfor digbac boscor bvrtot;
run;

* CREATE REDNDALLDATA DATABASE (DEMENTIA ONSET DATA REMOVED FROM REDALLDATA);
data lefslib.redndalldata; set lefslib.redalldata; * CREATE REDNDALLDATA DATABASE;
if yr < donset | donset = .;
run;

*CREATE ZREDALLDATA DATA (NORMALIZED REDALLDATA);
PROC STANDARD DATA=lefslib.redalldata MEAN=0 STD=1 OUT=lefslib.zredalldata; * CREATE ZREDALLDATA DATABASE;
	VAR simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot;
RUN;

* Export lefslib.zredalldata as .csv;
PROC EXPORT data = lefslib.zredalldata 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredalldata.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE COMPZREDALLDATA DATABASE (COMPOSITE SCORES FROM ZREDALLDATA);
* Composite scores from lefslib.zredalldata (Averaged: equal weighting from each sub-test);
data lefslib.compzredalldata; set lefslib.zredalldata; * CREATE COMPZREDALLDATA DATABASE;

abstr = simtot;
cap   = digfor;
chunk = cvlrst;
disc  = (cvlsv5 + cvldis)/2;
inhib = (fluctp + flultp)/2;
manip = (alphas + digbac)/2;
pretr = flulet;
sretr = (flucat + boscor)/2;
swtch = trba;
ltm   = cvlfrl;
stm   = (cvlfrs + cvltca)/2;
spd   = trats;
vocab = pmato3;
vis   = (bvrtot + crdrot)/2;

label 	abstr = 'abstraction'
		cap   = 'capacity'
		chunk = 'chunking'
		disc  = 'discrimination'
		inhib = 'inhibition'
		manip = 'manipulation'
		pretr = 'phonemic retrieval'
		sretr = 'semantic retrieval'
		swtch = 'switching'
		ltm   = 'long-term memory'				
		stm   = 'short-term memory'		
		spd   = 'speed'
		vocab = 'vocabulary'
		vis   = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot yr;
run;

data lefslib.compzredalldata;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set lefslib.compzredalldata;
run;

* Export lefslib.compzredalldata as .csv;
PROC EXPORT data = lefslib.compzredalldata 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredalldata.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE ZREDNDALLDATA DATABASE (NORMALIZED REDNDALLDATA);
PROC STANDARD DATA=lefslib.redndalldata MEAN=0 STD=1 OUT=lefslib.zredndalldata; * CREATE ZREDNDALLDATA DATABASE;
	VAR simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot;
RUN;

* Export lefslib.zredndalldata as .csv;
PROC EXPORT data = lefslib.zredndalldata 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndalldata.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE COMPZREDNDALLDATA DATABASE (COMPOSITE SCORES FROM ZREDNDALLDATA);
* Composite scores from lefslib.zredndalldata (Averaged: equal weighting from each sub-test);
data lefslib.compzredndalldata; set lefslib.zredndalldata; * CREATE COMPZREDNDALLDATA DATABASE;

abstr = simtot;
cap   = digfor;
chunk = cvlrst;
disc  = (cvlsv5 + cvldis)/2;
inhib = (fluctp + flultp)/2;
manip = (alphas + digbac)/2;
pretr = flulet;
sretr = (flucat + boscor)/2;
swtch = trba;
ltm   = cvlfrl;
stm   = (cvlfrs + cvltca)/2;
spd   = trats;
vocab = pmato3;
vis   = (bvrtot + crdrot)/2;

label 	abstr = 'abstraction'
		cap   = 'capacity'
		chunk = 'chunking'
		disc  = 'discrimination'
		inhib = 'inhibition'
		manip = 'manipulation'
		pretr = 'phonemic retrieval'
		sretr = 'semantic retrieval'
		swtch = 'switching'
		ltm   = 'long-term memory'				
		stm   = 'short-term memory'		
		spd   = 'speed'
		vocab = 'vocabulary'
		vis   = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot yr;
run;

data lefslib.compzredndalldata;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set lefslib.compzredndalldata;
run;

* Export lefslib.compzredndalldata as .csv;
PROC EXPORT data = lefslib.compzredndalldata 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndalldata.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* CREATE NIMAIN1 DATABASE (MAIN + BASELINE AGE AND INTERVAL, NI SUBSET);
* Select NI subset from lefslib.main into lefslib.nimain1 (updated from nimain - added age criterion);
data lefslib.nimain1; * CREATE NIMAIN1 DATABASE;
merge emslib.emsvis lefslib.main;
by id vi;
if emsvi ~= . and dot ~= .; * select only cognitive data for scan sessions;
if age >= 55; * select based on age;
*if inemvis = 1 and inmain = 1; * select only cognitive data for scan sessions;
drop psorder blsayr protocol fhxdone pettask status; * remove unnecessary info from nimain;
run;

* Create Baseline Age and Interval variables for nimain1. - Added Josh Goh 1 Mar 2011;
data lefslib.nimain1baseline; set lefslib.nimain1;
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.nimain1baseline; by id; run;
data lefslib.nimain1;
merge lefslib.nimain1 lefslib.nimain1baseline;
by id;
interval = age - baseage;
cbaseage = baseage - 55; * Center baseline age to 55;
run;

* Reorder lefslib.nimain1 to make more sense;
data lefslib.nimain1;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10;
set lefslib.nimain1;
run;

* Export lefslib.nimain1 as .csv;
PROC EXPORT data = lefslib.nimain1 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

/* REDUCE DATABASES */
* CREATE REDNIMAIN1 DATABASE (REDUCED FROM NIMAIN1);
* Reduce tests;
data lefslib.rednimain1; set lefslib.nimain1; * CREATE REDNIMAIN1 DATABASE;
yr = year(DOV);
keep id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot donset yr;
format alphas trats trba digfor digbac boscor bvrtot;
run;

* CREATE REDNDNIMIAIN1 DATABASE (DEMENTIA ONSET DATA REMOVED FROM REDNIMAIN1);
data lefslib.redndnimain1; set lefslib.rednimain1; * CREATE REDNDNIMAIN1 DATABASE;
if yr < donset | donset = .;
run;

*CREATE ZREDNIMAIN1 DATA (NORMALIZED REDNIMAIN1);
PROC STANDARD DATA=lefslib.rednimain1 MEAN=0 STD=1 OUT=lefslib.zrednimain1; * CREATE ZREDNIMAIN1 DATABASE;
	VAR simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot;
RUN;

* Export lefslib.zrednimain1 as .csv;
PROC EXPORT data = lefslib.zrednimain1 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zrednimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE COMPZREDNIMAIN1 DATABASE (COMPOSITE SCORES FROM ZREDNIMAIN1);
* Composite scores from lefslib.zrednimain1 (Averaged: equal weighting from each sub-test);
data lefslib.compzrednimain1; set lefslib.zrednimain1; * CREATE COMPZREDNIMAIN1 DATABASE;

abstr = simtot;
cap   = digfor;
chunk = cvlrst;
disc  = (cvlsv5 + cvldis)/2;
inhib = (fluctp + flultp)/2;
manip = (alphas + digbac)/2;
pretr = flulet;
sretr = (flucat + boscor)/2;
swtch = trba;
ltm   = cvlfrl;
stm   = (cvlfrs + cvltca)/2;
spd   = trats;
vocab = pmato3;
vis   = (bvrtot + crdrot)/2;

label 	abstr = 'abstraction'
		cap   = 'capacity'
		chunk = 'chunking'
		disc  = 'discrimination'
		inhib = 'inhibition'
		manip = 'manipulation'
		pretr = 'phonemic retrieval'
		sretr = 'semantic retrieval'
		swtch = 'switching'
		ltm   = 'long-term memory'				
		stm   = 'short-term memory'		
		spd   = 'speed'
		vocab = 'vocabulary'
		vis   = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot yr;
run;

data lefslib.compzrednimain1;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set lefslib.compzrednimain1;
run;

* Export lefslib.compzrednimain1 as .csv;
PROC EXPORT data = lefslib.compzrednimain1 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzrednimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE ZREDNDNIMAIN1 DATABASE (NORMALIZED REDNDNIMAIN1);
PROC STANDARD DATA=lefslib.redndnimain1 MEAN=0 STD=1 OUT=lefslib.zredndnimain1; * CREATE ZREDNDNIMAIN1 DATABASE;
	VAR simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot;
RUN;

* Export lefslib.zredndnimain1 as .csv;
PROC EXPORT data = lefslib.zredndnimain1 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* CREATE COMPZREDNDNIMAIN1 DATABASE (COMPOSITE SCORES FROM ZREDNDNIMAIN1);
* Composite scores from lefslib.zredndnimain1 (Averaged: equal weighting from each sub-test);
data lefslib.compzredndnimain1; set lefslib.zredndnimain1; * CREATE COMPZREDNDNIMAIN1 DATABASE;

abstr = simtot;
cap   = digfor;
chunk = cvlrst;
disc  = (cvlsv5 + cvldis)/2;
inhib = (fluctp + flultp)/2;
manip = (alphas + digbac)/2;
pretr = flulet;
sretr = (flucat + boscor)/2;
swtch = trba;
ltm   = cvlfrl;
stm   = (cvlfrs + cvltca)/2;
spd   = trats;
vocab = pmato3;
vis   = (bvrtot + crdrot)/2;

label 	abstr = 'abstraction'
		cap   = 'capacity'
		chunk = 'chunking'
		disc  = 'discrimination'
		inhib = 'inhibition'
		manip = 'manipulation'
		pretr = 'phonemic retrieval'
		sretr = 'semantic retrieval'
		swtch = 'switching'
		ltm   = 'long-term memory'				
		stm   = 'short-term memory'		
		spd   = 'speed'
		vocab = 'vocabulary'
		vis   = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot yr;
run;

data lefslib.compzredndnimain1;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set lefslib.compzredndnimain1;
run;

* Export lefslib.compzredndnimain1 as .csv;
PROC EXPORT data = lefslib.compzredndnimain1 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

/* ICC */

* Setup libraries in file system;
libname lefsest 'Z:\Research\Projects\BLSA\Battery\Data\lefsest';

* NIMAIN1;
%macro iccnimain1(varnames); * OPERATE ON NIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.nimain1ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.nimain1ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.nimain1ICCcov&dv;
		PROC SORT data = lefsest.nimain1ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%iccnimain1(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);
DATA lefslib.nimain1ICCest;
MERGE lefsest.nimain1ICCcovtrats lefsest.nimain1ICCcovtrbts lefsest.nimain1ICCcovtrba lefsest.nimain1ICCcovalphas lefsest.nimain1ICCcovsimtot lefsest.nimain1ICCcovcvltca lefsest.nimain1ICCcovcvlca1 lefsest.nimain1ICCcovcvlca2 lefsest.nimain1ICCcovcvlca3 lefsest.nimain1ICCcovcvlca4 lefsest.nimain1ICCcovcvlca5 lefsest.nimain1ICCcovcvltcb lefsest.nimain1ICCcovcvlfrs lefsest.nimain1ICCcovcvlcrs lefsest.nimain1ICCcovcvlfrl lefsest.nimain1ICCcovcvlcrl lefsest.nimain1ICCcovcvlrs1 lefsest.nimain1ICCcovcvlrs2 lefsest.nimain1ICCcovcvlrs3 lefsest.nimain1ICCcovcvlrs4 lefsest.nimain1ICCcovcvlrs5 lefsest.nimain1ICCcovcvlrst lefsest.nimain1ICCcovcvlrx1 lefsest.nimain1ICCcovcvlrx2 lefsest.nimain1ICCcovcvlrx3 lefsest.nimain1ICCcovcvlrx4 lefsest.nimain1ICCcovcvlrx5 lefsest.nimain1ICCcovcvlrxt lefsest.nimain1ICCcovcvlbet lefsest.nimain1ICCcovcvltpe lefsest.nimain1ICCcovcvltif lefsest.nimain1ICCcovcvltic lefsest.nimain1ICCcovcvldis lefsest.nimain1ICCcovcvlrbi lefsest.nimain1ICCcovcvlsv5 lefsest.nimain1ICCcovcvllv5 lefsest.nimain1ICCcovcvlsvs lefsest.nimain1ICCcovcvllvs lefsest.nimain1ICCcovcvllvl lefsest.nimain1ICCcovflucat lefsest.nimain1ICCcovflulet lefsest.nimain1ICCcovfluctw lefsest.nimain1ICCcovflultw lefsest.nimain1ICCcovflucti lefsest.nimain1ICCcovfluctp lefsest.nimain1ICCcovflulti lefsest.nimain1ICCcovflultp lefsest.nimain1ICCcovflucfw lefsest.nimain1ICCcovflucfp lefsest.nimain1ICCcovflucfi lefsest.nimain1ICCcovflucaw lefsest.nimain1ICCcovflucap lefsest.nimain1ICCcovflucai lefsest.nimain1ICCcovflucvw lefsest.nimain1ICCcovflucvp lefsest.nimain1ICCcovflucvi lefsest.nimain1ICCcovflulfw lefsest.nimain1ICCcovflulfp lefsest.nimain1ICCcovflulfi lefsest.nimain1ICCcovflulaw lefsest.nimain1ICCcovflulap lefsest.nimain1ICCcovflulai lefsest.nimain1ICCcovflulsw lefsest.nimain1ICCcovflulsp lefsest.nimain1ICCcovflulsi lefsest.nimain1ICCcovpmato3 lefsest.nimain1ICCcovdigbac lefsest.nimain1ICCcovdigfor lefsest.nimain1ICCcovcrdrot lefsest.nimain1ICCcovboscor lefsest.nimain1ICCcovbvrtot;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;

* Export lefslib.nimain1ICC*est as .csv;
PROC EXPORT data = lefslib.nimain1ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* ZREDNDNIMAIN1;
%macro icczredndnimain1(varnames); * OPERATE ON ZREDNDNIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zredndnimain1ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zredndnimain1ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zredndnimain1ICCcov&dv;
		PROC SORT data = lefsest.zredndnimain1ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%icczredndnimain1(simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot);
DATA lefslib.zredndnimain1ICCest;
MERGE lefsest.zredndnimain1ICCcovsimtot lefsest.zredndnimain1ICCcovdigfor lefsest.zredndnimain1ICCcovcvlrst lefsest.zredndnimain1ICCcovcvldis lefsest.zredndnimain1ICCcovcvlsv5 lefsest.zredndnimain1ICCcovfluctp lefsest.zredndnimain1ICCcovflultp lefsest.zredndnimain1ICCcovalphas lefsest.zredndnimain1ICCcovdigbac lefsest.zredndnimain1ICCcovflulet lefsest.zredndnimain1ICCcovflucat lefsest.zredndnimain1ICCcovboscor lefsest.zredndnimain1ICCcovtrba lefsest.zredndnimain1ICCcovcvlfrl lefsest.zredndnimain1ICCcovcvlfrs lefsest.zredndnimain1ICCcovcvltca lefsest.zredndnimain1ICCcovtrats lefsest.zredndnimain1ICCcovpmato3 lefsest.zredndnimain1ICCcovbvrtot lefsest.zredndnimain1ICCcovcrdrot;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zredndnimain1ICC*est as .csv;
PROC EXPORT data = lefslib.zredndnimain1ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* COMPZREDNDNIMAIN1;
%macro icccompzredndnimain1(varnames); * OPERATE ON COMPZREDNDNIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.compzredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.compzredndnimain1ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.compzredndnimain1ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.compzredndnimain1ICCcov&dv;
		PROC SORT data = lefsest.compzredndnimain1ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%icccompzredndnimain1(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);
DATA lefslib.compzredndnimain1ICCest;
MERGE lefsest.compzredndnimain1ICCcovabstr lefsest.compzredndnimain1ICCcovcap lefsest.compzredndnimain1ICCcovchunk lefsest.compzredndnimain1ICCcovdisc lefsest.compzredndnimain1ICCcovinhib lefsest.compzredndnimain1ICCcovmanip lefsest.compzredndnimain1ICCcovpretr lefsest.compzredndnimain1ICCcovsretr lefsest.compzredndnimain1ICCcovswtch lefsest.compzredndnimain1ICCcovltm lefsest.compzredndnimain1ICCcovstm lefsest.compzredndnimain1ICCcovspd lefsest.compzredndnimain1ICCcovvocab lefsest.compzredndnimain1ICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.compzredndnimain1ICC*est as .csv;
PROC EXPORT data = lefslib.compzredndnimain1ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

/* MIXED MODEL */

option nofmterr; run; * Switch off format errors ;

* NIMAIN1;
%macro mixedmodelnimain1(varnames); * OPERATE ON NIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.nimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.nimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.nimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.nimain1m1FE&dv;
		PROC SORT DATA = lefsest.nimain1m1FE&dv; by Effect; RUN;

		* Format < .0001 in pt;
		DATA lefsest.nimain1m1FE&dv;
		SET lefsest.nimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.nimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.nimain1m1RE&dv;
		PROC SORT DATA = lefsest.nimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.nimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.nimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.nimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.nimain1m2FE&dv;
		run;
		PROC SORT DATA = lefsest.nimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.nimain1m2FE&dv;
		SET lefsest.nimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.nimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.nimain1m2RE&dv;
		PROC SORT DATA = lefsest.nimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelnimain1(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);

* Merge variables into lefslib;
DATA lefslib.nimain1M1fest;
MERGE lefsest.nimain1m1FEtrats lefsest.nimain1m1FEtrbts lefsest.nimain1m1FEtrba lefsest.nimain1m1FEalphas lefsest.nimain1m1FEsimtot lefsest.nimain1m1FEcvltca lefsest.nimain1m1FEcvlca1 lefsest.nimain1m1FEcvlca2 lefsest.nimain1m1FEcvlca3 lefsest.nimain1m1FEcvlca4 lefsest.nimain1m1FEcvlca5 lefsest.nimain1m1FEcvltcb lefsest.nimain1m1FEcvlfrs lefsest.nimain1m1FEcvlcrs lefsest.nimain1m1FEcvlfrl lefsest.nimain1m1FEcvlcrl lefsest.nimain1m1FEcvlrs1 lefsest.nimain1m1FEcvlrs2 lefsest.nimain1m1FEcvlrs3 lefsest.nimain1m1FEcvlrs4 lefsest.nimain1m1FEcvlrs5 lefsest.nimain1m1FEcvlrst lefsest.nimain1m1FEcvlrx1 lefsest.nimain1m1FEcvlrx2 lefsest.nimain1m1FEcvlrx3 lefsest.nimain1m1FEcvlrx4 lefsest.nimain1m1FEcvlrx5 lefsest.nimain1m1FEcvlrxt lefsest.nimain1m1FEcvlbet lefsest.nimain1m1FEcvltpe lefsest.nimain1m1FEcvltif lefsest.nimain1m1FEcvltic lefsest.nimain1m1FEcvldis lefsest.nimain1m1FEcvlrbi lefsest.nimain1m1FEcvlsv5 lefsest.nimain1m1FEcvllv5 lefsest.nimain1m1FEcvlsvs lefsest.nimain1m1FEcvllvs lefsest.nimain1m1FEcvllvl lefsest.nimain1m1FEflucat lefsest.nimain1m1FEflulet lefsest.nimain1m1FEfluctw lefsest.nimain1m1FEflultw lefsest.nimain1m1FEflucti lefsest.nimain1m1FEfluctp lefsest.nimain1m1FEflulti lefsest.nimain1m1FEflultp lefsest.nimain1m1FEflucfw lefsest.nimain1m1FEflucfp lefsest.nimain1m1FEflucfi lefsest.nimain1m1FEflucaw lefsest.nimain1m1FEflucap lefsest.nimain1m1FEflucai lefsest.nimain1m1FEflucvw lefsest.nimain1m1FEflucvp lefsest.nimain1m1FEflucvi lefsest.nimain1m1FEflulfw lefsest.nimain1m1FEflulfp lefsest.nimain1m1FEflulfi lefsest.nimain1m1FEflulaw lefsest.nimain1m1FEflulap lefsest.nimain1m1FEflulai lefsest.nimain1m1FEflulsw lefsest.nimain1m1FEflulsp lefsest.nimain1m1FEflulsi lefsest.nimain1m1FEpmato3 lefsest.nimain1m1FEdigbac lefsest.nimain1m1FEdigfor lefsest.nimain1m1FEcrdrot lefsest.nimain1m1FEboscor lefsest.nimain1m1FEbvrtot;
BY Effect;
RUN;

DATA lefslib.nimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.nimain1m1REtrats lefsest.nimain1m1REtrbts lefsest.nimain1m1REtrba lefsest.nimain1m1REalphas lefsest.nimain1m1REsimtot lefsest.nimain1m1REcvltca lefsest.nimain1m1REcvlca1 lefsest.nimain1m1REcvlca2 lefsest.nimain1m1REcvlca3 lefsest.nimain1m1REcvlca4 lefsest.nimain1m1REcvlca5 lefsest.nimain1m1REcvltcb lefsest.nimain1m1REcvlfrs lefsest.nimain1m1REcvlcrs lefsest.nimain1m1REcvlfrl lefsest.nimain1m1REcvlcrl lefsest.nimain1m1REcvlrs1 lefsest.nimain1m1REcvlrs2 lefsest.nimain1m1REcvlrs3 lefsest.nimain1m1REcvlrs4 lefsest.nimain1m1REcvlrs5 lefsest.nimain1m1REcvlrst lefsest.nimain1m1REcvlrx1 lefsest.nimain1m1REcvlrx2 lefsest.nimain1m1REcvlrx3 lefsest.nimain1m1REcvlrx4 lefsest.nimain1m1REcvlrx5 lefsest.nimain1m1REcvlrxt lefsest.nimain1m1REcvlbet lefsest.nimain1m1REcvltpe lefsest.nimain1m1REcvltif lefsest.nimain1m1REcvltic lefsest.nimain1m1REcvldis lefsest.nimain1m1REcvlrbi lefsest.nimain1m1REcvlsv5 lefsest.nimain1m1REcvllv5 lefsest.nimain1m1REcvlsvs lefsest.nimain1m1REcvllvs lefsest.nimain1m1REcvllvl lefsest.nimain1m1REflucat lefsest.nimain1m1REflulet lefsest.nimain1m1REfluctw lefsest.nimain1m1REflultw lefsest.nimain1m1REflucti lefsest.nimain1m1REfluctp lefsest.nimain1m1REflulti lefsest.nimain1m1REflultp lefsest.nimain1m1REflucfw lefsest.nimain1m1REflucfp lefsest.nimain1m1REflucfi lefsest.nimain1m1REflucaw lefsest.nimain1m1REflucap lefsest.nimain1m1REflucai lefsest.nimain1m1REflucvw lefsest.nimain1m1REflucvp lefsest.nimain1m1REflucvi lefsest.nimain1m1REflulfw lefsest.nimain1m1REflulfp lefsest.nimain1m1REflulfi lefsest.nimain1m1REflulaw lefsest.nimain1m1REflulap lefsest.nimain1m1REflulai lefsest.nimain1m1REflulsw lefsest.nimain1m1REflulsp lefsest.nimain1m1REflulsi lefsest.nimain1m1REpmato3 lefsest.nimain1m1REdigbac lefsest.nimain1m1REdigfor lefsest.nimain1m1REcrdrot lefsest.nimain1m1REboscor lefsest.nimain1m1REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.nimain1M2fest;
MERGE lefsest.nimain1m2FEtrats lefsest.nimain1m2FEtrbts lefsest.nimain1m2FEtrba lefsest.nimain1m2FEalphas lefsest.nimain1m2FEsimtot lefsest.nimain1m2FEcvltca lefsest.nimain1m2FEcvlca1 lefsest.nimain1m2FEcvlca2 lefsest.nimain1m2FEcvlca3 lefsest.nimain1m2FEcvlca4 lefsest.nimain1m2FEcvlca5 lefsest.nimain1m2FEcvltcb lefsest.nimain1m2FEcvlfrs lefsest.nimain1m2FEcvlcrs lefsest.nimain1m2FEcvlfrl lefsest.nimain1m2FEcvlcrl lefsest.nimain1m2FEcvlrs1 lefsest.nimain1m2FEcvlrs2 lefsest.nimain1m2FEcvlrs3 lefsest.nimain1m2FEcvlrs4 lefsest.nimain1m2FEcvlrs5 lefsest.nimain1m2FEcvlrst lefsest.nimain1m2FEcvlrx1 lefsest.nimain1m2FEcvlrx2 lefsest.nimain1m2FEcvlrx3 lefsest.nimain1m2FEcvlrx4 lefsest.nimain1m2FEcvlrx5 lefsest.nimain1m2FEcvlrxt lefsest.nimain1m2FEcvlbet lefsest.nimain1m2FEcvltpe lefsest.nimain1m2FEcvltif lefsest.nimain1m2FEcvltic lefsest.nimain1m2FEcvldis lefsest.nimain1m2FEcvlrbi lefsest.nimain1m2FEcvlsv5 lefsest.nimain1m2FEcvllv5 lefsest.nimain1m2FEcvlsvs lefsest.nimain1m2FEcvllvs lefsest.nimain1m2FEcvllvl lefsest.nimain1m2FEflucat lefsest.nimain1m2FEflulet lefsest.nimain1m2FEfluctw lefsest.nimain1m2FEflultw lefsest.nimain1m2FEflucti lefsest.nimain1m2FEfluctp lefsest.nimain1m2FEflulti lefsest.nimain1m2FEflultp lefsest.nimain1m2FEflucfw lefsest.nimain1m2FEflucfp lefsest.nimain1m2FEflucfi lefsest.nimain1m2FEflucaw lefsest.nimain1m2FEflucap lefsest.nimain1m2FEflucai lefsest.nimain1m2FEflucvw lefsest.nimain1m2FEflucvp lefsest.nimain1m2FEflucvi lefsest.nimain1m2FEflulfw lefsest.nimain1m2FEflulfp lefsest.nimain1m2FEflulfi lefsest.nimain1m2FEflulaw lefsest.nimain1m2FEflulap lefsest.nimain1m2FEflulai lefsest.nimain1m2FEflulsw lefsest.nimain1m2FEflulsp lefsest.nimain1m2FEflulsi lefsest.nimain1m2FEpmato3 lefsest.nimain1m2FEdigbac lefsest.nimain1m2FEdigfor lefsest.nimain1m2FEcrdrot lefsest.nimain1m2FEboscor lefsest.nimain1m2FEbvrtot;
BY Effect;
RUN;

DATA lefslib.nimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.nimain1m2REtrats lefsest.nimain1m2REtrbts lefsest.nimain1m2REtrba lefsest.nimain1m2REalphas lefsest.nimain1m2REsimtot lefsest.nimain1m2REcvltca lefsest.nimain1m2REcvlca1 lefsest.nimain1m2REcvlca2 lefsest.nimain1m2REcvlca3 lefsest.nimain1m2REcvlca4 lefsest.nimain1m2REcvlca5 lefsest.nimain1m2REcvltcb lefsest.nimain1m2REcvlfrs lefsest.nimain1m2REcvlcrs lefsest.nimain1m2REcvlfrl lefsest.nimain1m2REcvlcrl lefsest.nimain1m2REcvlrs1 lefsest.nimain1m2REcvlrs2 lefsest.nimain1m2REcvlrs3 lefsest.nimain1m2REcvlrs4 lefsest.nimain1m2REcvlrs5 lefsest.nimain1m2REcvlrst lefsest.nimain1m2REcvlrx1 lefsest.nimain1m2REcvlrx2 lefsest.nimain1m2REcvlrx3 lefsest.nimain1m2REcvlrx4 lefsest.nimain1m2REcvlrx5 lefsest.nimain1m2REcvlrxt lefsest.nimain1m2REcvlbet lefsest.nimain1m2REcvltpe lefsest.nimain1m2REcvltif lefsest.nimain1m2REcvltic lefsest.nimain1m2REcvldis lefsest.nimain1m2REcvlrbi lefsest.nimain1m2REcvlsv5 lefsest.nimain1m2REcvllv5 lefsest.nimain1m2REcvlsvs lefsest.nimain1m2REcvllvs lefsest.nimain1m2REcvllvl lefsest.nimain1m2REflucat lefsest.nimain1m2REflulet lefsest.nimain1m2REfluctw lefsest.nimain1m2REflultw lefsest.nimain1m2REflucti lefsest.nimain1m2REfluctp lefsest.nimain1m2REflulti lefsest.nimain1m2REflultp lefsest.nimain1m2REflucfw lefsest.nimain1m2REflucfp lefsest.nimain1m2REflucfi lefsest.nimain1m2REflucaw lefsest.nimain1m2REflucap lefsest.nimain1m2REflucai lefsest.nimain1m2REflucvw lefsest.nimain1m2REflucvp lefsest.nimain1m2REflucvi lefsest.nimain1m2REflulfw lefsest.nimain1m2REflulfp lefsest.nimain1m2REflulfi lefsest.nimain1m2REflulaw lefsest.nimain1m2REflulap lefsest.nimain1m2REflulai lefsest.nimain1m2REflulsw lefsest.nimain1m2REflulsp lefsest.nimain1m2REflulsi lefsest.nimain1m2REpmato3 lefsest.nimain1m2REdigbac lefsest.nimain1m2REdigfor lefsest.nimain1m2REcrdrot lefsest.nimain1m2REboscor lefsest.nimain1m2REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.nimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.nimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.nimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.nimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* ZREDNDNIMAIN1;
%macro mixedmodelzredndnimain1(varnames); * OPERATE ON ZREDNDNIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndnimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndnimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndnimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndnimain1m1FE&dv;
		PROC SORT DATA = lefsest.zredndnimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndnimain1m1FE&dv;
		SET lefsest.zredndnimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zredndnimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndnimain1m1RE&dv;
		PROC SORT DATA = lefsest.zredndnimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndnimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndnimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndnimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndnimain1m2FE&dv;
		PROC SORT DATA = lefsest.zredndnimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndnimain1m2FE&dv;
		SET lefsest.zredndnimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zredndnimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndnimain1m2RE&dv;
		PROC SORT DATA = lefsest.zredndnimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzredndnimain1(simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot);

* Merge variables into lefslib;
DATA lefslib.zredndnimain1M1fest;
MERGE lefsest.zredndnimain1m1FEsimtot lefsest.zredndnimain1m1FEdigfor lefsest.zredndnimain1m1FEcvlrst lefsest.zredndnimain1m1FEcvldis lefsest.zredndnimain1m1FEcvlsv5 lefsest.zredndnimain1m1FEfluctp lefsest.zredndnimain1m1FEflultp lefsest.zredndnimain1m1FEalphas lefsest.zredndnimain1m1FEdigbac lefsest.zredndnimain1m1FEflulet lefsest.zredndnimain1m1FEflucat lefsest.zredndnimain1m1FEboscor lefsest.zredndnimain1m1FEtrba lefsest.zredndnimain1m1FEcvlfrl lefsest.zredndnimain1m1FEcvlfrs lefsest.zredndnimain1m1FEcvltca lefsest.zredndnimain1m1FEtrats lefsest.zredndnimain1m1FEpmato3 lefsest.zredndnimain1m1FEbvrtot lefsest.zredndnimain1m1FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndnimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.zredndnimain1m1REsimtot lefsest.zredndnimain1m1REdigfor lefsest.zredndnimain1m1REcvlrst lefsest.zredndnimain1m1REcvldis lefsest.zredndnimain1m1REcvlsv5 lefsest.zredndnimain1m1REfluctp lefsest.zredndnimain1m1REflultp lefsest.zredndnimain1m1REalphas lefsest.zredndnimain1m1REdigbac lefsest.zredndnimain1m1REflulet lefsest.zredndnimain1m1REflucat lefsest.zredndnimain1m1REboscor lefsest.zredndnimain1m1REtrba lefsest.zredndnimain1m1REcvlfrl lefsest.zredndnimain1m1REcvlfrs lefsest.zredndnimain1m1REcvltca lefsest.zredndnimain1m1REtrats lefsest.zredndnimain1m1REpmato3 lefsest.zredndnimain1m1REbvrtot lefsest.zredndnimain1m1REcrdrot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zredndnimain1M2fest;
MERGE lefsest.zredndnimain1m2FEsimtot lefsest.zredndnimain1m2FEdigfor lefsest.zredndnimain1m2FEcvlrst lefsest.zredndnimain1m2FEcvldis lefsest.zredndnimain1m2FEcvlsv5 lefsest.zredndnimain1m2FEfluctp lefsest.zredndnimain1m2FEflultp lefsest.zredndnimain1m2FEalphas lefsest.zredndnimain1m2FEdigbac lefsest.zredndnimain1m2FEflulet lefsest.zredndnimain1m2FEflucat lefsest.zredndnimain1m2FEboscor lefsest.zredndnimain1m2FEtrba lefsest.zredndnimain1m2FEcvlfrl lefsest.zredndnimain1m2FEcvlfrs lefsest.zredndnimain1m2FEcvltca lefsest.zredndnimain1m2FEtrats lefsest.zredndnimain1m2FEpmato3 lefsest.zredndnimain1m2FEbvrtot lefsest.zredndnimain1m2FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndnimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.zredndnimain1m2REsimtot lefsest.zredndnimain1m2REdigfor lefsest.zredndnimain1m2REcvlrst lefsest.zredndnimain1m2REcvldis lefsest.zredndnimain1m2REcvlsv5 lefsest.zredndnimain1m2REfluctp lefsest.zredndnimain1m2REflultp lefsest.zredndnimain1m2REalphas lefsest.zredndnimain1m2REdigbac lefsest.zredndnimain1m2REflulet lefsest.zredndnimain1m2REflucat lefsest.zredndnimain1m2REboscor lefsest.zredndnimain1m2REtrba lefsest.zredndnimain1m2REcvlfrl lefsest.zredndnimain1m2REcvlfrs lefsest.zredndnimain1m2REcvltca lefsest.zredndnimain1m2REtrats lefsest.zredndnimain1m2REpmato3 lefsest.zredndnimain1m2REbvrtot lefsest.zredndnimain1m2REcrdrot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.zredndnimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndnimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndnimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndnimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndnimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* COMPZREDNDNIMAIN1;
%macro mixedmodelcompzredndnimain1(varnames); * COMPZREDNDNIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.compzredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndnimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndnimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndnimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndnimain1m1FE&dv;
		PROC SORT DATA = lefsest.compzredndnimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndnimain1m1FE&dv;
		SET lefsest.compzredndnimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.compzredndnimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndnimain1m1RE&dv;
		PROC SORT DATA = lefsest.compzredndnimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.compzredndnimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndnimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndnimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndnimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndnimain1m2FE&dv;
		PROC SORT DATA = lefsest.compzredndnimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndnimain1m2FE&dv;
		SET lefsest.compzredndnimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.compzredndnimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndnimain1m2RE&dv;
		PROC SORT DATA = lefsest.compzredndnimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelcompzredndnimain1(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);

* Merge variables into lefslib;
DATA lefslib.compzredndnimain1M1fest;
MERGE lefsest.compzredndnimain1m1FEabstr lefsest.compzredndnimain1m1FEcap lefsest.compzredndnimain1m1FEchunk lefsest.compzredndnimain1m1FEdisc lefsest.compzredndnimain1m1FEinhib lefsest.compzredndnimain1m1FEmanip lefsest.compzredndnimain1m1FEpretr lefsest.compzredndnimain1m1FEsretr lefsest.compzredndnimain1m1FEswtch lefsest.compzredndnimain1m1FEltm lefsest.compzredndnimain1m1FEstm lefsest.compzredndnimain1m1FEspd lefsest.compzredndnimain1m1FEvocab lefsest.compzredndnimain1m1FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndnimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndnimain1m1REabstr lefsest.compzredndnimain1m1REcap lefsest.compzredndnimain1m1REchunk lefsest.compzredndnimain1m1REdisc lefsest.compzredndnimain1m1REinhib lefsest.compzredndnimain1m1REmanip lefsest.compzredndnimain1m1REpretr lefsest.compzredndnimain1m1REsretr lefsest.compzredndnimain1m1REswtch lefsest.compzredndnimain1m1REltm lefsest.compzredndnimain1m1REstm lefsest.compzredndnimain1m1REspd lefsest.compzredndnimain1m1REvocab lefsest.compzredndnimain1m1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.compzredndnimain1M2fest;
MERGE lefsest.compzredndnimain1m2FEabstr lefsest.compzredndnimain1m2FEcap lefsest.compzredndnimain1m2FEchunk lefsest.compzredndnimain1m2FEdisc lefsest.compzredndnimain1m2FEinhib lefsest.compzredndnimain1m2FEmanip lefsest.compzredndnimain1m2FEpretr lefsest.compzredndnimain1m2FEsretr lefsest.compzredndnimain1m2FEswtch lefsest.compzredndnimain1m2FEltm lefsest.compzredndnimain1m2FEstm lefsest.compzredndnimain1m2FEspd lefsest.compzredndnimain1m2FEvocab lefsest.compzredndnimain1m2FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndnimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndnimain1m2REabstr lefsest.compzredndnimain1m2REcap lefsest.compzredndnimain1m2REchunk lefsest.compzredndnimain1m2REdisc lefsest.compzredndnimain1m2REinhib lefsest.compzredndnimain1m2REmanip lefsest.compzredndnimain1m2REpretr lefsest.compzredndnimain1m2REsretr lefsest.compzredndnimain1m2REswtch lefsest.compzredndnimain1m2REltm lefsest.compzredndnimain1m2REstm lefsest.compzredndnimain1m2REspd lefsest.compzredndnimain1m2REvocab lefsest.compzredndnimain1m2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.compzredndnimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndnimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndnimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndnimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndnimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* INCLUDE CN and MCI group predictor;

* CREATE ZREDNDG1NIMAIN1;
* Code dementia = 1;
data lefslib.zredndg1nimain1; set lefslib.zredndnimain1; * CREATE ZREDNDG1NIMAIN1;
if donset = . then dg = 0;
if donset ~= . then dg = 1;
run;

PROC EXPORT data = lefslib.zredndg1nimain1
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg1nimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelzredndg1nimain1(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.zredndg1nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndg1nimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndg1nimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndg1nimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndg1nimain1m1FE&dv;
		PROC SORT DATA = lefsest.zredndg1nimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndg1nimain1m1FE&dv;
		SET lefsest.zredndg1nimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zredndg1nimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndg1nimain1m1RE&dv;
		PROC SORT DATA = lefsest.zredndg1nimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zredndg1nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndg1nimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndg1nimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndg1nimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndg1nimain1m2FE&dv;
		PROC SORT DATA = lefsest.zredndg1nimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndg1nimain1m2FE&dv;
		SET lefsest.zredndg1nimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zredndg1nimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndg1nimain1m2RE&dv;
		PROC SORT DATA = lefsest.zredndg1nimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzredndg1nimain1(alphas bvrtot boscor crdrot cvlrst cvlsv5 cvldis cvlfrs cvltca cvlfrl digfor digbac pmato3 simtot trats trba flucat fluctp flulet flultp);

* Merge variables into lefslib;
DATA lefslib.zredndg1nimain1M1fest;
MERGE  lefsest.zredndg1nimain1m1FEsimtot lefsest.zredndg1nimain1m1FEdigfor lefsest.zredndg1nimain1m1FEcvlrst lefsest.zredndg1nimain1m1FEcvldis lefsest.zredndg1nimain1m1FEcvlsv5 lefsest.zredndg1nimain1m1FEfluctp lefsest.zredndg1nimain1m1FEflultp lefsest.zredndg1nimain1m1FEalphas lefsest.zredndg1nimain1m1FEdigbac lefsest.zredndg1nimain1m1FEflulet lefsest.zredndg1nimain1m1FEflucat lefsest.zredndg1nimain1m1FEboscor lefsest.zredndg1nimain1m1FEtrba lefsest.zredndg1nimain1m1FEcvlfrl lefsest.zredndg1nimain1m1FEcvlfrs lefsest.zredndg1nimain1m1FEcvltca lefsest.zredndg1nimain1m1FEtrats lefsest.zredndg1nimain1m1FEpmato3 lefsest.zredndg1nimain1m1FEbvrtot lefsest.zredndg1nimain1m1FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndg1nimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.zredndg1nimain1m1REsimtot lefsest.zredndg1nimain1m1REdigfor lefsest.zredndg1nimain1m1REcvlrst lefsest.zredndg1nimain1m1REcvldis lefsest.zredndg1nimain1m1REcvlsv5 lefsest.zredndg1nimain1m1REfluctp lefsest.zredndg1nimain1m1REflultp lefsest.zredndg1nimain1m1REalphas lefsest.zredndg1nimain1m1REdigbac lefsest.zredndg1nimain1m1REflulet lefsest.zredndg1nimain1m1REflucat lefsest.zredndg1nimain1m1REboscor lefsest.zredndg1nimain1m1REtrba lefsest.zredndg1nimain1m1REcvlfrl lefsest.zredndg1nimain1m1REcvlfrs lefsest.zredndg1nimain1m1REcvltca lefsest.zredndg1nimain1m1REtrats lefsest.zredndg1nimain1m1REpmato3 lefsest.zredndg1nimain1m1REbvrtot lefsest.zredndg1nimain1m1REcrdrot;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zredndg1nimain1M2fest;
MERGE lefsest.zredndg1nimain1m2FEsimtot lefsest.zredndg1nimain1m2FEdigfor lefsest.zredndg1nimain1m2FEcvlrst lefsest.zredndg1nimain1m2FEcvldis lefsest.zredndg1nimain1m2FEcvlsv5 lefsest.zredndg1nimain1m2FEfluctp lefsest.zredndg1nimain1m2FEflultp lefsest.zredndg1nimain1m2FEalphas lefsest.zredndg1nimain1m2FEdigbac lefsest.zredndg1nimain1m2FEflulet lefsest.zredndg1nimain1m2FEflucat lefsest.zredndg1nimain1m2FEboscor lefsest.zredndg1nimain1m2FEtrba lefsest.zredndg1nimain1m2FEcvlfrl lefsest.zredndg1nimain1m2FEcvlfrs lefsest.zredndg1nimain1m2FEcvltca lefsest.zredndg1nimain1m2FEtrats lefsest.zredndg1nimain1m2FEpmato3 lefsest.zredndg1nimain1m2FEbvrtot lefsest.zredndg1nimain1m2FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndg1nimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.zredndg1nimain1m2REsimtot lefsest.zredndg1nimain1m2REdigfor lefsest.zredndg1nimain1m2REcvlrst lefsest.zredndg1nimain1m2REcvldis lefsest.zredndg1nimain1m2REcvlsv5 lefsest.zredndg1nimain1m2REfluctp lefsest.zredndg1nimain1m2REflultp lefsest.zredndg1nimain1m2REalphas lefsest.zredndg1nimain1m2REdigbac lefsest.zredndg1nimain1m2REflulet lefsest.zredndg1nimain1m2REflucat lefsest.zredndg1nimain1m2REboscor lefsest.zredndg1nimain1m2REtrba lefsest.zredndg1nimain1m2REcvlfrl lefsest.zredndg1nimain1m2REcvlfrs lefsest.zredndg1nimain1m2REcvltca lefsest.zredndg1nimain1m2REtrats lefsest.zredndg1nimain1m2REpmato3 lefsest.zredndg1nimain1m2REbvrtot lefsest.zredndg1nimain1m2REcrdrot;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zredndg1nimain1*est as .csv;
PROC EXPORT data = lefslib.zredndg1nimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg1nimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg1nimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg1nimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg1nimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg1nimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg1nimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg1nimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* CREATE ZREDNDG2NIMAIN1;
* Code dementia = 0;
data lefslib.zredndg2nimain1; set lefslib.zredndnimain1; * CREATE ZREDNDG2NIMAIN1;
if donset = . then dg = 1;
if donset ~= . then dg = 0;
run; 


PROC EXPORT data = lefslib.zredndg2nimain1
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg2nimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelzredndg2nimain1(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.zredndg2nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndg2nimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndg2nimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndg2nimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndg2nimain1m1FE&dv;
		PROC SORT DATA = lefsest.zredndg2nimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndg2nimain1m1FE&dv;
		SET lefsest.zredndg2nimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zredndg2nimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndg2nimain1m1RE&dv;
		PROC SORT DATA = lefsest.zredndg2nimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zredndg2nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zredndg2nimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zredndg2nimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zredndg2nimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zredndg2nimain1m2FE&dv;
		PROC SORT DATA = lefsest.zredndg2nimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.zredndg2nimain1m2FE&dv;
		SET lefsest.zredndg2nimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zredndg2nimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zredndg2nimain1m2RE&dv;
		PROC SORT DATA = lefsest.zredndg2nimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzredndg2nimain1(alphas bvrtot boscor crdrot cvlrst cvlsv5 cvldis cvlfrs cvltca cvlfrl digfor digbac pmato3 simtot trats trba flucat fluctp flulet flultp);

* Merge variables into lefslib;
DATA lefslib.zredndg2nimain1M1fest;
MERGE  lefsest.zredndg2nimain1m1FEsimtot lefsest.zredndg2nimain1m1FEdigfor lefsest.zredndg2nimain1m1FEcvlrst lefsest.zredndg2nimain1m1FEcvldis lefsest.zredndg2nimain1m1FEcvlsv5 lefsest.zredndg2nimain1m1FEfluctp lefsest.zredndg2nimain1m1FEflultp lefsest.zredndg2nimain1m1FEalphas lefsest.zredndg2nimain1m1FEdigbac lefsest.zredndg2nimain1m1FEflulet lefsest.zredndg2nimain1m1FEflucat lefsest.zredndg2nimain1m1FEboscor lefsest.zredndg2nimain1m1FEtrba lefsest.zredndg2nimain1m1FEcvlfrl lefsest.zredndg2nimain1m1FEcvlfrs lefsest.zredndg2nimain1m1FEcvltca lefsest.zredndg2nimain1m1FEtrats lefsest.zredndg2nimain1m1FEpmato3 lefsest.zredndg2nimain1m1FEbvrtot lefsest.zredndg2nimain1m1FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndg2nimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.zredndg2nimain1m1REsimtot lefsest.zredndg2nimain1m1REdigfor lefsest.zredndg2nimain1m1REcvlrst lefsest.zredndg2nimain1m1REcvldis lefsest.zredndg2nimain1m1REcvlsv5 lefsest.zredndg2nimain1m1REfluctp lefsest.zredndg2nimain1m1REflultp lefsest.zredndg2nimain1m1REalphas lefsest.zredndg2nimain1m1REdigbac lefsest.zredndg2nimain1m1REflulet lefsest.zredndg2nimain1m1REflucat lefsest.zredndg2nimain1m1REboscor lefsest.zredndg2nimain1m1REtrba lefsest.zredndg2nimain1m1REcvlfrl lefsest.zredndg2nimain1m1REcvlfrs lefsest.zredndg2nimain1m1REcvltca lefsest.zredndg2nimain1m1REtrats lefsest.zredndg2nimain1m1REpmato3 lefsest.zredndg2nimain1m1REbvrtot lefsest.zredndg2nimain1m1REcrdrot;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zredndg2nimain1M2fest;
MERGE lefsest.zredndg2nimain1m2FEsimtot lefsest.zredndg2nimain1m2FEdigfor lefsest.zredndg2nimain1m2FEcvlrst lefsest.zredndg2nimain1m2FEcvldis lefsest.zredndg2nimain1m2FEcvlsv5 lefsest.zredndg2nimain1m2FEfluctp lefsest.zredndg2nimain1m2FEflultp lefsest.zredndg2nimain1m2FEalphas lefsest.zredndg2nimain1m2FEdigbac lefsest.zredndg2nimain1m2FEflulet lefsest.zredndg2nimain1m2FEflucat lefsest.zredndg2nimain1m2FEboscor lefsest.zredndg2nimain1m2FEtrba lefsest.zredndg2nimain1m2FEcvlfrl lefsest.zredndg2nimain1m2FEcvlfrs lefsest.zredndg2nimain1m2FEcvltca lefsest.zredndg2nimain1m2FEtrats lefsest.zredndg2nimain1m2FEpmato3 lefsest.zredndg2nimain1m2FEbvrtot lefsest.zredndg2nimain1m2FEcrdrot;
BY Effect;
RUN;

DATA lefslib.zredndg2nimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.zredndg2nimain1m2REsimtot lefsest.zredndg2nimain1m2REdigfor lefsest.zredndg2nimain1m2REcvlrst lefsest.zredndg2nimain1m2REcvldis lefsest.zredndg2nimain1m2REcvlsv5 lefsest.zredndg2nimain1m2REfluctp lefsest.zredndg2nimain1m2REflultp lefsest.zredndg2nimain1m2REalphas lefsest.zredndg2nimain1m2REdigbac lefsest.zredndg2nimain1m2REflulet lefsest.zredndg2nimain1m2REflucat lefsest.zredndg2nimain1m2REboscor lefsest.zredndg2nimain1m2REtrba lefsest.zredndg2nimain1m2REcvlfrl lefsest.zredndg2nimain1m2REcvlfrs lefsest.zredndg2nimain1m2REcvltca lefsest.zredndg2nimain1m2REtrats lefsest.zredndg2nimain1m2REpmato3 lefsest.zredndg2nimain1m2REbvrtot lefsest.zredndg2nimain1m2REcrdrot;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zredndg2nimain1*est as .csv;
PROC EXPORT data = lefslib.zredndg2nimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg2nimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg2nimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg2nimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg2nimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg2nimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zredndg2nimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zredndg2nimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* CREATE COMPZREDNDG1NIMAIN1;
* Code dementia = 1;
data lefslib.compzredndg1nimain1; set lefslib.compzredndnimain1; * CREATE COMPZREDNDG1NIMAIN1;
if donset = . then dg = 0;
if donset ~= . then dg = 1;
run;

PROC EXPORT data = lefslib.compzredndg1nimain1
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg1nimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelcompzredndg1nimain1(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.compzredndg1nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndg1nimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndg1nimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndg1nimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndg1nimain1m1FE&dv;
		PROC SORT DATA = lefsest.compzredndg1nimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndg1nimain1m1FE&dv;
		SET lefsest.compzredndg1nimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.compzredndg1nimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndg1nimain1m1RE&dv;
		PROC SORT DATA = lefsest.compzredndg1nimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.compzredndg1nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndg1nimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndg1nimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndg1nimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndg1nimain1m2FE&dv;
		PROC SORT DATA = lefsest.compzredndg1nimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndg1nimain1m2FE&dv;
		SET lefsest.compzredndg1nimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.compzredndg1nimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndg1nimain1m2RE&dv;
		PROC SORT DATA = lefsest.compzredndg1nimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelcompzredndg1nimain1(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);

* Merge variables into lefslib;
DATA lefslib.compzredndg1nimain1M1fest;
MERGE  lefsest.compzredndg1nimain1m1FEabstr lefsest.compzredndg1nimain1m1FEcap lefsest.compzredndg1nimain1m1FEchunk lefsest.compzredndg1nimain1m1FEdisc lefsest.compzredndg1nimain1m1FEinhib lefsest.compzredndg1nimain1m1FEmanip lefsest.compzredndg1nimain1m1FEpretr lefsest.compzredndg1nimain1m1FEsretr lefsest.compzredndg1nimain1m1FEswtch lefsest.compzredndg1nimain1m1FEltm lefsest.compzredndg1nimain1m1FEstm lefsest.compzredndg1nimain1m1FEspd lefsest.compzredndg1nimain1m1FEvocab lefsest.compzredndg1nimain1m1FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndg1nimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndg1nimain1m1REabstr lefsest.compzredndg1nimain1m1REcap lefsest.compzredndg1nimain1m1REchunk lefsest.compzredndg1nimain1m1REdisc lefsest.compzredndg1nimain1m1REinhib lefsest.compzredndg1nimain1m1REmanip lefsest.compzredndg1nimain1m1REpretr lefsest.compzredndg1nimain1m1REsretr lefsest.compzredndg1nimain1m1REswtch lefsest.compzredndg1nimain1m1REltm lefsest.compzredndg1nimain1m1REstm lefsest.compzredndg1nimain1m1REspd lefsest.compzredndg1nimain1m1REvocab lefsest.compzredndg1nimain1m1REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.compzredndg1nimain1M2fest;
MERGE lefsest.compzredndg1nimain1m2FEabstr lefsest.compzredndg1nimain1m2FEcap lefsest.compzredndg1nimain1m2FEchunk lefsest.compzredndg1nimain1m2FEdisc lefsest.compzredndg1nimain1m2FEinhib lefsest.compzredndg1nimain1m2FEmanip lefsest.compzredndg1nimain1m2FEpretr lefsest.compzredndg1nimain1m2FEsretr lefsest.compzredndg1nimain1m2FEswtch lefsest.compzredndg1nimain1m2FEltm lefsest.compzredndg1nimain1m2FEstm lefsest.compzredndg1nimain1m2FEspd lefsest.compzredndg1nimain1m2FEvocab lefsest.compzredndg1nimain1m2FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndg1nimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndg1nimain1m2REabstr lefsest.compzredndg1nimain1m2REcap lefsest.compzredndg1nimain1m2REchunk lefsest.compzredndg1nimain1m2REdisc lefsest.compzredndg1nimain1m2REinhib lefsest.compzredndg1nimain1m2REmanip lefsest.compzredndg1nimain1m2REpretr lefsest.compzredndg1nimain1m2REsretr lefsest.compzredndg1nimain1m2REswtch lefsest.compzredndg1nimain1m2REltm lefsest.compzredndg1nimain1m2REstm lefsest.compzredndg1nimain1m2REspd lefsest.compzredndg1nimain1m2REvocab lefsest.compzredndg1nimain1m2REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.compzredndg1nimain1*est as .csv;
PROC EXPORT data = lefslib.compzredndg1nimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg1nimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg1nimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg1nimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg1nimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg1nimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg1nimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg1nimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';

* CREATE COMPZREDNDG2NIMAIN1;
* Code dementia = 0;
data lefslib.compzredndg2nimain1; set lefslib.compzredndnimain1; * CREATE COMPZREDNDG2NIMAIN1;
if donset = . then dg = 1;
if donset ~= . then dg = 0;
run;

PROC EXPORT data = lefslib.compzredndg2nimain1
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg2nimain1.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelcompzredndg2nimain1(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.compzredndg2nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndg2nimain1m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndg2nimain1m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndg2nimain1m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndg2nimain1m1FE&dv;
		PROC SORT DATA = lefsest.compzredndg2nimain1m1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndg2nimain1m1FE&dv;
		SET lefsest.compzredndg2nimain1m1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.compzredndg2nimain1m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndg2nimain1m1RE&dv;
		PROC SORT DATA = lefsest.compzredndg2nimain1m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.compzredndg2nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndg2nimain1m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndg2nimain1m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndg2nimain1m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndg2nimain1m2FE&dv;
		PROC SORT DATA = lefsest.compzredndg2nimain1m2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.nimain1m2FE&dv;
		SET lefsest.nimain1m2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.compzredndg2nimain1m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndg2nimain1m2RE&dv;
		PROC SORT DATA = lefsest.compzredndg2nimain1m2RE&dv; by id; RUN;

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelcompzredndg2nimain1(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);

* Merge variables into lefslib;
DATA lefslib.compzredndg2nimain1M1fest;
MERGE  lefsest.compzredndg2nimain1m1FEabstr lefsest.compzredndg2nimain1m1FEcap lefsest.compzredndg2nimain1m1FEchunk lefsest.compzredndg2nimain1m1FEdisc lefsest.compzredndg2nimain1m1FEinhib lefsest.compzredndg2nimain1m1FEmanip lefsest.compzredndg2nimain1m1FEpretr lefsest.compzredndg2nimain1m1FEsretr lefsest.compzredndg2nimain1m1FEswtch lefsest.compzredndg2nimain1m1FEltm lefsest.compzredndg2nimain1m1FEstm lefsest.compzredndg2nimain1m1FEspd lefsest.compzredndg2nimain1m1FEvocab lefsest.compzredndg2nimain1m1FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndg2nimain1M1rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndg2nimain1m1REabstr lefsest.compzredndg2nimain1m1REcap lefsest.compzredndg2nimain1m1REchunk lefsest.compzredndg2nimain1m1REdisc lefsest.compzredndg2nimain1m1REinhib lefsest.compzredndg2nimain1m1REmanip lefsest.compzredndg2nimain1m1REpretr lefsest.compzredndg2nimain1m1REsretr lefsest.compzredndg2nimain1m1REswtch lefsest.compzredndg2nimain1m1REltm lefsest.compzredndg2nimain1m1REstm lefsest.compzredndg2nimain1m1REspd lefsest.compzredndg2nimain1m1REvocab lefsest.compzredndg2nimain1m1REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.compzredndg2nimain1M2fest;
MERGE lefsest.compzredndg2nimain1m2FEabstr lefsest.compzredndg2nimain1m2FEcap lefsest.compzredndg2nimain1m2FEchunk lefsest.compzredndg2nimain1m2FEdisc lefsest.compzredndg2nimain1m2FEinhib lefsest.compzredndg2nimain1m2FEmanip lefsest.compzredndg2nimain1m2FEpretr lefsest.compzredndg2nimain1m2FEsretr lefsest.compzredndg2nimain1m2FEswtch lefsest.compzredndg2nimain1m2FEltm lefsest.compzredndg2nimain1m2FEstm lefsest.compzredndg2nimain1m2FEspd lefsest.compzredndg2nimain1m2FEvocab lefsest.compzredndg2nimain1m2FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndg2nimain1M2rest;
MERGE lefslib.nimain1Baseline lefsest.compzredndg2nimain1m2REabstr lefsest.compzredndg2nimain1m2REcap lefsest.compzredndg2nimain1m2REchunk lefsest.compzredndg2nimain1m2REdisc lefsest.compzredndg2nimain1m2REinhib lefsest.compzredndg2nimain1m2REmanip lefsest.compzredndg2nimain1m2REpretr lefsest.compzredndg2nimain1m2REsretr lefsest.compzredndg2nimain1m2REswtch lefsest.compzredndg2nimain1m2REltm lefsest.compzredndg2nimain1m2REstm lefsest.compzredndg2nimain1m2REspd lefsest.compzredndg2nimain1m2REvocab lefsest.compzredndg2nimain1m2REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.compzredndg2nimain1*est as .csv;
PROC EXPORT data = lefslib.compzredndg2nimain1M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg2nimain1M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg2nimain1M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg2nimain1M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg2nimain1M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg2nimain1M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndg2nimain1M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndg2nimain1M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
DM 'CLEAR LOG';
DM 'CLEAR OUTPUT';
