option nofmterr; run; * Switch off format errors ;

/* Create databases with emsfn and output csv for R lmer analysis.
Created by Josh Goh 26 April 2011.

*/

* CREATE COMPZREDNIMAINRSEL DATABASE (COMPOSITE SCORES FROM ZREDNIMAIN1);
* Composite scores from lefslib.zrednimain1 (Averaged: equal weighting from each sub-test);
data compzrednimainRsel; set lefslib.zrednimain1; * CREATE COMPZREDNIMAINRSEL DATABASE;

abstr = simtot;
cap   = digfor;
chunk = cvlrst;
disc  = (cvlsv5 + cvldis)/2;
inhib = (fluctp + flultp)/2;
manip = (alphas + digbac)/2;
pretr = flulet;
sretr = (flucat + boscor)/2;
swtch = trba;
ltm   = cvlfrl;
stm   = (cvlfrs + cvltca)/2;
spd   = trats;
vocab = pmato3;
vis   = (bvrtot + crdrot)/2;

label 	abstr = 'abstraction'
		cap   = 'capacity'
		chunk = 'chunking'
		disc  = 'discrimination'
		inhib = 'inhibition'
		manip = 'manipulation'
		pretr = 'phonemic retrieval'
		sretr = 'semantic retrieval'
		swtch = 'switching'
		ltm   = 'long-term memory'				
		stm   = 'short-term memory'		
		spd   = 'speed'
		vocab = 'vocabulary'
		vis   = 'visual';

drop EMSid EMSwave DOV DOT mridone petdone simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot yr;
run;

data compzrednimainRsel;
retain id sex vi emsvi emsfn baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set compzrednimainRsel;
run;

* Merge with P (emsfn and emsvi based on available CBF data);
proc sort data = compzrednimainRsel;
by emsfn emsvi; run;

proc sort data = lefslib.Psel;
by emsfn emsvi; run;

proc sort data = emslib.emsvis;
by emsfn emsvi; run;

data lefslib.compzrednimainRsel;
merge compzrednimainRsel (IN = InCZNimainR) lefslib.Psel (IN = InP) emslib.emsvis;
by emsfn emsvi;
if InP = 1;
drop baseage;
run;

* CREATE BASELINE AGE VARIABLE;
proc sort data = lefslib.compzrednimainRsel; by id; run;
data lefslib.allbaseline; set lefslib.compzrednimainRsel; 
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.allbaseline; by id; run;
proc sort data = lefslib.compzrednimainRsel; by id; run;

data lefslib.compzrednimainRsel;
merge lefslib.compzrednimainRsel lefslib.allbaseline;
by id;run;

data lefslib.compzrednimainRsel;
retain id sex vi emsvi emsfn baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset;
set lefslib.compzrednimainRsel;
interval = age - baseage;
cbaseage = baseage - 55;
if id < 5000 then sex = 1;
if id >= 5000 then sex = 0;
drop emswave dov emsid psorder blsayr protocol fhxdone mridone petdone pettask status;
run;

* Export lefslib.compzrednimainRsel as .csv;
PROC EXPORT data = lefslib.compzrednimainRsel 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzrednimainRsel.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
