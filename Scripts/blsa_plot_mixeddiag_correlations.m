function blsa_plot_mixeddiag_correlations(B_long,B_cross,varinfo,v,poptions,I,pt)

% Combine correlation plots from cross-sectional and longitudinal
% associations into one matrix with complementary (non-redundant)
% diagonals.
%
% Usage: blsa_plot_mixeddiag_correlations(B_long,B_cross,varinfo,v,poptions,I,pt)
%
% B_long  - n x m matrix of longitudinal estimates.
% B_cross - n x m matrix of cross-sectional estimates.
% varinfo - structure with variable V and varnames.
% v       - vector of variable order.
% poptions - plot options structure with fields gca, gcf.
% I       - flag 1 for plot intensity matrix, 0 for none (default).
% pt      - p-value threshold, default 1 i.e. no threshold.
%
% Created by Josh Goh 03032011.

%% ADDITIONAL ALLOCATIONS (Most already pre-loaded in blsa_print_figures.m)
V = varinfo.varnames;

% Reorder variable names
for i=1:length(v)
    varnames(i) = V(v(i));
end

% General plot options
if ~isempty(poptions.gca.Position),aPos = poptions.gca.Position;else aPos = [.3,.02,.6,.65]; end;
if ~isempty(poptions.gca.vnxy),vnxy = poptions.gca.vnxy;else vnxy = [-.1 -.1]; end;
if ~isempty(poptions.gca.vnfs),vnfs = poptions.gca.vnfs;else vnfs = 10; end;
if ~isempty(poptions.gca.titleY),titleY = poptions.gca.titleY;else titleY = -5; end;
if ~isempty(poptions.gcf.Position),fPos = poptions.gcf.Position;else fPos = [0,300,500,510]; end;
if ~isempty(poptions.cbarh.Position),cbarhPos = poptions.cbarh.Position;else cbarhPos = [.93 .2 .01 .4]; end;
if ~isempty(poptions.cbarh.Rxy),cbarhRxy = poptions.cbarh.Rxy;else cbarhRxy = [12.1 1]; end;

% Setting for Longitudinal (1) and Cross-sectional (2) data
Y(1).pair = 'all';
Y(2).pair = 'pairwise';

% Set variable beta estimates for correlating
Y(1).B = B_long; Y(2).B = B_cross;

%% GET CROSS CORRELATIONS
for i=1:2 % Loop longitudinal and cross-sectional data
    [Y(i).R Y(i).P] = blsa_crosscorrelations(Y(i).B,varinfo,v,poptions,I,pt,Y(i).pair);
end

% Merge diagonals of R1 and R2, P1 and P2
R = tril(Y(2).R) + triu(Y(1).R) - eye(size(Y(1).R,1));
P = tril(Y(2).P) + triu(Y(1).P) - eye(size(Y(1).P,1));

% Thresholding
R(find(P>pt)) = 1.1;

% Plot cross-correlation matrix.
colormap(jet);
colmap = colormap;
if pt~=1
    gv = .8;
    colmap(length(colmap)+1,:) = [gv gv gv];
end
colormap(colmap);

h = image(R,'CDataMapping','Scaled');

% Overlay gridlines
hold on;
for l = [0 1.5:size(R,2)+.5]
    plot([l l],[0 size(R,1)+.5],'-k','LineWidth',.5);
    plot([0 size(R,2)+.5],[l l],'-k','LineWidth',.5);
end
hold off

% Figure object settings
Xlim = get(gca,'Xlim');
Ylim = get(gca,'Ylim');

set(gca,...
    'LineWidth',1,...
    'CLim',[-1 max(max(R))]);
set(h,'UserData',V);   
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@blsa_plot_crosscorrelations_update_datacursor);

% Title
text(mean(Xlim),...
    Ylim(1)-titleY,...
    'Cross-Correlations',...
    'FontName','Arial',...
    'FontWeight','bold',...
    'FontSize',14,...
    'HorizontalAlignment','center');
set(gcf,'Color',[1 1 1],...
    'Position',fPos);
set(gca,'YDir','reverse',...
    'XAxisLocation','top',...
    'XTick',[],...
    'YTick',[],...
    'ZTick',[],...
    'Position',aPos);

% Variable name axis labels
for i = 1:length(V)
    text(vnxy(1),i,varnames{i},...
        'FontName','Arial',...
        'FontSize',vnfs,...
        'HorizontalAlignment','right',...
        'VerticalAlignment','middle');

    text(i,vnxy(2),varnames{i},...
        'FontName','Arial',...
        'FontSize',vnfs,...
        'HorizontalAlignment','left',...
        'VerticalAlignment','middle',...
        'Rotation',90);

end

% Colorbar
cbarh = colorbar;
set(cbarh,'Position',cbarhPos);
set(cbarh,'YLim',[-1 1]);
text(cbarhRxy(1),cbarhRxy(2),'R','FontName','Arial','FontSize',10,'FontWeight','bold');