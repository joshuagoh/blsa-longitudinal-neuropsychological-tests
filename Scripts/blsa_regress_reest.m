function O = blsa_regress_reest(B)

% Regresses the slope estimates of random effects estimates of interval on 
% baseline age. B is generated from SAS blsa_mixedmodel_interval.sas and
% then Matlab blsa_extract_csv_battery_SAS_est.m. In general B contains id 
% values as 1st column, baseline age at first visit as 2nd column, followed
% by columns of the random effects estimates of interval slopes.
%
% Usage O = blsa_regress_reest(B)
%
% Writes M4fest.csv containing O as well.
%
% Created by Josh Goh 10 Nov 2010.

nb = 2; % No. of coefficients.
bage = B(:,2);

for i=3:size(B,2);
    [~,~,stats] = glmfit(bage,B(:,i),'normal'); % Note 1st beta is constant.
    
    for j = 1:nb
        O(i-2,1+(j-1)*5) = stats.beta(j);
        O(i-2,2+(j-1)*5) = stats.se(j);
        O(i-2,3+(j-1)*5) = stats.dfe;
        O(i-2,4+(j-1)*5) = stats.t(j);
        O(i-2,5+(j-1)*5) = stats.p(j);
    end
    
end

csvwrite('oM4fest.csv',O);