function [R P Z FZ SEZ] = blsa_crosscorrelations(B,varinfo,v,poptions,I,pt,Rtype)

% Cross-correlates values in B (n x m matrix from
% blsa_extract_csv_battery_SAS_est.m or raw scores). Plots R matrix of intensities.
%
% Usage [R P Z FZ SEZ] = blsa_crosscorrelations(B,varinfo,v,poptions,I,pt,Rtype)
%
% R       - output R matrix of cross-correlations
% P       - output P matrix of significance values
% Z       - output Z of R
% FZ      - output Fisher Z of R
% SEZ     - output Fisher SE of Z
% B       - n x m matrix of estimates.
% varinfo - structure with variable V and varnames.
% v       - vector of variable order.
% poptions - plot options structure with fields gca, gcf.
% I       - flag 1 for plot intensity matrix, 0 for none (default).
% pt      - p-value threshold, default 1 i.e. no threshold.
% Rtype   - 'all' default or 'pairwise', to specify dealing with NaN in correlation.
%
% Modified from blsa_zred3crosscorrelations.m by Josh Goh 03032011.

% Allocations
if nargin<6
    pt = 1;
    Rtype = 'all';
elseif nargin<7
    Rtype = 'all';
end

odataV = varinfo.odataV;
varvec = varinfo.varvec;
V = varinfo.varnames;

% General plot options
if ~isempty(poptions.gca.Position),aPos = poptions.gca.Position;else aPos = [.3,.02,.6,.65]; end;
if ~isempty(poptions.gca.vnxy),vnxy = poptions.gca.vnxy;else vnxy = [-.1 -.1]; end;
if ~isempty(poptions.gca.vnfs),vnfs = poptions.gca.vnfs;else vnfs = 10; end;
if ~isempty(poptions.gca.titleY),titleY = poptions.gca.titleY;else titleY = -5; end;
if ~isempty(poptions.gcf.Position),fPos = poptions.gcf.Position;else fPos = [0,300,500,510]; end;
if ~isempty(poptions.cbarh.Position),cbarhPos = poptions.cbarh.Position;else cbarhPos = [.93 .2 .01 .4]; end;
if ~isempty(poptions.cbarh.Rxy),cbarhRxy = poptions.cbarh.Rxy;else cbarhRxy = [12.1 1]; end;


% Reorder data
for i=1:length(v)
    D(:,i) = B(:,v(i)+2);
    varnames(i) = V(v(i));
end

% Compute R and Fisher Z tranform
[R P] = corr(D,'rows',Rtype);
SEZ = 1/sqrt(size(D,1)-3);
FZ = atanh(R);
Z = FZ./SEZ;

% Thresholding
R(find(P>pt)) = 1.1;

if I==1
    
    % Plot cross-correlation matrix.
    %close
    colormap(jet);
    colmap = colormap;
    if pt~=1
        gv = .8;
        colmap(length(colmap)+1,:) = [gv gv gv];
    end
    colormap(colmap);
        
    h = image(R,'CDataMapping','Scaled');
    
    % Overlay gridlines
    hold on;
    for l = [0 1.5:size(R,2)+.5]
        plot([l l],[0 size(R,1)+.5],'-k','LineWidth',.5);
        plot([0 size(R,2)+.5],[l l],'-k','LineWidth',.5);
    end
    hold off
    
    % Figure object settings        
    Xlim = get(gca,'Xlim');
    Ylim = get(gca,'Ylim');
    
    set(gca,...
        'LineWidth',1,...
        'CLim',[-1 max(max(R))]);
    set(h,'UserData',varnames);   
    dcm_obj = datacursormode(gcf);
    set(dcm_obj,'UpdateFcn',@blsa_plot_crosscorrelations_update_datacursor);
    
    % Title
    text(mean(Xlim),...
        Ylim(1)-titleY,...
        'Cross-Correlations',...
        'FontName','Arial',...
        'FontWeight','bold',...
        'FontSize',14,...
        'HorizontalAlignment','center');
    set(gcf,'Color',[1 1 1],...
        'Position',fPos);
    set(gca,'YDir','reverse',...
        'XAxisLocation','top',...
        'XTick',[],...
        'YTick',[],...
        'ZTick',[],...
        'Position',aPos);
        
    % Variable name axis labels
    for i = 1:length(v)
        text(vnxy(1),i,varnames{i},...
            'FontName','Arial',...
            'FontSize',vnfs,...
            'HorizontalAlignment','right',...
            'VerticalAlignment','middle');
        
        text(i,vnxy(2),varnames{i},...
            'FontName','Arial',...
            'FontSize',vnfs,...
            'HorizontalAlignment','left',...
            'VerticalAlignment','middle',...
            'Rotation',90);
        
    end
    
    % Colorbar
    cbarh = colorbar;
    set(cbarh,'Position',cbarhPos);
    set(cbarh,'YLim',[-1 1]);
    text(cbarhRxy(1),cbarhRxy(2),'R','FontName','Arial','FontSize',10,'FontWeight','bold');
    
end