function blsa_process_battery(dbvec,prnt,flag)

% dbvec: choose which database(s) to run;
% prnt: print figures (1) or not (0).
% flag: choose which figures to print - 'all','scatterplots','betas','correlations';

if nargin < 1;
    dbvec = 1;
    prnt = 1;
    flag = 'all';
elseif nargin < 2;
    prnt = 1;
    flag = 'all';
elseif nargin < 3;
    flag = 'all';
end

warning('OFF');
addpath /Users/gohjo/Research/Projects/BLSA/Battery/Scripts/

for db = dbvec; % Set database for subsequent processes to operate on.

lefslibdir = '/Users/gohjo/Research/Projects/BLSA/Battery/Data/lefslib/';

dbase = {'nimain1',...
         'zredndnimain1',...
         'compzredndnimain1',...
         'zredndg1nimain1',...
         'compzredndg1nimain1',...
         'zredndg2nimain1',...
         'compzredndg2nimain1'};
    
nimain = [lefslibdir dbase{db} '.csv'];
iccest = [lefslibdir dbase{db} 'ICCest.csv'];
m1fest = [lefslibdir dbase{db} 'M1fest.csv'];
m1rest = [lefslibdir dbase{db} 'M1rest.csv'];
m2fest = [lefslibdir dbase{db} 'M2fest.csv'];
m2rest = [lefslibdir dbase{db} 'M2rest.csv'];

datafname = sprintf('%s_DATA.mat',dbase{db});
estfname  = sprintf('%s_ESTIMATE.mat',dbase{db});

cd('/Users/gohjo/Research/Projects/BLSA/Battery/Analysis');

%% Read csv output from SAS into matlab RAW DATA variables
[D S C O] = blsa_read_csv_battery_data(nimain,db);
varvec = O.varvec;

%% Compute first available scores (for cross-correlation of cross-sectional age).
B = blsa_extract_firstscore(C,varvec);

%% RELIABILITY
if db<=3
    iccR = blsa_battery_icc_reliability(iccest,db);
else
    iccR = [];
end

%% Extract Estimates

if db<=3; % Non-DG
    
    %% MODEL 1 - y = intercept(f) + interval(f) + intercept(r) + intercept(r)
    
    % Extract model fixed estimates
    [m1fB0 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','Intercept');
    [m1fB1 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','interval');
    
    % Extract model random estimates
    [m1rb0 m1rV] = blsa_extract_csv_battery_SAS_est(m1rest,db,'rand','Intercept');
    [m1rb1 m1rV] = blsa_extract_csv_battery_SAS_est(m1rest,db,'rand','interval');
    
    % Reorganize SAS csv output into matlab matrix
    m1fo = blsa_reorg_est_csv(m1fest,db);
    
    
    %% MODEL 2 - y = intercept(f) + cbaseage(f) + interval(f) cbaseage*interval(f) + intercept(r) + intercept(r)
    
    % Extract model fixed estimates
    [m2fB0 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','Intercept');
    [m2fB1 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage');
    [m2fB2 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','interval');
    [m2fB3 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage*interval');
    
    % Extract model random estimates
    [m2rb0 m2rV] = blsa_extract_csv_battery_SAS_est(m2rest,db,'rand','Intercept');
    [m2rb1 m2rV] = blsa_extract_csv_battery_SAS_est(m2rest,db,'rand','interval');
    
    % Reorganize SAS csv output into matlab matrix
    m2fo = blsa_reorg_est_csv(m2fest,db);
    
else % DG
    
    %% MODEL 1 - y = intercept(f) + interval(f) + dg(f) + interval*dg(f) + intercept(r) + intercept(r)
    
    % Extract model fixed estimates
    [m1fB0 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','Intercept');
    [m1fB1 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','dg');
    [m1fB2 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','interval');
    [m1fB3 m1fV] = blsa_extract_csv_battery_SAS_est(m1fest,db,'fixed','interval*dg');
    
    % Extract model random estimates
    [m1rb0 m1rV] = blsa_extract_csv_battery_SAS_est(m1rest,db,'rand','Intercept');
    [m1rb1 m1rV] = blsa_extract_csv_battery_SAS_est(m1rest,db,'rand','interval');
    
    % Reorganize SAS csv output into matlab matrix
    m1fo = blsa_reorg_est_csv(m1fest,db);
    
    
    %% MODEL 2 - y = intercept(f) + cbaseage(f) + cbaseage*dg(f) + cbaseage*interval(f) + cbaseage*interval*dg(f) + dg(f) + interval(f) + interval*dg(f) + intercept(r) + intercept(r)
    
    % Extract model fixed estimates
    [m2fB0 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','Intercept');
    [m2fB1 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage');
    [m2fB2 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage*dg');
    [m2fB3 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage*interval');
    [m2fB4 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','cbaseage*interval*dg');
    [m2fB5 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','dg');
    [m2fB6 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','interval');
    [m2fB7 m2fV] = blsa_extract_csv_battery_SAS_est(m2fest,db,'fixed','interval*dg');
    
    % Extract model random estimates
    [m2rb0 m2rV] = blsa_extract_csv_battery_SAS_est(m2rest,db,'rand','Intercept');
    [m2rb1 m2rV] = blsa_extract_csv_battery_SAS_est(m2rest,db,'rand','interval');
    
    % Reorganize SAS csv output into matlab matrix
    m2fo = blsa_reorg_est_csv(m2fest,db);
    
end

% Reorder B according to m1rV
Bvn = C.varnames(varvec(1):varvec(2));
Bn(:,1:2) = B(:,1:2);
for i=3:length(m1rV)
    vj = strmatch(m1rV(i),Bvn) + 2;
    Bn(:,i) = B(:,vj);
end
B = Bn;

save(datafname,'D','S','C','B','O');
save(estfname,'m1*','m2*','iccR');

clear Bvn Bn vj;

%--------------------------------------------------------------------------
%% PRINT FIGURES
if prnt
    blsa_print_figures(db,flag); % Note, plot settings in blsa_read_csv_switcher.m
end
%clear all

cd('/Users/gohjo/Research/Projects/BLSA/Battery/Analysis');

end