function S = blsa_plot_spaghetti_pertest(S,subj,yvar,xvar,pred,ptype,dement,ntp)

% Usage: S = blsa_plot_spaghetti_pertest(S,subj,yvar,xvar,pred,ptype,dement,ntp)
%
% Plot spaghetti plot of neuropsych scores for specified subject(s), a
% specified test (yvar) against another variable(xvar), and a specified
% length of timepoints.
% Outputs updated S with estimates B if prediction is used (see below).
%
% S     - S structure variable containing data (default generated from
%        blsa_read_csv_battery_data)
% subj  - 'all' or vector of BLSA id numbers
% yvar  - string specify neuropsych test to plot, see numbered list:
%   1  id           31	cvlfrl      61	fluctp      91	dxad
%   2  EMSid        32	cvlcrl      62	flulti      92	pfaq01
%   3  emsfn        33	cvlrs1      63	flultp      93	pfaq02
%   4  sex          34	cvlrs2      64	flucfw      94	pfaq03
%   5  vi           35	cvlrs3      65	flucfp      95	pfaq04
%   6  emsvi        36	cvlrs4      66	flucfi      96	pfaq05
%   7  EMSwave      37	cvlrs5      67	flucaw      97	pfaq06
%   8  baseage      38	cvlrst      68	flucap      98	pfaq07
%   9  age          39	cvlrx1      69	flucai      99	pfaq08
%   10 cbaseage     40	cvlrx2      70	flucvw      100	pfaq09
%   11 AgeYrs       41	cvlrx3      71	flucvp      101	pfaq10
%   12 interval     42	cvlrx4      72	flucvi		
%   13 DOV          43	cvlrx5      73	flulfw		
%   14 DOT          44	cvlrxt      74	flulfp		
%   15 mridone      45	cvlbet      75	flulfi		
%   16 petdone      46	cvltpe      76	flulaw		
%   17 trats        47	cvltif      77	flulap
%   18 trbts        48	cvltic      78	flulai
%   19 trba         49	cvldis      79	flulsw
%   20 alphas       50	cvlrbi      80	flulsp
%   21 simtot       51	cvlsv5      81	flulsi
%   22 cvltca       52	cvllv5      82	pmato3
%   23 cvlca1       53	cvlsvs      83	digbac
%   24 cvlca2       54	cvllvs      84	digfor
%   25 cvlca3       55	cvllvl      85	crdrot
%   26 cvlca4       56	flucat      86	boscor
%   27 cvlca5       57	flulet      87	bvrtot
%   28 cvltcb       58	fluctw      88	donset
%   29 cvlfrs       59	flultw      89	dodx
%   30 cvlcrs       60	flucti      90	dx
%
% xvar - x-axis variable, default 'age'.
% pred - specify to type of data estimation plot
%        1. 'raw'      : plots direct values for each specified subject,
%                        default.
%        2. 'predlin'  : plots linear predicted values for each specified
%                        subject, requires Statistics toolbox.
%        3. 'prednlin' : plots nonlinear (currently quadratic) predicted
%                        values for each specified subject, requires
%                        Statistics toolbox.
% ptype  - type of figure 's': spaghetti,'sh': spaghetti & histogram (default),'none'.
% dement - subj id with CI.
% ntp    - max. no. of time points to plot, default 99
%
% Created by Josh Goh 28 Sept 2010
% Modified by Josh Goh 1 Oct 2010
% Modified by Josh Goh 20 Oct 2010 - updated variables, added prediction
%                                    plots.
% Modified by Josh Goh 25 Oct 2010 - added dementia diagnosis and daily
%                                    function (Pfeffer).
% Modified by Josh Goh 27 Oct 2010 - added ptype.
% Updated by Josh Goh 2 Nov 2010 - included sex, baseage, interval.
% Updated by Josh Goh 10 Nov 2010 - added centered cbaseage.
% Updated by Josh Goh 16 Nov 2010 - added verbal fluency #words, #persev, #instrus.
% Modified by Joshua Goh 18 Nov 2010 - added boscor.
% Modified by Joshua Goh 30 Nov 2010 - added fluctw flultw flucti fluctp flulti flultp.
% Modified by Josh Goh 1 Dec 2010 - Added trba, cvlsv5, cvllv5, cvlsvs, cvllvs, cvllvl; Removed trate, trbte, cvlbei.
% Modified by Joshua Goh 13 Dec 2010 - added dementia diagnosis.
% Modified by Joshua Goh 03032011 - moved dementia vector to
%                                   blsa_print_figures.m

% Allocations
V = S.varnames;
scode = vertcat(S.subj.id);

if strcmp('all',subj);
    subj = scode;
end
srange = 1:length(subj);
k = strmatch(yvar,V,'exact');

if nargin<4
    xvar = strmatch('age',V);
    pred = 'raw';
    ptype = 'sh';
    dement = [];
    ntp=99;
elseif nargin<5
    xvar = strmatch(xvar,V);
    pred = 'raw';
    ptype = 'sh';
    dement = [];
    ntp=99;
elseif nargin<6
    xvar = strmatch(xvar,V);
    ptype = 'sh';
    dement = [];
    ntp=99;
elseif nargin<7
    xvar = strmatch(xvar,V);
    dement = [];
    ntp=99;
elseif nargin<8
    xvar = strmatch(xvar,V);
    ntp=99;
end

Yhist = [];

% Loop visits per subj
for i=srange; % Subj loop
    I = find(scode==subj(i));
    endtp = min(ntp,length(S.subj(I).variable(xvar).visvalue));
    Y = S.subj(I).variable(k).visvalue(1:endtp); % Get variable visit values
    Yhist = [Yhist Y];
    X = S.subj(I).variable(xvar).visvalue(1:endtp); % Get x variable values
    
    % Plot graphs
    if strcmp('sh',ptype);
        subplot(2,1,1);
    end
    if any(dement==subj(i))
        %color = 'r'; lw = 2;
        color = [1 0 0]; lw = 1;
    else
        %color = 'k'; lw = 1;
        color = [0 0 0]; lw = 1;
    end
    switch pred
        case 'raw' % Plot subject raw data line(s)
            h = plot(X,Y,'-o','Color',color,'LineWidth',lw,...
                'MarkerEdgeColor',color,...
                'MarkerSize',3);
                %'MarkerFaceColor',color);
            
        case 'predlin' % Plot subject linear predicted line(s)
            if length(Y)==1
                h = plot(X,Y,'o',...
                    'MarkerEdgeColor',color,...
                    'MarkerSize',2,...
                    'MarkerFaceColor',color);
            elseif any(~isnan(Y))
                [X Y B] = getsubjlinpred(X',Y');
                h = plot(X,Y,'-','Color',color,'LineWidth',lw);
                S.subj(I).variable(k).B = B;
            end
            
        case 'prednlin' % Plot subject nonlinear predicted line(s)
            if length(Y)==1
                h = plot(X,Y,'o',...
                    'MarkerEdgeColor',color,...
                    'MarkerSize',2,...
                    'MarkerFaceColor',color);
            elseif any(~isnan(Y))
                [X Y B] = getsubjnlinpred(X',Y');
                h = plot(X,Y,['-' color],'LineWidth',lw);
                S.subj(I).variable(k).B = B;
            end
    end
    
    set(h,'Tag',['SNum: ' num2str(i) ' BLSA id: ' num2str(subj(i))]);
    hold on;
    clear X Y;
    
end

% Graph visual properties
set(gcf,'Color',[1 1 1]);
title(['Raw'],'FontName','Arial','FontWeight','bold','FontSize',14);
ylabel(V(k),'FontName','Arial','FontWeight','bold');
xlabel(V(xvar),'FontName','Arial','FontWeight','bold');
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@blsa_plot_spaghetti_update_datacursor);

% set(gca,'FontName','Arial','FontWeight','bold');
hold off;

% Plot histogram
if strcmp('sh',ptype);
    subplot(2,1,2);
    hist(Yhist,20);
    title('Histogram','FontName','Arial','FontWeight','bold','FontSize',14);
    xlabel([V(k) ' Score'],...
        'FontName','Arial','FontWeight','bold');
    ylabel([V(k) ' Frequency'],'FontName','Arial','FontWeight','bold');
    h2 = findobj(gca,'Type','patch');
    set(h2,'FaceColor','k');
end

% Switch off figures
if strcmp('none',ptype)
    close all
end

%--------------------------------------------------------------------------
% Subfunctions
% Obtain subject linear predicted values
function [X Y B] = getsubjlinpred(X,Y)
XC = [X ones(size(X,1),1)];
B = regress(Y,XC);
Y = B'*XC';

% Obtain subject nonlinear predicted values (Currently quadratic function)
function [X Y B] = getsubjnlinpred(X,Y)
ncoeff = 3; % For quadratic, 3 coefficients.
b = ones(ncoeff,1);
FX=@(b,X) b(1).*X.^2 + b(2).*X + b(3);
B = nlinfit(X,Y,FX,b);
Y = FX(B,X);
