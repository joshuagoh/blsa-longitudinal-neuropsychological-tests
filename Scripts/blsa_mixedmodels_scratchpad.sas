ODS TRACE ON;
* Original mixed model (full, model 1);
PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL trats = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION; 
RUN;

* Original mixed model (reduced, model 2);
PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL trats = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION; 
RUN;
ODS TRACE OFF;

* Mixed model for ICC;
ODS TRACE ON;
PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL trats = /DDFM = kr SOLUTION;
			RANDOM intercept/ TYPE=un SUBJECT=id SOLUTION; 
RUN;
ODS TRACE OFF;

* Set covariances from covtest;
PROC MIXED data=lefslib.nimain noprofile;
			CLASS id;
			MODEL cvlrxt = age /DDFM = kr SOLUTION;
			RANDOM intercept age / TYPE=un SUBJECT=id SOLUTION;
			parms (5.3264) (-0.3004) (0.009542) (1.1712) /; 
RUN;


* Variables: bias cap efa efd efi efr efs em ltm sd sp ver vis
* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL bias = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndblsam1FEbias;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndblsam1REbias;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndblsam1FEbias (RENAME = (Estimate=Estbias StdErr=sebias DF=DFbias tValue=tbias Probt=ptbias));
		SET lefsest.zred3ndblsam1FEbias;
		PROC SORT DATA = lefsest.zred3ndblsam1FEbias; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndblsam1REbias (RENAME = (Estimate=Estbias));
		SET lefsest.zred3ndblsam1REbias;
		PROC SORT DATA = lefsest.zred3ndblsam1REbias; by id; RUN;



* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL bias = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndblsam2FEbias;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndblsam2REbias;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndm2FEbias (RENAME = (Estimate=Estbias StdErr=sebias DF=DFbias tValue=tbias Probt=ptbias));
		SET lefsest.zred3ndm2FEbias;
		PROC SORT DATA = lefsest.zred3ndm2FEbias; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndm2REbias (RENAME = (Estimate=Estbias));
		SET lefsest.zred3ndm2REbias;
		PROC SORT DATA = lefsest.zred3ndm2REbias; by id; RUN;
