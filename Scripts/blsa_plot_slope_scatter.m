function [x y] = blsa_plot_slope_scatter(M,V,xvar,yvar)

% Plots scatterplot of slope effects of interval (longitudinal change: in
% M, an n x m matrix) for yvar relative to xvar.
%
% Usage [x y] = blsa_plot_slope_scatter(M,yvar,xvar)
%
% M - n x m matrix of slopes from blsa_extract_csv_battery_SAS_est.m (note
%     1st 2 columns are id and baseage).
% V - rV from blsa_extract_csv_battery_SAS_est.m
% xvar - string of test for x axis.
% yvar - string of test for y axis.
% x    - output x values
% y    - output y values
%
% Created by Josh Goh 7 Dec 2010.

yi = strmatch(yvar,V);
xi = strmatch(xvar,V);
x = M(:,xi);
y = M(:,yi);

plot(x,y,...
    'o',...
    'MarkerEdgeColor','k',...
    'MarkerSize',2,...
    'MarkerFaceColor','k');

% Graph visual properties
set(gcf,'Color',[1 1 1]);
title(['Scatterplot'],'FontName','Arial','FontWeight','bold','FontSize',14);
ylabel(V(yi),'FontName','Arial','FontWeight','bold');
xlabel(V(xi),'FontName','Arial','FontWeight','bold');