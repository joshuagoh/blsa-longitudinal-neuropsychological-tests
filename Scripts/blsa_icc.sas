option nofmterr; run; * Switch off format errors ;
/* Compute ICC from Mixed Models for BLSA data lefs.nimain.

Created by Joshua Goh 28 Oct 2010;
Modified by Josh Goh 30 Nov 2010 - Added fluctw flultw flucti fluctp flulti flultp.
Modified by Josh Goh 1 Dec 2010 - Added trba, cvlsv5, cvllv5, cvlsvs, cvllvs, cvllvl; Removed trate, trbte, cvlbei.
Modified by Josh Goh 10 Dec 2010 - Changed code to operate on Zred3nimain.
Script modified by Josh Goh 26 Jan 2011 - Changed code to operate on Zred3ndrnimain.
*/

* Create macro to loop cognitive test variables (Operate on lefslib.nimain);
%macro mixedmodel(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.ICCcov&dv;
		PROC SORT data = lefsest.ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodel(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);

* Merge variables into lefslib;
DATA lefslib.ICCest;
MERGE lefsest.Icccovtrats lefsest.Icccovtrbts lefsest.Icccovtrate lefsest.Icccovtrbte lefsest.Icccovalphas lefsest.Icccovsimtot lefsest.Icccovcvltca lefsest.Icccovcvlca1 lefsest.Icccovcvlca2 lefsest.Icccovcvlca3 lefsest.Icccovcvlca4 lefsest.Icccovcvlca5 lefsest.Icccovcvltcb lefsest.Icccovcvlfrs lefsest.Icccovcvlcrs lefsest.Icccovcvlfrl lefsest.Icccovcvlcrl lefsest.Icccovcvlrs1 lefsest.Icccovcvlrs2 lefsest.Icccovcvlrs3 lefsest.Icccovcvlrs4 lefsest.Icccovcvlrs5 lefsest.Icccovcvlrst lefsest.Icccovcvlrx1 lefsest.Icccovcvlrx2 lefsest.Icccovcvlrx3 lefsest.Icccovcvlrx4 lefsest.Icccovcvlrx5 lefsest.Icccovcvlrxt lefsest.Icccovcvlbet lefsest.Icccovcvltpe lefsest.Icccovcvltif lefsest.Icccovcvltic lefsest.Icccovcvldis lefsest.Icccovcvlrbi lefsest.Icccovcvlsv5 lefsest.Icccovcvllv5 lefsest.Icccovcvlsvs lefsest.Icccovcvllvs lefsest.Icccovcvllvl lefsest.Icccovflucat lefsest.Icccovflulet lefsest.Icccovfluctw lefsest.Icccovflultw lefsest.Icccovflucti lefsest.Icccovfluctp lefsest.Icccovflulti lefsest.Icccovflultp lefsest.Icccovflucfw lefsest.Icccovflucfp lefsest.Icccovflucfi lefsest.Icccovflucaw lefsest.Icccovflucap lefsest.Icccovflucai lefsest.Icccovflucvw lefsest.Icccovflucvp lefsest.Icccovflucvi lefsest.Icccovflulfw lefsest.Icccovflulfp lefsest.Icccovflulfi lefsest.Icccovflulaw lefsest.Icccovflulap lefsest.Icccovflulai lefsest.Icccovflulsw lefsest.Icccovflulsp lefsest.Icccovflulsi lefsest.Icccovpmato3 lefsest.Icccovdigbac lefsest.Icccovdigfor lefsest.Icccovcrdrot lefsest.Icccovboscor lefsest.Icccovbvrtot;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Operate on lefslib.zred3nimain;
%macro mixedmodelzred3(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zred3nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zred3ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zred3ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zred3ICCcov&dv;
		PROC SORT data = lefsest.zred3ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzred3(bias cap efa efd efi efr efs em ltm sd sp ver vis);
DATA lefslib.zred3ICCest;
MERGE lefsest.zred3ICCcovbias lefsest.zred3ICCcovcap lefsest.zred3ICCcovefa lefsest.zred3ICCcovefd lefsest.zred3ICCcovefi lefsest.zred3ICCcovefr lefsest.zred3ICCcovefs lefsest.zred3ICCcovem lefsest.zred3ICCcovltm lefsest.zred3ICCcovsd lefsest.zred3ICCcovsp lefsest.zred3ICCcovver lefsest.zred3ICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zred3ICC*est as .csv;
PROC EXPORT data = lefslib.zred3ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Operate on lefslib.zred3ndnimain;
%macro mixedmodelzred3nd(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zred3ndnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zred3ndICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zred3ndICCcov&dv;
		PROC SORT data = lefsest.zred3ndICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzred3nd(bias cap efa efd efi efr efs em ltm sd sp ver vis);
DATA lefslib.zred3ndICCest;
MERGE lefsest.zred3ndICCcovbias lefsest.zred3ndICCcovcap lefsest.zred3ndICCcovefa lefsest.zred3ndICCcovefd lefsest.zred3ndICCcovefi lefsest.zred3ndICCcovefr lefsest.zred3ndICCcovefs lefsest.zred3ndICCcovem lefsest.zred3ndICCcovltm lefsest.zred3ndICCcovsd lefsest.zred3ndICCcovsp lefsest.zred3ndICCcovver lefsest.zred3ndICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zred3ndICC*est as .csv;
PROC EXPORT data = lefslib.zred3ndICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Operate on lefslib.zred3ndrnimain;
%macro mixedmodelzred3ndr(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zred3ndrnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zred3ndrICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndrICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zred3ndrICCcov&dv;
		PROC SORT data = lefsest.zred3ndrICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzred3ndr(cap efm efi efa efd efr stm ltm sp ver vis);
DATA lefslib.zred3ndrICCest;
MERGE lefsest.zred3ndrICCcovcap lefsest.zred3ndrICCcovefm lefsest.zred3ndrICCcovefi lefsest.zred3ndrICCcovefa lefsest.zred3ndrICCcovefd lefsest.zred3ndrICCcovefr lefsest.zred3ndrICCcovstm lefsest.zred3ndrICCcovltm lefsest.zred3ndrICCcovsp lefsest.zred3ndrICCcovver lefsest.zred3ndrICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zred3ndrICC*est as .csv;
PROC EXPORT data = lefslib.zred3ndrICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
