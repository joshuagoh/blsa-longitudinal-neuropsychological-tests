% Compute root mean-square residuals between two standard deviation matrices

% RMR = blsa_rmr(SD1,SD2,st)
% std - 'raw','standardized'

function RMR = blsa_rmr(SD1,SD2,std)

nvar = size(SD1,1);

SDD = SD1-SD2;
SDD2 = SDD.^2;
tlSDD2 = tril(SDD2) + (SDD2.*eye(nvar)); % Lower triangle of symmetric matrix of sd deviations.

% Standardize residuals
SSDD2 = SDD2./(SD1.*SD2);
tlSSDD2 = tril(SSDD2) + (SSDD2.*eye(nvar));

if strcmp('raw',std)
    RMR = sqrt((2/(nvar*(nvar+1)))*sum(sum(tlSDD2)));
else
    RMR = sqrt((2/(nvar*(nvar+1)))*sum(sum(tlSSDD2)));
end