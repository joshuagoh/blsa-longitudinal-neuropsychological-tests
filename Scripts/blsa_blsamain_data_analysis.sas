option nofmterr; run; * Switch off format errors ;

* Setup raw, normalized, dementia coded data for full BLSA database. Requires that databases in blsa_merge_data.sas be ran.
* Run ICC on this database.
* Run mixed model analysis on this database.
* Modified from blsa_merge_data.sas by Josh Goh 21 Jan 2011.

* Setup lefslib.blsamain from lefslib.main;
data lefslib.blsamain; set lefslib.main;
drop psorder blsayr protocol fhxdone pettask status; * remove unnecessary info from blsamain;
run;

* Create Baseline Age and Interval variables. - Added Josh Goh 2 Nov 2010;
data lefslib.blsabaseline; set lefslib.blsamain;
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.blsabaseline; by id; run;
data lefslib.blsamain;
merge lefslib.blsamain lefslib.blsabaseline;
by id;
interval = age - baseage;
cbaseage = baseage - 17.2; * Center baseline age to minimum;
run;

* Reorder lefslib.blsamain to make more sense;
data lefslib.blsamain;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10;
set lefslib.blsamain;
run;

* Export lefslib.blsamain as .csv;
PROC EXPORT data = lefslib.blsamain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsamain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

/* Reduce, Standardize, and form Composite Scores*/
* Reduce tests;
data lefslib.zred3blsamain; set lefslib.blsamain;
yr = year(DOV);
keep id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot donset yr;
format trats alphas digbac digfor boscor bvrtot;
run;

* New database lefslib.zred3ndblsamain with data from dementia onwards removed.;
data lefslib.zred3ndblsamain; set lefslib.zred3blsamain;
if yr < donset | donset = .;
run;

* Standardize lefslib.zred3blsamain scores;
PROC STANDARD DATA=lefslib.zred3blsamain MEAN=0 STD=1 OUT=lefslib.zred3blsamain;
	VAR	trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot;
RUN;

* Composite scores from lefslib.zred3blsamain (Averaged: equal weighting from each sub-test, Sign changed so lower score is poorer performance);
data lefslib.zred3blsamain; set lefslib.zred3blsamain;

bias = cvlrbi;
cap = digfor;
efa = (simtot + cvlrst)/2;
efd = (-cvltif - cvlsv5 + cvldis)/3;
efi = (-cvltpe - fluctp - flultp)/3;
efr = (cvlfrs + cvlcrs + cvlcrl + fluctw)/4;
efs = (alphas + digbac - trba)/3;
em  = (cvlca1 + cvltca + cvltcb)/3;
ltm = (cvlfrl - cvllv5 - cvllvs)/3;
sd  = (-cvlsvs - cvllvl - cvlrxt)/3;
sp  = -trats;
ver = (boscor + pmato3 + flultw)/3;
vis = (-bvrtot + crdrot)/2;

label 	bias = 'bias'
		cap  = 'capacity'
		efa  = 'abstraction'
		efd  = 'discrimination'
		efi  = 'inhibition'
		efr  = 'retrieval'
		efs  = 'switching'
		em   = 'episodic memory'
		ltm  = 'long-term memory'
		sd   = 'stimulus dependency'
		sp   = 'speed'
		ver  = 'verbal'
		vis  = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot yr;
run;

data lefslib.zred3blsamain;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval bias cap efa efd efi efr efs em ltm sd sp ver vis donset;
set lefslib.zred3blsamain;
run;

* Export lefslib.zred3blsamain as .csv;
PROC EXPORT data = lefslib.zred3blsamain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsamain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Standardize lefslib.zred3ndblsamain scores;
PROC STANDARD DATA=lefslib.zred3ndblsamain MEAN=0 STD=1 OUT=lefslib.zred3ndblsamain;
	VAR	trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot;
RUN;

* Composite scores from lefslib.zred3ndblsamain (Averaged: equal weighting from each sub-test, Sign changed so lower score is poorer performance);
data lefslib.zred3ndblsamain; set lefslib.zred3ndblsamain;

bias = cvlrbi;
cap = digfor;
efa = (simtot + cvlrst)/2;
efd = (-cvltif - cvlsv5 + cvldis)/3;
efi = (-cvltpe - fluctp - flultp)/3;
efr = (cvlfrs + cvlcrs + cvlcrl + fluctw)/4;
efs = (alphas + digbac - trba)/3;
em  = (cvlca1 + cvltca + cvltcb)/3;
ltm = (cvlfrl - cvllv5 - cvllvs)/3;
sd  = (-cvlsvs - cvllvl - cvlrxt)/3;
sp  = -trats;
ver = (boscor + pmato3 + flultw)/3;
vis = (-bvrtot + crdrot)/2;

label 	bias = 'bias'
		cap  = 'capacity'
		efa  = 'abstraction'
		efd  = 'discrimination'
		efi  = 'inhibition'
		efr  = 'retrieval'
		efs  = 'switching'
		em   = 'episodic memory'
		ltm  = 'long-term memory'
		sd   = 'stimulus dependency'
		sp   = 'speed'
		ver  = 'verbal'
		vis  = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot yr;
run;

data lefslib.zred3ndblsamain;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval bias cap efa efd efi efr efs em ltm sd sp ver vis donset;
set lefslib.zred3ndblsamain;
run;

* Export lefslib.zred3ndblsamain as .csv;
PROC EXPORT data = lefslib.zred3ndblsamain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsamain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;



* RUN ICC
* Create macro to loop cognitive test variables (Operate on lefslib.blsamain);
%macro mixedmodel(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.blsaICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.blsaICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.blsaICCcov&dv;
		PROC SORT data = lefsest.blsaICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodel(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);

* Merge variables into lefslib;
DATA lefslib.blsaICCest;
MERGE lefsest.blsaIcccovtrats lefsest.blsaIcccovtrbts lefsest.blsaIcccovtrate lefsest.blsaIcccovtrbte lefsest.blsaIcccovalphas lefsest.blsaIcccovsimtot lefsest.blsaIcccovcvltca lefsest.blsaIcccovcvlca1 lefsest.blsaIcccovcvlca2 lefsest.blsaIcccovcvlca3 lefsest.blsaIcccovcvlca4 lefsest.blsaIcccovcvlca5 lefsest.blsaIcccovcvltcb lefsest.blsaIcccovcvlfrs lefsest.blsaIcccovcvlcrs lefsest.blsaIcccovcvlfrl lefsest.blsaIcccovcvlcrl lefsest.blsaIcccovcvlrs1 lefsest.blsaIcccovcvlrs2 lefsest.blsaIcccovcvlrs3 lefsest.blsaIcccovcvlrs4 lefsest.blsaIcccovcvlrs5 lefsest.blsaIcccovcvlrst lefsest.blsaIcccovcvlrx1 lefsest.blsaIcccovcvlrx2 lefsest.blsaIcccovcvlrx3 lefsest.blsaIcccovcvlrx4 lefsest.blsaIcccovcvlrx5 lefsest.blsaIcccovcvlrxt lefsest.blsaIcccovcvlbet lefsest.blsaIcccovcvltpe lefsest.blsaIcccovcvltif lefsest.blsaIcccovcvltic lefsest.blsaIcccovcvldis lefsest.blsaIcccovcvlrbi lefsest.blsaIcccovcvlsv5 lefsest.blsaIcccovcvllv5 lefsest.blsaIcccovcvlsvs lefsest.blsaIcccovcvllvs lefsest.blsaIcccovcvllvl lefsest.blsaIcccovflucat lefsest.blsaIcccovflulet lefsest.blsaIcccovfluctw lefsest.blsaIcccovflultw lefsest.blsaIcccovflucti lefsest.blsaIcccovfluctp lefsest.blsaIcccovflulti lefsest.blsaIcccovflultp lefsest.blsaIcccovflucfw lefsest.blsaIcccovflucfp lefsest.blsaIcccovflucfi lefsest.blsaIcccovflucaw lefsest.blsaIcccovflucap lefsest.blsaIcccovflucai lefsest.blsaIcccovflucvw lefsest.blsaIcccovflucvp lefsest.blsaIcccovflucvi lefsest.blsaIcccovflulfw lefsest.blsaIcccovflulfp lefsest.blsaIcccovflulfi lefsest.blsaIcccovflulaw lefsest.blsaIcccovflulap lefsest.blsaIcccovflulai lefsest.blsaIcccovflulsw lefsest.blsaIcccovflulsp lefsest.blsaIcccovflulsi lefsest.blsaIcccovpmato3 lefsest.blsaIcccovdigbac lefsest.blsaIcccovdigfor lefsest.blsaIcccovcrdrot lefsest.blsaIcccovboscor lefsest.blsaIcccovbvrtot;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.blsaICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Operate on lefslib.zred3blsamain;
%macro mixedmodelzred3(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zred3blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zred3blsaICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zred3blsaICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zred3blsaICCcov&dv;
		PROC SORT data = lefsest.zred3blsaICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzred3(bias cap efa efd efi efr efs em ltm sd sp ver vis);
DATA lefslib.zred3blsaICCest;
MERGE lefsest.zred3blsaICCcovbias lefsest.zred3blsaICCcovcap lefsest.zred3blsaICCcovefa lefsest.zred3blsaICCcovefd lefsest.zred3blsaICCcovefi lefsest.zred3blsaICCcovefr lefsest.zred3blsaICCcovefs lefsest.zred3blsaICCcovem lefsest.zred3blsaICCcovltm lefsest.zred3blsaICCcovsd lefsest.zred3blsaICCcovsp lefsest.zred3blsaICCcovver lefsest.zred3blsaICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zred3blsaICC*est as .csv;
PROC EXPORT data = lefslib.zred3blsaICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Operate on lefslib.zred3ndblsamain;
%macro mixedmodelzred3nd(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.zred3ndblsaICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndblsaICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.zred3ndblsaICCcov&dv;
		PROC SORT data = lefsest.zred3ndblsaICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelzred3nd(bias cap efa efd efi efr efs em ltm sd sp ver vis);
DATA lefslib.zred3ndblsaICCest;
MERGE lefsest.zred3ndblsaICCcovbias lefsest.zred3ndblsaICCcovcap lefsest.zred3ndblsaICCcovefa lefsest.zred3ndblsaICCcovefd lefsest.zred3ndblsaICCcovefi lefsest.zred3ndblsaICCcovefr lefsest.zred3ndblsaICCcovefs lefsest.zred3ndblsaICCcovem lefsest.zred3ndblsaICCcovltm lefsest.zred3ndblsaICCcovsd lefsest.zred3ndblsaICCcovsp lefsest.zred3ndblsaICCcovver lefsest.zred3ndblsaICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.zred3ndblsaICC*est as .csv;
PROC EXPORT data = lefslib.zred3ndblsaICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* RUN MIXED MODEL ANALYSIS

* Create macro to loop cognitive test variables (Operate on lefslib.blsamain);;
%macro mixedmodel(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.blsam1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.blsam1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.blsam1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.blsam1FE&dv;
		PROC SORT DATA = lefsest.blsam1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.blsam1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.blsam1RE&dv;
		PROC SORT DATA = lefsest.blsam1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.blsam2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.blsam2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.blsam2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.blsam2FE&dv;
		PROC SORT DATA = lefsest.blsam2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.blsam2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.blsam2RE&dv;
		PROC SORT DATA = lefsest.blsam2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.blsam3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.blsam3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.blsam3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.blsam3RE&dv;

		DATA lefsest.blsam3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.blsam3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodel(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);

* Sans non-convergent variables;
* %mixedmodel(trats trbts trate trbte alphas simtot cvltca cvlca2 cvlca3 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlbet cvlbei cvltpe cvltif cvltic cvldis cvlrbi flucat flulet pmato3 digbac digfor crdrot bvrtot);

* Non-convergent variables;
* %mixedmodel(cvlca1 cvlca4 cvlrxt);

* Merge variables into lefslib;
DATA lefslib.blsaM1fest;
MERGE lefsest.blsam1FEtrats lefsest.blsam1FEtrbts lefsest.blsam1FEtrba lefsest.blsam1FEalphas lefsest.blsam1FEsimtot lefsest.blsam1FEcvltca lefsest.blsam1FEcvlca1 lefsest.blsam1FEcvlca2 lefsest.blsam1FEcvlca3 lefsest.blsam1FEcvlca4 lefsest.blsam1FEcvlca5 lefsest.blsam1FEcvltcb lefsest.blsam1FEcvlfrs lefsest.blsam1FEcvlcrs lefsest.blsam1FEcvlfrl lefsest.blsam1FEcvlcrl lefsest.blsam1FEcvlrs1 lefsest.blsam1FEcvlrs2 lefsest.blsam1FEcvlrs3 lefsest.blsam1FEcvlrs4 lefsest.blsam1FEcvlrs5 lefsest.blsam1FEcvlrst lefsest.blsam1FEcvlrx1 lefsest.blsam1FEcvlrx2 lefsest.blsam1FEcvlrx3 lefsest.blsam1FEcvlrx4 lefsest.blsam1FEcvlrx5 lefsest.blsam1FEcvlrxt lefsest.blsam1FEcvlbet lefsest.blsam1FEcvltpe lefsest.blsam1FEcvltif lefsest.blsam1FEcvltic lefsest.blsam1FEcvldis lefsest.blsam1FEcvlrbi lefsest.blsam1FEcvlsv5 lefsest.blsam1FEcvllv5 lefsest.blsam1FEcvlsvs lefsest.blsam1FEcvllvs lefsest.blsam1FEcvllvl lefsest.blsam1FEflucat lefsest.blsam1FEflulet lefsest.blsam1FEfluctw lefsest.blsam1FEflultw lefsest.blsam1FEflucti lefsest.blsam1FEfluctp lefsest.blsam1FEflulti lefsest.blsam1FEflultp lefsest.blsam1FEflucfw lefsest.blsam1FEflucfp lefsest.blsam1FEflucfi lefsest.blsam1FEflucaw lefsest.blsam1FEflucap lefsest.blsam1FEflucai lefsest.blsam1FEflucvw lefsest.blsam1FEflucvp lefsest.blsam1FEflucvi lefsest.blsam1FEflulfw lefsest.blsam1FEflulfp lefsest.blsam1FEflulfi lefsest.blsam1FEflulaw lefsest.blsam1FEflulap lefsest.blsam1FEflulai lefsest.blsam1FEflulsw lefsest.blsam1FEflulsp lefsest.blsam1FEflulsi lefsest.blsam1FEpmato3 lefsest.blsam1FEdigbac lefsest.blsam1FEdigfor lefsest.blsam1FEcrdrot lefsest.blsam1FEboscor lefsest.blsam1FEbvrtot;
BY Effect;
RUN;

DATA lefslib.blsaM1rest;
MERGE lefslib.blsaBaseline lefsest.blsam1REtrats lefsest.blsam1REtrbts lefsest.blsam1REtrba lefsest.blsam1REalphas lefsest.blsam1REsimtot lefsest.blsam1REcvltca lefsest.blsam1REcvlca1 lefsest.blsam1REcvlca2 lefsest.blsam1REcvlca3 lefsest.blsam1REcvlca4 lefsest.blsam1REcvlca5 lefsest.blsam1REcvltcb lefsest.blsam1REcvlfrs lefsest.blsam1REcvlcrs lefsest.blsam1REcvlfrl lefsest.blsam1REcvlcrl lefsest.blsam1REcvlrs1 lefsest.blsam1REcvlrs2 lefsest.blsam1REcvlrs3 lefsest.blsam1REcvlrs4 lefsest.blsam1REcvlrs5 lefsest.blsam1REcvlrst lefsest.blsam1REcvlrx1 lefsest.blsam1REcvlrx2 lefsest.blsam1REcvlrx3 lefsest.blsam1REcvlrx4 lefsest.blsam1REcvlrx5 lefsest.blsam1REcvlrxt lefsest.blsam1REcvlbet lefsest.blsam1REcvltpe lefsest.blsam1REcvltif lefsest.blsam1REcvltic lefsest.blsam1REcvldis lefsest.blsam1REcvlrbi lefsest.blsam1REcvlsv5 lefsest.blsam1REcvllv5 lefsest.blsam1REcvlsvs lefsest.blsam1REcvllvs lefsest.blsam1REcvllvl lefsest.blsam1REflucat lefsest.blsam1REflulet lefsest.blsam1REfluctw lefsest.blsam1REflultw lefsest.blsam1REflucti lefsest.blsam1REfluctp lefsest.blsam1REflulti lefsest.blsam1REflultp lefsest.blsam1REflucfw lefsest.blsam1REflucfp lefsest.blsam1REflucfi lefsest.blsam1REflucaw lefsest.blsam1REflucap lefsest.blsam1REflucai lefsest.blsam1REflucvw lefsest.blsam1REflucvp lefsest.blsam1REflucvi lefsest.blsam1REflulfw lefsest.blsam1REflulfp lefsest.blsam1REflulfi lefsest.blsam1REflulaw lefsest.blsam1REflulap lefsest.blsam1REflulai lefsest.blsam1REflulsw lefsest.blsam1REflulsp lefsest.blsam1REflulsi lefsest.blsam1REpmato3 lefsest.blsam1REdigbac lefsest.blsam1REdigfor lefsest.blsam1REcrdrot lefsest.blsam1REboscor lefsest.blsam1REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.blsaM2fest;
MERGE lefsest.blsam2FEtrats lefsest.blsam2FEtrbts lefsest.blsam2FEtrba lefsest.blsam2FEalphas lefsest.blsam2FEsimtot lefsest.blsam2FEcvltca lefsest.blsam2FEcvlca1 lefsest.blsam2FEcvlca2 lefsest.blsam2FEcvlca3 lefsest.blsam2FEcvlca4 lefsest.blsam2FEcvlca5 lefsest.blsam2FEcvltcb lefsest.blsam2FEcvlfrs lefsest.blsam2FEcvlcrs lefsest.blsam2FEcvlfrl lefsest.blsam2FEcvlcrl lefsest.blsam2FEcvlrs1 lefsest.blsam2FEcvlrs2 lefsest.blsam2FEcvlrs3 lefsest.blsam2FEcvlrs4 lefsest.blsam2FEcvlrs5 lefsest.blsam2FEcvlrst lefsest.blsam2FEcvlrx1 lefsest.blsam2FEcvlrx2 lefsest.blsam2FEcvlrx3 lefsest.blsam2FEcvlrx4 lefsest.blsam2FEcvlrx5 lefsest.blsam2FEcvlrxt lefsest.blsam2FEcvlbet lefsest.blsam2FEcvltpe lefsest.blsam2FEcvltif lefsest.blsam2FEcvltic lefsest.blsam2FEcvldis lefsest.blsam2FEcvlrbi lefsest.blsam2FEcvlsv5 lefsest.blsam2FEcvllv5 lefsest.blsam2FEcvlsvs lefsest.blsam2FEcvllvs lefsest.blsam2FEcvllvl lefsest.blsam2FEflucat lefsest.blsam2FEflulet lefsest.blsam2FEfluctw lefsest.blsam2FEflultw lefsest.blsam2FEflucti lefsest.blsam2FEfluctp lefsest.blsam2FEflulti lefsest.blsam2FEflultp lefsest.blsam2FEflucfw lefsest.blsam2FEflucfp lefsest.blsam2FEflucfi lefsest.blsam2FEflucaw lefsest.blsam2FEflucap lefsest.blsam2FEflucai lefsest.blsam2FEflucvw lefsest.blsam2FEflucvp lefsest.blsam2FEflucvi lefsest.blsam2FEflulfw lefsest.blsam2FEflulfp lefsest.blsam2FEflulfi lefsest.blsam2FEflulaw lefsest.blsam2FEflulap lefsest.blsam2FEflulai lefsest.blsam2FEflulsw lefsest.blsam2FEflulsp lefsest.blsam2FEflulsi lefsest.blsam2FEpmato3 lefsest.blsam2FEdigbac lefsest.blsam2FEdigfor lefsest.blsam2FEcrdrot lefsest.blsam2FEboscor lefsest.blsam2FEbvrtot;
BY Effect;
RUN;

DATA lefslib.blsablsaM2rest;
MERGE lefslib.blsaBaseline lefsest.blsam2REtrats lefsest.blsam2REtrbts lefsest.blsam2REtrba lefsest.blsam2REalphas lefsest.blsam2REsimtot lefsest.blsam2REcvltca lefsest.blsam2REcvlca1 lefsest.blsam2REcvlca2 lefsest.blsam2REcvlca3 lefsest.blsam2REcvlca4 lefsest.blsam2REcvlca5 lefsest.blsam2REcvltcb lefsest.blsam2REcvlfrs lefsest.blsam2REcvlcrs lefsest.blsam2REcvlfrl lefsest.blsam2REcvlcrl lefsest.blsam2REcvlrs1 lefsest.blsam2REcvlrs2 lefsest.blsam2REcvlrs3 lefsest.blsam2REcvlrs4 lefsest.blsam2REcvlrs5 lefsest.blsam2REcvlrst lefsest.blsam2REcvlrx1 lefsest.blsam2REcvlrx2 lefsest.blsam2REcvlrx3 lefsest.blsam2REcvlrx4 lefsest.blsam2REcvlrx5 lefsest.blsam2REcvlrxt lefsest.blsam2REcvlbet lefsest.blsam2REcvltpe lefsest.blsam2REcvltif lefsest.blsam2REcvltic lefsest.blsam2REcvldis lefsest.blsam2REcvlrbi lefsest.blsam2REcvlsv5 lefsest.blsam2REcvllv5 lefsest.blsam2REcvlsvs lefsest.blsam2REcvllvs lefsest.blsam2REcvllvl lefsest.blsam2REflucat lefsest.blsam2REflulet lefsest.blsam2REfluctw lefsest.blsam2REflultw lefsest.blsam2REflucti lefsest.blsam2REfluctp lefsest.blsam2REflulti lefsest.blsam2REflultp lefsest.blsam2REflucfw lefsest.blsam2REflucfp lefsest.blsam2REflucfi lefsest.blsam2REflucaw lefsest.blsam2REflucap lefsest.blsam2REflucai lefsest.blsam2REflucvw lefsest.blsam2REflucvp lefsest.blsam2REflucvi lefsest.blsam2REflulfw lefsest.blsam2REflulfp lefsest.blsam2REflulfi lefsest.blsam2REflulaw lefsest.blsam2REflulap lefsest.blsam2REflulai lefsest.blsam2REflulsw lefsest.blsam2REflulsp lefsest.blsam2REflulsi lefsest.blsam2REpmato3 lefsest.blsam2REdigbac lefsest.blsam2REdigfor lefsest.blsam2REcrdrot lefsest.blsam2REboscor lefsest.blsam2REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.blsaM3fest;
MERGE lefsest.blsam3FEtrats lefsest.blsam3FEtrbts lefsest.blsam3FEtrba lefsest.blsam3FEalphas lefsest.blsam3FEsimtot lefsest.blsam3FEcvltca lefsest.blsam3FEcvlca1 lefsest.blsam3FEcvlca2 lefsest.blsam3FEcvlca3 lefsest.blsam3FEcvlca4 lefsest.blsam3FEcvlca5 lefsest.blsam3FEcvltcb lefsest.blsam3FEcvlfrs lefsest.blsam3FEcvlcrs lefsest.blsam3FEcvlfrl lefsest.blsam3FEcvlcrl lefsest.blsam3FEcvlrs1 lefsest.blsam3FEcvlrs2 lefsest.blsam3FEcvlrs3 lefsest.blsam3FEcvlrs4 lefsest.blsam3FEcvlrs5 lefsest.blsam3FEcvlrst lefsest.blsam3FEcvlrx1 lefsest.blsam3FEcvlrx2 lefsest.blsam3FEcvlrx3 lefsest.blsam3FEcvlrx4 lefsest.blsam3FEcvlrx5 lefsest.blsam3FEcvlrxt lefsest.blsam3FEcvlbet lefsest.blsam3FEcvltpe lefsest.blsam3FEcvltif lefsest.blsam3FEcvltic lefsest.blsam3FEcvldis lefsest.blsam3FEcvlrbi lefsest.blsam3FEcvlsv5 lefsest.blsam3FEcvllv5 lefsest.blsam3FEcvlsvs lefsest.blsam3FEcvllvs lefsest.blsam3FEcvllvl lefsest.blsam3FEflucat lefsest.blsam3FEflulet lefsest.blsam3FEfluctw lefsest.blsam3FEflultw lefsest.blsam3FEflucti lefsest.blsam3FEfluctp lefsest.blsam3FEflulti lefsest.blsam3FEflultp lefsest.blsam3FEflucfw lefsest.blsam3FEflucfp lefsest.blsam3FEflucfi lefsest.blsam3FEflucaw lefsest.blsam3FEflucap lefsest.blsam3FEflucai lefsest.blsam3FEflucvw lefsest.blsam3FEflucvp lefsest.blsam3FEflucvi lefsest.blsam3FEflulfw lefsest.blsam3FEflulfp lefsest.blsam3FEflulfi lefsest.blsam3FEflulaw lefsest.blsam3FEflulap lefsest.blsam3FEflulai lefsest.blsam3FEflulsw lefsest.blsam3FEflulsp lefsest.blsam3FEflulsi lefsest.blsam3FEpmato3 lefsest.blsam3FEdigbac lefsest.blsam3FEdigfor lefsest.blsam3FEcrdrot lefsest.blsam3FEboscor lefsest.blsam3FEbvrtot;
BY Effect;
RUN;

DATA lefslib.blsaM3rest;
MERGE lefslib.blsaBaseline lefsest.blsam3REtrats lefsest.blsam3REtrbts lefsest.blsam3REtrba lefsest.blsam3REalphas lefsest.blsam3REsimtot lefsest.blsam3REcvltca lefsest.blsam3REcvlca1 lefsest.blsam3REcvlca2 lefsest.blsam3REcvlca3 lefsest.blsam3REcvlca4 lefsest.blsam3REcvlca5 lefsest.blsam3REcvltcb lefsest.blsam3REcvlfrs lefsest.blsam3REcvlcrs lefsest.blsam3REcvlfrl lefsest.blsam3REcvlcrl lefsest.blsam3REcvlrs1 lefsest.blsam3REcvlrs2 lefsest.blsam3REcvlrs3 lefsest.blsam3REcvlrs4 lefsest.blsam3REcvlrs5 lefsest.blsam3REcvlrst lefsest.blsam3REcvlrx1 lefsest.blsam3REcvlrx2 lefsest.blsam3REcvlrx3 lefsest.blsam3REcvlrx4 lefsest.blsam3REcvlrx5 lefsest.blsam3REcvlrxt lefsest.blsam3REcvlbet lefsest.blsam3REcvltpe lefsest.blsam3REcvltif lefsest.blsam3REcvltic lefsest.blsam3REcvldis lefsest.blsam3REcvlrbi lefsest.blsam3REcvlsv5 lefsest.blsam3REcvllv5 lefsest.blsam3REcvlsvs lefsest.blsam3REcvllvs lefsest.blsam3REcvllvl lefsest.blsam3REflucat lefsest.blsam3REflulet lefsest.blsam3REfluctw lefsest.blsam3REflultw lefsest.blsam3REflucti lefsest.blsam3REfluctp lefsest.blsam3REflulti lefsest.blsam3REflultp lefsest.blsam3REflucfw lefsest.blsam3REflucfp lefsest.blsam3REflucfi lefsest.blsam3REflucaw lefsest.blsam3REflucap lefsest.blsam3REflucai lefsest.blsam3REflucvw lefsest.blsam3REflucvp lefsest.blsam3REflucvi lefsest.blsam3REflulfw lefsest.blsam3REflulfp lefsest.blsam3REflulfi lefsest.blsam3REflulaw lefsest.blsam3REflulap lefsest.blsam3REflulai lefsest.blsam3REflulsw lefsest.blsam3REflulsp lefsest.blsam3REflulsi lefsest.blsam3REpmato3 lefsest.blsam3REdigbac lefsest.blsam3REdigfor lefsest.blsam3REcrdrot lefsest.blsam3REboscor lefsest.blsam3REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.blsaM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.blsaM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.blsaM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.blsaM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.blsaM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.blsaM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\blsaM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Create macro to loop cognitive test variables (Operate on lefslib.zred3blsamain);;
%macro mixedmodelzred3(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3blsam1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3blsam1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3blsam1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3blsam1FE&dv;
		PROC SORT DATA = lefsest.zred3blsam1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3blsam1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3blsam1RE&dv;
		PROC SORT DATA = lefsest.zred3blsam1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3blsam2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3blsam2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3blsam2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3blsam2FE&dv;
		PROC SORT DATA = lefsest.zred3blsam2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3blsam2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3blsam2RE&dv;
		PROC SORT DATA = lefsest.zred3blsam2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.zred3blsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3blsam3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3blsam3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3blsam3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3blsam3RE&dv;

		DATA lefsest.zred3blsam3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3blsam3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3blsaM1fest;
MERGE lefsest.zred3blsam1FEbias lefsest.zred3blsam1FEcap lefsest.zred3blsam1FEefa lefsest.zred3blsam1FEefd lefsest.zred3blsam1FEefi lefsest.zred3blsam1FEefr lefsest.zred3blsam1FEefs lefsest.zred3blsam1FEem lefsest.zred3blsam1FEltm lefsest.zred3blsam1FEsd lefsest.zred3blsam1FEsp lefsest.zred3blsam1FEver lefsest.zred3blsam1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3blsaM1rest;
MERGE lefslib.blsaBaseline lefsest.zred3blsam1REbias lefsest.zred3blsam1REcap lefsest.zred3blsam1REefa lefsest.zred3blsam1REefd lefsest.zred3blsam1REefi lefsest.zred3blsam1REefr lefsest.zred3blsam1REefs lefsest.zred3blsam1REem lefsest.zred3blsam1REltm lefsest.zred3blsam1REsd lefsest.zred3blsam1REsp lefsest.zred3blsam1REver lefsest.zred3blsam1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3blsaM2fest;
MERGE lefsest.zred3blsam2FEbias lefsest.zred3blsam2FEcap lefsest.zred3blsam2FEefa lefsest.zred3blsam2FEefd lefsest.zred3blsam2FEefi lefsest.zred3blsam2FEefr lefsest.zred3blsam2FEefs lefsest.zred3blsam2FEem lefsest.zred3blsam2FEltm lefsest.zred3blsam2FEsd lefsest.zred3blsam2FEsp lefsest.zred3blsam2FEver lefsest.zred3blsam2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3blsaM2rest;
MERGE lefslib.blsaBaseline lefsest.zred3blsam2REbias lefsest.zred3blsam2REcap lefsest.zred3blsam2REefa lefsest.zred3blsam2REefd lefsest.zred3blsam2REefi lefsest.zred3blsam2REefr lefsest.zred3blsam2REefs lefsest.zred3blsam2REem lefsest.zred3blsam2REltm lefsest.zred3blsam2REsd lefsest.zred3blsam2REsp lefsest.zred3blsam2REver lefsest.zred3blsam2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3blsaM3fest;
MERGE lefsest.zred3blsam3FEbias lefsest.zred3blsam3FEcap lefsest.zred3blsam3FEefa lefsest.zred3blsam3FEefd lefsest.zred3blsam3FEefi lefsest.zred3blsam3FEefr lefsest.zred3blsam3FEefs lefsest.zred3blsam3FEem lefsest.zred3blsam3FEltm lefsest.zred3blsam3FEsd lefsest.zred3blsam3FEsp lefsest.zred3blsam3FEver lefsest.zred3blsam3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3blsaM3rest;
MERGE lefslib.blsaBaseline lefsest.zred3blsam3REbias lefsest.zred3blsam3REcap lefsest.zred3blsam3REefa lefsest.zred3blsam3REefd lefsest.zred3blsam3REefi lefsest.zred3blsam3REefr lefsest.zred3blsam3REefs lefsest.zred3blsam3REem lefsest.zred3blsam3REltm lefsest.zred3blsam3REsd lefsest.zred3blsam3REsp lefsest.zred3blsam3REver lefsest.zred3blsam3REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3*est as .csv;
PROC EXPORT data = lefslib.zred3blsaM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3blsaM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3blsaM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3blsaM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3blsaM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3blsaM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3blsaM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndblsamain);;
%macro mixedmodelzred3nd(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndblsam1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndblsam1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndblsam1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndblsam1FE&dv;
		PROC SORT DATA = lefsest.zred3ndblsam1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndblsam1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndblsam1RE&dv;
		PROC SORT DATA = lefsest.zred3ndblsam1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndblsam2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndblsam2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndblsam2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndblsam2FE&dv;
		PROC SORT DATA = lefsest.zred3ndblsam2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndblsam2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndblsam2RE&dv;
		PROC SORT DATA = lefsest.zred3ndblsam2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.zred3ndblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndblsam3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndblsam3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndblsam3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndblsam3RE&dv;

		DATA lefsest.zred3ndblsam3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndblsam3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3nd(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndblsaM1fest;
MERGE lefsest.zred3ndblsam1FEbias lefsest.zred3ndblsam1FEcap lefsest.zred3ndblsam1FEefa lefsest.zred3ndblsam1FEefd lefsest.zred3ndblsam1FEefi lefsest.zred3ndblsam1FEefr lefsest.zred3ndblsam1FEefs lefsest.zred3ndblsam1FEem lefsest.zred3ndblsam1FEltm lefsest.zred3ndblsam1FEsd lefsest.zred3ndblsam1FEsp lefsest.zred3ndblsam1FEver lefsest.zred3ndblsam1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndblsaM1rest;
MERGE lefslib.Baseline lefsest.zred3ndblsam1REbias lefsest.zred3ndblsam1REcap lefsest.zred3ndblsam1REefa lefsest.zred3ndblsam1REefd lefsest.zred3ndblsam1REefi lefsest.zred3ndblsam1REefr lefsest.zred3ndblsam1REefs lefsest.zred3ndblsam1REem lefsest.zred3ndblsam1REltm lefsest.zred3ndblsam1REsd lefsest.zred3ndblsam1REsp lefsest.zred3ndblsam1REver lefsest.zred3ndblsam1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndblsaM2fest;
MERGE lefsest.zred3ndblsam2FEbias lefsest.zred3ndblsam2FEcap lefsest.zred3ndblsam2FEefa lefsest.zred3ndblsam2FEefd lefsest.zred3ndblsam2FEefi lefsest.zred3ndblsam2FEefr lefsest.zred3ndblsam2FEefs lefsest.zred3ndblsam2FEem lefsest.zred3ndblsam2FEltm lefsest.zred3ndblsam2FEsd lefsest.zred3ndblsam2FEsp lefsest.zred3ndblsam2FEver lefsest.zred3ndblsam2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndblsaM2rest;
MERGE lefslib.Baseline lefsest.zred3ndblsam2REbias lefsest.zred3ndblsam2REcap lefsest.zred3ndblsam2REefa lefsest.zred3ndblsam2REefd lefsest.zred3ndblsam2REefi lefsest.zred3ndblsam2REefr lefsest.zred3ndblsam2REefs lefsest.zred3ndblsam2REem lefsest.zred3ndblsam2REltm lefsest.zred3ndblsam2REsd lefsest.zred3ndblsam2REsp lefsest.zred3ndblsam2REver lefsest.zred3ndblsam2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndblsaM3fest;
MERGE lefsest.zred3ndblsam3FEbias lefsest.zred3ndblsam3FEcap lefsest.zred3ndblsam3FEefa lefsest.zred3ndblsam3FEefd lefsest.zred3ndblsam3FEefi lefsest.zred3ndblsam3FEefr lefsest.zred3ndblsam3FEefs lefsest.zred3ndblsam3FEem lefsest.zred3ndblsam3FEltm lefsest.zred3ndblsam3FEsd lefsest.zred3ndblsam3FEsp lefsest.zred3ndblsam3FEver lefsest.zred3ndblsam3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndblsaM3rest;
MERGE lefslib.Baseline lefsest.zred3ndblsam3REbias lefsest.zred3ndblsam3REcap lefsest.zred3ndblsam3REefa lefsest.zred3ndblsam3REefd lefsest.zred3ndblsam3REefi lefsest.zred3ndblsam3REefr lefsest.zred3ndblsam3REefs lefsest.zred3ndblsam3REem lefsest.zred3ndblsam3REltm lefsest.zred3ndblsam3REsd lefsest.zred3ndblsam3REsp lefsest.zred3ndblsam3REver lefsest.zred3ndblsam3REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3nd*est as .csv;
PROC EXPORT data = lefslib.zred3ndblsaM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndblsaM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndblsaM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndblsaM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndblsaM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndblsaM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndblsaM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* INCLUDE CN and MCI group predictor;
* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndblsamain);;

data lefslib.zred3ndgblsamain; set lefslib.zred3ndblsamain;
if donset = . then dg = 0;
if donset ~= . then dg =1;
run; 

PROC EXPORT data = lefslib.zred3ndgblsamain
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsamain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelzred3ndg(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgblsam1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgblsam1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndgblsam1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgblsam1FE&dv;
		PROC SORT DATA = lefsest.zred3ndgblsam1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndgblsam1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgblsam1RE&dv;
		PROC SORT DATA = lefsest.zred3ndgblsam1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgblsam2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgblsam2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndgblsam2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgblsam2FE&dv;
		PROC SORT DATA = lefsest.zred3ndgblsam2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndgblsam2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgblsam2RE&dv;
		PROC SORT DATA = lefsest.zred3ndgblsam2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgblsamain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = dg/ DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgblsam3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgblsam3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndgblsam3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgblsam3RE&dv;

		DATA lefsest.zred3ndgblsam3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgblsam3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3ndg(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndgblsaM1fest;
MERGE lefsest.zred3ndgblsam1FEbias lefsest.zred3ndgblsam1FEcap lefsest.zred3ndgblsam1FEefa lefsest.zred3ndgblsam1FEefd lefsest.zred3ndgblsam1FEefi lefsest.zred3ndgblsam1FEefr lefsest.zred3ndgblsam1FEefs lefsest.zred3ndgblsam1FEem lefsest.zred3ndgblsam1FEltm lefsest.zred3ndgblsam1FEsd lefsest.zred3ndgblsam1FEsp lefsest.zred3ndgblsam1FEver lefsest.zred3ndgblsam1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgblsaM1rest;
MERGE lefslib.Baseline lefsest.zred3ndgblsam1REbias lefsest.zred3ndgblsam1REcap lefsest.zred3ndgblsam1REefa lefsest.zred3ndgblsam1REefd lefsest.zred3ndgblsam1REefi lefsest.zred3ndgblsam1REefr lefsest.zred3ndgblsam1REefs lefsest.zred3ndgblsam1REem lefsest.zred3ndgblsam1REltm lefsest.zred3ndgblsam1REsd lefsest.zred3ndgblsam1REsp lefsest.zred3ndgblsam1REver lefsest.zred3ndgblsam1REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndgblsaM2fest;
MERGE lefsest.zred3ndgblsam2FEbias lefsest.zred3ndgblsam2FEcap lefsest.zred3ndgblsam2FEefa lefsest.zred3ndgblsam2FEefd lefsest.zred3ndgblsam2FEefi lefsest.zred3ndgblsam2FEefr lefsest.zred3ndgblsam2FEefs lefsest.zred3ndgblsam2FEem lefsest.zred3ndgblsam2FEltm lefsest.zred3ndgblsam2FEsd lefsest.zred3ndgblsam2FEsp lefsest.zred3ndgblsam2FEver lefsest.zred3ndgblsam2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgblsaM2rest;
MERGE lefslib.Baseline lefsest.zred3ndgblsam2REbias lefsest.zred3ndgblsam2REcap lefsest.zred3ndgblsam2REefa lefsest.zred3ndgblsam2REefd lefsest.zred3ndgblsam2REefi lefsest.zred3ndgblsam2REefr lefsest.zred3ndgblsam2REefs lefsest.zred3ndgblsam2REem lefsest.zred3ndgblsam2REltm lefsest.zred3ndgblsam2REsd lefsest.zred3ndgblsam2REsp lefsest.zred3ndgblsam2REver lefsest.zred3ndgblsam2REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndgblsaM3fest;
MERGE lefsest.zred3ndgblsam3FEbias lefsest.zred3ndgblsam3FEcap lefsest.zred3ndgblsam3FEefa lefsest.zred3ndgblsam3FEefd lefsest.zred3ndgblsam3FEefi lefsest.zred3ndgblsam3FEefr lefsest.zred3ndgblsam3FEefs lefsest.zred3ndgblsam3FEem lefsest.zred3ndgblsam3FEltm lefsest.zred3ndgblsam3FEsd lefsest.zred3ndgblsam3FEsp lefsest.zred3ndgblsam3FEver lefsest.zred3ndgblsam3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgblsaM3rest;
MERGE lefslib.Baseline lefsest.zred3ndgblsam3REbias lefsest.zred3ndgblsam3REcap lefsest.zred3ndgblsam3REefa lefsest.zred3ndgblsam3REefd lefsest.zred3ndgblsam3REefi lefsest.zred3ndgblsam3REefr lefsest.zred3ndgblsam3REefs lefsest.zred3ndgblsam3REem lefsest.zred3ndgblsam3REltm lefsest.zred3ndgblsam3REsd lefsest.zred3ndgblsam3REsp lefsest.zred3ndgblsam3REver lefsest.zred3ndgblsam3REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3ndg*est as .csv;
PROC EXPORT data = lefslib.zred3ndgblsaM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgblsaM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgblsaM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgblsaM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgblsaM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgblsaM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgblsaM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
