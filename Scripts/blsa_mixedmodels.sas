option nofmterr; run; * Switch off format errors ;

/* Estimate Mixed Model fixed effects of centered baseline age and interval for BLSA data lefs.nimain.

Created by Joshua Goh 10 Nov 2010;
	- model(1) y = intercept(f) + interval(f) + intercept(r) + interval(r)
	- model(2) y = intercept(f) + cbaseage(f) + interval(f) + cbaseage*interval(f) + intercept(r) + interval(r)
	- model(3) y = intercept(f) + intercept(r) + interval(r)

NOTE!!! After csv export, "<.0001" must be replace by ".0001" in Excel to enable Matlab reading.
Modified by Josh Goh 30 Nov 2010 - added fluctw flultw flucti fluctp flulti flultp 
Modified by Josh Goh 1 Dec 2010 - Added trba, cvlsv5, cvllv5, cvlsvs, cvllvs, cvllvl; Removed trate, trbte, cvlbei.
Modified by Josh Goh 10 Dec 2010 - Changed code to operate on Zred3nimain.
Modified by Josh Goh 19 Jan 2011 - Added new model to zred3ndnimain to include MCI/CN group variable.
Script modified by Josh Goh 26 Jan 2011 - Changed code to operate on Zred3ndrnimain. */

* Setup libraries in file system;
libname lefsest 'Z:\Research\Projects\BLSA\Battery\Data\lefsest';

* Create macro to loop cognitive test variables (Operate on lefslib.nimain);;
%macro mixedmodel(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.m1FE&dv;
		PROC SORT DATA = lefsest.m1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.m1RE&dv;
		PROC SORT DATA = lefsest.m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.m2FE&dv;
		PROC SORT DATA = lefsest.m2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.m2RE&dv;
		PROC SORT DATA = lefsest.m2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.m3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.m3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.m3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.m3RE&dv;

		DATA lefsest.m3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.m3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodel(trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot);

* Sans non-convergent variables;
* %mixedmodel(trats trbts trate trbte alphas simtot cvltca cvlca2 cvlca3 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlbet cvlbei cvltpe cvltif cvltic cvldis cvlrbi flucat flulet pmato3 digbac digfor crdrot bvrtot);

* Non-convergent variables;
* %mixedmodel(cvlca1 cvlca4 cvlrxt);

* Merge variables into lefslib;
DATA lefslib.M1fest;
MERGE lefsest.m1FEtrats lefsest.m1FEtrbts lefsest.m1FEtrba lefsest.m1FEalphas lefsest.m1FEsimtot lefsest.m1FEcvltca lefsest.m1FEcvlca1 lefsest.m1FEcvlca2 lefsest.m1FEcvlca3 lefsest.m1FEcvlca4 lefsest.m1FEcvlca5 lefsest.m1FEcvltcb lefsest.m1FEcvlfrs lefsest.m1FEcvlcrs lefsest.m1FEcvlfrl lefsest.m1FEcvlcrl lefsest.m1FEcvlrs1 lefsest.m1FEcvlrs2 lefsest.m1FEcvlrs3 lefsest.m1FEcvlrs4 lefsest.m1FEcvlrs5 lefsest.m1FEcvlrst lefsest.m1FEcvlrx1 lefsest.m1FEcvlrx2 lefsest.m1FEcvlrx3 lefsest.m1FEcvlrx4 lefsest.m1FEcvlrx5 lefsest.m1FEcvlrxt lefsest.m1FEcvlbet lefsest.m1FEcvltpe lefsest.m1FEcvltif lefsest.m1FEcvltic lefsest.m1FEcvldis lefsest.m1FEcvlrbi lefsest.m1FEcvlsv5 lefsest.m1FEcvllv5 lefsest.m1FEcvlsvs lefsest.m1FEcvllvs lefsest.m1FEcvllvl lefsest.m1FEflucat lefsest.m1FEflulet lefsest.m1FEfluctw lefsest.m1FEflultw lefsest.m1FEflucti lefsest.m1FEfluctp lefsest.m1FEflulti lefsest.m1FEflultp lefsest.m1FEflucfw lefsest.m1FEflucfp lefsest.m1FEflucfi lefsest.m1FEflucaw lefsest.m1FEflucap lefsest.m1FEflucai lefsest.m1FEflucvw lefsest.m1FEflucvp lefsest.m1FEflucvi lefsest.m1FEflulfw lefsest.m1FEflulfp lefsest.m1FEflulfi lefsest.m1FEflulaw lefsest.m1FEflulap lefsest.m1FEflulai lefsest.m1FEflulsw lefsest.m1FEflulsp lefsest.m1FEflulsi lefsest.m1FEpmato3 lefsest.m1FEdigbac lefsest.m1FEdigfor lefsest.m1FEcrdrot lefsest.m1FEboscor lefsest.m1FEbvrtot;
BY Effect;
RUN;

DATA lefslib.M1rest;
MERGE lefslib.Baseline lefsest.m1REtrats lefsest.m1REtrbts lefsest.m1REtrba lefsest.m1REalphas lefsest.m1REsimtot lefsest.m1REcvltca lefsest.m1REcvlca1 lefsest.m1REcvlca2 lefsest.m1REcvlca3 lefsest.m1REcvlca4 lefsest.m1REcvlca5 lefsest.m1REcvltcb lefsest.m1REcvlfrs lefsest.m1REcvlcrs lefsest.m1REcvlfrl lefsest.m1REcvlcrl lefsest.m1REcvlrs1 lefsest.m1REcvlrs2 lefsest.m1REcvlrs3 lefsest.m1REcvlrs4 lefsest.m1REcvlrs5 lefsest.m1REcvlrst lefsest.m1REcvlrx1 lefsest.m1REcvlrx2 lefsest.m1REcvlrx3 lefsest.m1REcvlrx4 lefsest.m1REcvlrx5 lefsest.m1REcvlrxt lefsest.m1REcvlbet lefsest.m1REcvltpe lefsest.m1REcvltif lefsest.m1REcvltic lefsest.m1REcvldis lefsest.m1REcvlrbi lefsest.m1REcvlsv5 lefsest.m1REcvllv5 lefsest.m1REcvlsvs lefsest.m1REcvllvs lefsest.m1REcvllvl lefsest.m1REflucat lefsest.m1REflulet lefsest.m1REfluctw lefsest.m1REflultw lefsest.m1REflucti lefsest.m1REfluctp lefsest.m1REflulti lefsest.m1REflultp lefsest.m1REflucfw lefsest.m1REflucfp lefsest.m1REflucfi lefsest.m1REflucaw lefsest.m1REflucap lefsest.m1REflucai lefsest.m1REflucvw lefsest.m1REflucvp lefsest.m1REflucvi lefsest.m1REflulfw lefsest.m1REflulfp lefsest.m1REflulfi lefsest.m1REflulaw lefsest.m1REflulap lefsest.m1REflulai lefsest.m1REflulsw lefsest.m1REflulsp lefsest.m1REflulsi lefsest.m1REpmato3 lefsest.m1REdigbac lefsest.m1REdigfor lefsest.m1REcrdrot lefsest.m1REboscor lefsest.m1REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.M2fest;
MERGE lefsest.m2FEtrats lefsest.m2FEtrbts lefsest.m2FEtrba lefsest.m2FEalphas lefsest.m2FEsimtot lefsest.m2FEcvltca lefsest.m2FEcvlca1 lefsest.m2FEcvlca2 lefsest.m2FEcvlca3 lefsest.m2FEcvlca4 lefsest.m2FEcvlca5 lefsest.m2FEcvltcb lefsest.m2FEcvlfrs lefsest.m2FEcvlcrs lefsest.m2FEcvlfrl lefsest.m2FEcvlcrl lefsest.m2FEcvlrs1 lefsest.m2FEcvlrs2 lefsest.m2FEcvlrs3 lefsest.m2FEcvlrs4 lefsest.m2FEcvlrs5 lefsest.m2FEcvlrst lefsest.m2FEcvlrx1 lefsest.m2FEcvlrx2 lefsest.m2FEcvlrx3 lefsest.m2FEcvlrx4 lefsest.m2FEcvlrx5 lefsest.m2FEcvlrxt lefsest.m2FEcvlbet lefsest.m2FEcvltpe lefsest.m2FEcvltif lefsest.m2FEcvltic lefsest.m2FEcvldis lefsest.m2FEcvlrbi lefsest.m2FEcvlsv5 lefsest.m2FEcvllv5 lefsest.m2FEcvlsvs lefsest.m2FEcvllvs lefsest.m2FEcvllvl lefsest.m2FEflucat lefsest.m2FEflulet lefsest.m2FEfluctw lefsest.m2FEflultw lefsest.m2FEflucti lefsest.m2FEfluctp lefsest.m2FEflulti lefsest.m2FEflultp lefsest.m2FEflucfw lefsest.m2FEflucfp lefsest.m2FEflucfi lefsest.m2FEflucaw lefsest.m2FEflucap lefsest.m2FEflucai lefsest.m2FEflucvw lefsest.m2FEflucvp lefsest.m2FEflucvi lefsest.m2FEflulfw lefsest.m2FEflulfp lefsest.m2FEflulfi lefsest.m2FEflulaw lefsest.m2FEflulap lefsest.m2FEflulai lefsest.m2FEflulsw lefsest.m2FEflulsp lefsest.m2FEflulsi lefsest.m2FEpmato3 lefsest.m2FEdigbac lefsest.m2FEdigfor lefsest.m2FEcrdrot lefsest.m2FEboscor lefsest.m2FEbvrtot;
BY Effect;
RUN;

DATA lefslib.M2rest;
MERGE lefslib.Baseline lefsest.m2REtrats lefsest.m2REtrbts lefsest.m2REtrba lefsest.m2REalphas lefsest.m2REsimtot lefsest.m2REcvltca lefsest.m2REcvlca1 lefsest.m2REcvlca2 lefsest.m2REcvlca3 lefsest.m2REcvlca4 lefsest.m2REcvlca5 lefsest.m2REcvltcb lefsest.m2REcvlfrs lefsest.m2REcvlcrs lefsest.m2REcvlfrl lefsest.m2REcvlcrl lefsest.m2REcvlrs1 lefsest.m2REcvlrs2 lefsest.m2REcvlrs3 lefsest.m2REcvlrs4 lefsest.m2REcvlrs5 lefsest.m2REcvlrst lefsest.m2REcvlrx1 lefsest.m2REcvlrx2 lefsest.m2REcvlrx3 lefsest.m2REcvlrx4 lefsest.m2REcvlrx5 lefsest.m2REcvlrxt lefsest.m2REcvlbet lefsest.m2REcvltpe lefsest.m2REcvltif lefsest.m2REcvltic lefsest.m2REcvldis lefsest.m2REcvlrbi lefsest.m2REcvlsv5 lefsest.m2REcvllv5 lefsest.m2REcvlsvs lefsest.m2REcvllvs lefsest.m2REcvllvl lefsest.m2REflucat lefsest.m2REflulet lefsest.m2REfluctw lefsest.m2REflultw lefsest.m2REflucti lefsest.m2REfluctp lefsest.m2REflulti lefsest.m2REflultp lefsest.m2REflucfw lefsest.m2REflucfp lefsest.m2REflucfi lefsest.m2REflucaw lefsest.m2REflucap lefsest.m2REflucai lefsest.m2REflucvw lefsest.m2REflucvp lefsest.m2REflucvi lefsest.m2REflulfw lefsest.m2REflulfp lefsest.m2REflulfi lefsest.m2REflulaw lefsest.m2REflulap lefsest.m2REflulai lefsest.m2REflulsw lefsest.m2REflulsp lefsest.m2REflulsi lefsest.m2REpmato3 lefsest.m2REdigbac lefsest.m2REdigfor lefsest.m2REcrdrot lefsest.m2REboscor lefsest.m2REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.M3fest;
MERGE lefsest.m3FEtrats lefsest.m3FEtrbts lefsest.m3FEtrba lefsest.m3FEalphas lefsest.m3FEsimtot lefsest.m3FEcvltca lefsest.m3FEcvlca1 lefsest.m3FEcvlca2 lefsest.m3FEcvlca3 lefsest.m3FEcvlca4 lefsest.m3FEcvlca5 lefsest.m3FEcvltcb lefsest.m3FEcvlfrs lefsest.m3FEcvlcrs lefsest.m3FEcvlfrl lefsest.m3FEcvlcrl lefsest.m3FEcvlrs1 lefsest.m3FEcvlrs2 lefsest.m3FEcvlrs3 lefsest.m3FEcvlrs4 lefsest.m3FEcvlrs5 lefsest.m3FEcvlrst lefsest.m3FEcvlrx1 lefsest.m3FEcvlrx2 lefsest.m3FEcvlrx3 lefsest.m3FEcvlrx4 lefsest.m3FEcvlrx5 lefsest.m3FEcvlrxt lefsest.m3FEcvlbet lefsest.m3FEcvltpe lefsest.m3FEcvltif lefsest.m3FEcvltic lefsest.m3FEcvldis lefsest.m3FEcvlrbi lefsest.m3FEcvlsv5 lefsest.m3FEcvllv5 lefsest.m3FEcvlsvs lefsest.m3FEcvllvs lefsest.m3FEcvllvl lefsest.m3FEflucat lefsest.m3FEflulet lefsest.m3FEfluctw lefsest.m3FEflultw lefsest.m3FEflucti lefsest.m3FEfluctp lefsest.m3FEflulti lefsest.m3FEflultp lefsest.m3FEflucfw lefsest.m3FEflucfp lefsest.m3FEflucfi lefsest.m3FEflucaw lefsest.m3FEflucap lefsest.m3FEflucai lefsest.m3FEflucvw lefsest.m3FEflucvp lefsest.m3FEflucvi lefsest.m3FEflulfw lefsest.m3FEflulfp lefsest.m3FEflulfi lefsest.m3FEflulaw lefsest.m3FEflulap lefsest.m3FEflulai lefsest.m3FEflulsw lefsest.m3FEflulsp lefsest.m3FEflulsi lefsest.m3FEpmato3 lefsest.m3FEdigbac lefsest.m3FEdigfor lefsest.m3FEcrdrot lefsest.m3FEboscor lefsest.m3FEbvrtot;
BY Effect;
RUN;

DATA lefslib.M3rest;
MERGE lefslib.Baseline lefsest.m3REtrats lefsest.m3REtrbts lefsest.m3REtrba lefsest.m3REalphas lefsest.m3REsimtot lefsest.m3REcvltca lefsest.m3REcvlca1 lefsest.m3REcvlca2 lefsest.m3REcvlca3 lefsest.m3REcvlca4 lefsest.m3REcvlca5 lefsest.m3REcvltcb lefsest.m3REcvlfrs lefsest.m3REcvlcrs lefsest.m3REcvlfrl lefsest.m3REcvlcrl lefsest.m3REcvlrs1 lefsest.m3REcvlrs2 lefsest.m3REcvlrs3 lefsest.m3REcvlrs4 lefsest.m3REcvlrs5 lefsest.m3REcvlrst lefsest.m3REcvlrx1 lefsest.m3REcvlrx2 lefsest.m3REcvlrx3 lefsest.m3REcvlrx4 lefsest.m3REcvlrx5 lefsest.m3REcvlrxt lefsest.m3REcvlbet lefsest.m3REcvltpe lefsest.m3REcvltif lefsest.m3REcvltic lefsest.m3REcvldis lefsest.m3REcvlrbi lefsest.m3REcvlsv5 lefsest.m3REcvllv5 lefsest.m3REcvlsvs lefsest.m3REcvllvs lefsest.m3REcvllvl lefsest.m3REflucat lefsest.m3REflulet lefsest.m3REfluctw lefsest.m3REflultw lefsest.m3REflucti lefsest.m3REfluctp lefsest.m3REflulti lefsest.m3REflultp lefsest.m3REflucfw lefsest.m3REflucfp lefsest.m3REflucfi lefsest.m3REflucaw lefsest.m3REflucap lefsest.m3REflucai lefsest.m3REflucvw lefsest.m3REflucvp lefsest.m3REflucvi lefsest.m3REflulfw lefsest.m3REflulfp lefsest.m3REflulfi lefsest.m3REflulaw lefsest.m3REflulap lefsest.m3REflulai lefsest.m3REflulsw lefsest.m3REflulsp lefsest.m3REflulsi lefsest.m3REpmato3 lefsest.m3REdigbac lefsest.m3REdigfor lefsest.m3REcrdrot lefsest.m3REboscor lefsest.m3REbvrtot;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.M3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.M3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\M3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Create macro to loop cognitive test variables (Operate on lefslib.zred3nimain);;
%macro mixedmodelzred3(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3m1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3m1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3m1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3m1FE&dv;
		PROC SORT DATA = lefsest.zred3m1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3m1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3m1RE&dv;
		PROC SORT DATA = lefsest.zred3m1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3m2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3m2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3m2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3m2FE&dv;
		PROC SORT DATA = lefsest.zred3m2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3m2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3m2RE&dv;
		PROC SORT DATA = lefsest.zred3m2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.zred3nimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3m3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3m3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3m3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3m3RE&dv;

		DATA lefsest.zred3m3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3m3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3M1fest;
MERGE lefsest.zred3m1FEbias lefsest.zred3m1FEcap lefsest.zred3m1FEefa lefsest.zred3m1FEefd lefsest.zred3m1FEefi lefsest.zred3m1FEefr lefsest.zred3m1FEefs lefsest.zred3m1FEem lefsest.zred3m1FEltm lefsest.zred3m1FEsd lefsest.zred3m1FEsp lefsest.zred3m1FEver lefsest.zred3m1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3M1rest;
MERGE lefslib.Baseline lefsest.zred3m1REbias lefsest.zred3m1REcap lefsest.zred3m1REefa lefsest.zred3m1REefd lefsest.zred3m1REefi lefsest.zred3m1REefr lefsest.zred3m1REefs lefsest.zred3m1REem lefsest.zred3m1REltm lefsest.zred3m1REsd lefsest.zred3m1REsp lefsest.zred3m1REver lefsest.zred3m1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3M2fest;
MERGE lefsest.zred3m2FEbias lefsest.zred3m2FEcap lefsest.zred3m2FEefa lefsest.zred3m2FEefd lefsest.zred3m2FEefi lefsest.zred3m2FEefr lefsest.zred3m2FEefs lefsest.zred3m2FEem lefsest.zred3m2FEltm lefsest.zred3m2FEsd lefsest.zred3m2FEsp lefsest.zred3m2FEver lefsest.zred3m2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3M2rest;
MERGE lefslib.Baseline lefsest.zred3m2REbias lefsest.zred3m2REcap lefsest.zred3m2REefa lefsest.zred3m2REefd lefsest.zred3m2REefi lefsest.zred3m2REefr lefsest.zred3m2REefs lefsest.zred3m2REem lefsest.zred3m2REltm lefsest.zred3m2REsd lefsest.zred3m2REsp lefsest.zred3m2REver lefsest.zred3m2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3M3fest;
MERGE lefsest.zred3m3FEbias lefsest.zred3m3FEcap lefsest.zred3m3FEefa lefsest.zred3m3FEefd lefsest.zred3m3FEefi lefsest.zred3m3FEefr lefsest.zred3m3FEefs lefsest.zred3m3FEem lefsest.zred3m3FEltm lefsest.zred3m3FEsd lefsest.zred3m3FEsp lefsest.zred3m3FEver lefsest.zred3m3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3M3rest;
MERGE lefslib.Baseline lefsest.zred3m3REbias lefsest.zred3m3REcap lefsest.zred3m3REefa lefsest.zred3m3REefd lefsest.zred3m3REefi lefsest.zred3m3REefr lefsest.zred3m3REefs lefsest.zred3m3REem lefsest.zred3m3REltm lefsest.zred3m3REsd lefsest.zred3m3REsp lefsest.zred3m3REver lefsest.zred3m3REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3*est as .csv;
PROC EXPORT data = lefslib.zred3M1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3M1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3M2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3M2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3M3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3M3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3M3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndnimain);;
%macro mixedmodelzred3nd(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3ndnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndm1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndm1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndm1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndm1FE&dv;
		PROC SORT DATA = lefsest.zred3ndm1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndm1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndm1RE&dv;
		PROC SORT DATA = lefsest.zred3ndm1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3ndnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndm2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndm2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndm2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndm2FE&dv;
		PROC SORT DATA = lefsest.zred3ndm2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndm2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndm2RE&dv;
		PROC SORT DATA = lefsest.zred3ndm2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.zred3ndnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndm3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndm3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndm3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndm3RE&dv;

		DATA lefsest.zred3ndm3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndm3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3nd(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndM1fest;
MERGE lefsest.zred3ndm1FEbias lefsest.zred3ndm1FEcap lefsest.zred3ndm1FEefa lefsest.zred3ndm1FEefd lefsest.zred3ndm1FEefi lefsest.zred3ndm1FEefr lefsest.zred3ndm1FEefs lefsest.zred3ndm1FEem lefsest.zred3ndm1FEltm lefsest.zred3ndm1FEsd lefsest.zred3ndm1FEsp lefsest.zred3ndm1FEver lefsest.zred3ndm1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndM1rest;
MERGE lefslib.Baseline lefsest.zred3ndm1REbias lefsest.zred3ndm1REcap lefsest.zred3ndm1REefa lefsest.zred3ndm1REefd lefsest.zred3ndm1REefi lefsest.zred3ndm1REefr lefsest.zred3ndm1REefs lefsest.zred3ndm1REem lefsest.zred3ndm1REltm lefsest.zred3ndm1REsd lefsest.zred3ndm1REsp lefsest.zred3ndm1REver lefsest.zred3ndm1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndM2fest;
MERGE lefsest.zred3ndm2FEbias lefsest.zred3ndm2FEcap lefsest.zred3ndm2FEefa lefsest.zred3ndm2FEefd lefsest.zred3ndm2FEefi lefsest.zred3ndm2FEefr lefsest.zred3ndm2FEefs lefsest.zred3ndm2FEem lefsest.zred3ndm2FEltm lefsest.zred3ndm2FEsd lefsest.zred3ndm2FEsp lefsest.zred3ndm2FEver lefsest.zred3ndm2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndM2rest;
MERGE lefslib.Baseline lefsest.zred3ndm2REbias lefsest.zred3ndm2REcap lefsest.zred3ndm2REefa lefsest.zred3ndm2REefd lefsest.zred3ndm2REefi lefsest.zred3ndm2REefr lefsest.zred3ndm2REefs lefsest.zred3ndm2REem lefsest.zred3ndm2REltm lefsest.zred3ndm2REsd lefsest.zred3ndm2REsp lefsest.zred3ndm2REver lefsest.zred3ndm2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndM3fest;
MERGE lefsest.zred3ndm3FEbias lefsest.zred3ndm3FEcap lefsest.zred3ndm3FEefa lefsest.zred3ndm3FEefd lefsest.zred3ndm3FEefi lefsest.zred3ndm3FEefr lefsest.zred3ndm3FEefs lefsest.zred3ndm3FEem lefsest.zred3ndm3FEltm lefsest.zred3ndm3FEsd lefsest.zred3ndm3FEsp lefsest.zred3ndm3FEver lefsest.zred3ndm3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndM3rest;
MERGE lefslib.Baseline lefsest.zred3ndm3REbias lefsest.zred3ndm3REcap lefsest.zred3ndm3REefa lefsest.zred3ndm3REefd lefsest.zred3ndm3REefi lefsest.zred3ndm3REefr lefsest.zred3ndm3REefs lefsest.zred3ndm3REem lefsest.zred3ndm3REltm lefsest.zred3ndm3REsd lefsest.zred3ndm3REsp lefsest.zred3ndm3REver lefsest.zred3ndm3REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3nd*est as .csv;
PROC EXPORT data = lefslib.zred3ndM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* INCLUDE CN and MCI group predictor;
* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndgnimain);;

/* Code dementia = 1;
data lefslib.zred3ndgnimain; set lefslib.zred3ndnimain;
if donset = . then dg = 0;
if donset ~= . then dg = 1;
run; */

* Code dementia = 0;
data lefslib.zred3ndgnimain; set lefslib.zred3ndnimain;
if donset = . then dg = 1;
if donset ~= . then dg = 0;
run; 

PROC EXPORT data = lefslib.zred3ndgnimain
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgnimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelzred3ndg(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgm1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgm1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndgm1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgm1FE&dv;
		PROC SORT DATA = lefsest.zred3ndgm1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndgm1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgm1RE&dv;
		PROC SORT DATA = lefsest.zred3ndgm1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgm2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgm2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndgm2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgm2FE&dv;
		PROC SORT DATA = lefsest.zred3ndgm2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndgm2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgm2RE&dv;
		PROC SORT DATA = lefsest.zred3ndgm2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = dg/ DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndgm3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndgm3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndgm3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndgm3RE&dv;

		DATA lefsest.zred3ndgm3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndgm3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3ndg(bias cap efa efd efi efr efs em ltm sd sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndgM1fest;
MERGE lefsest.zred3ndgm1FEbias lefsest.zred3ndgm1FEcap lefsest.zred3ndgm1FEefa lefsest.zred3ndgm1FEefd lefsest.zred3ndgm1FEefi lefsest.zred3ndgm1FEefr lefsest.zred3ndgm1FEefs lefsest.zred3ndgm1FEem lefsest.zred3ndgm1FEltm lefsest.zred3ndgm1FEsd lefsest.zred3ndgm1FEsp lefsest.zred3ndgm1FEver lefsest.zred3ndgm1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgM1rest;
MERGE lefslib.Baseline lefsest.zred3ndgm1REbias lefsest.zred3ndgm1REcap lefsest.zred3ndgm1REefa lefsest.zred3ndgm1REefd lefsest.zred3ndgm1REefi lefsest.zred3ndgm1REefr lefsest.zred3ndgm1REefs lefsest.zred3ndgm1REem lefsest.zred3ndgm1REltm lefsest.zred3ndgm1REsd lefsest.zred3ndgm1REsp lefsest.zred3ndgm1REver lefsest.zred3ndgm1REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndgM2fest;
MERGE lefsest.zred3ndgm2FEbias lefsest.zred3ndgm2FEcap lefsest.zred3ndgm2FEefa lefsest.zred3ndgm2FEefd lefsest.zred3ndgm2FEefi lefsest.zred3ndgm2FEefr lefsest.zred3ndgm2FEefs lefsest.zred3ndgm2FEem lefsest.zred3ndgm2FEltm lefsest.zred3ndgm2FEsd lefsest.zred3ndgm2FEsp lefsest.zred3ndgm2FEver lefsest.zred3ndgm2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgM2rest;
MERGE lefslib.Baseline lefsest.zred3ndgm2REbias lefsest.zred3ndgm2REcap lefsest.zred3ndgm2REefa lefsest.zred3ndgm2REefd lefsest.zred3ndgm2REefi lefsest.zred3ndgm2REefr lefsest.zred3ndgm2REefs lefsest.zred3ndgm2REem lefsest.zred3ndgm2REltm lefsest.zred3ndgm2REsd lefsest.zred3ndgm2REsp lefsest.zred3ndgm2REver lefsest.zred3ndgm2REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndgM3fest;
MERGE lefsest.zred3ndgm3FEbias lefsest.zred3ndgm3FEcap lefsest.zred3ndgm3FEefa lefsest.zred3ndgm3FEefd lefsest.zred3ndgm3FEefi lefsest.zred3ndgm3FEefr lefsest.zred3ndgm3FEefs lefsest.zred3ndgm3FEem lefsest.zred3ndgm3FEltm lefsest.zred3ndgm3FEsd lefsest.zred3ndgm3FEsp lefsest.zred3ndgm3FEver lefsest.zred3ndgm3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndgM3rest;
MERGE lefslib.Baseline lefsest.zred3ndgm3REbias lefsest.zred3ndgm3REcap lefsest.zred3ndgm3REefa lefsest.zred3ndgm3REefd lefsest.zred3ndgm3REefi lefsest.zred3ndgm3REefr lefsest.zred3ndgm3REefs lefsest.zred3ndgm3REem lefsest.zred3ndgm3REltm lefsest.zred3ndgm3REsd lefsest.zred3ndgm3REsp lefsest.zred3ndgm3REver lefsest.zred3ndgm3REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3ndg*est as .csv;
PROC EXPORT data = lefslib.zred3ndgM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndgM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndgM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;



* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndrnimain);;
%macro mixedmodelzred3ndr(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.zred3ndrnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrm1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrm1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndrm1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrm1FE&dv;
		PROC SORT DATA = lefsest.zred3ndrm1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndrm1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrm1RE&dv;
		PROC SORT DATA = lefsest.zred3ndrm1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.zred3ndrnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrm2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrm2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndrm2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrm2FE&dv;
		PROC SORT DATA = lefsest.zred3ndrm2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndrm2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrm2RE&dv;
		PROC SORT DATA = lefsest.zred3ndrm2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects;
		PROC MIXED data=lefslib.zred3ndrnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = / DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrm3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrm3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndrm3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrm3RE&dv;

		DATA lefsest.zred3ndrm3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrm3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3ndr(cap efm efi efa efd efr stm ltm sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndrM1fest;
MERGE lefsest.zred3ndrm1FEcap lefsest.zred3ndrm1FEefm lefsest.zred3ndrm1FEefi lefsest.zred3ndrm1FEefa lefsest.zred3ndrm1FEefd lefsest.zred3ndrm1FEefr lefsest.zred3ndrm1FEstm lefsest.zred3ndrm1FEltm lefsest.zred3ndrm1FEsp lefsest.zred3ndrm1FEver lefsest.zred3ndrm1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrM1rest;
MERGE lefslib.Baseline lefsest.zred3ndrm1REcap lefsest.zred3ndrm1REefm lefsest.zred3ndrm1REefi lefsest.zred3ndrm1REefa lefsest.zred3ndrm1REefd lefsest.zred3ndrm1REefr lefsest.zred3ndrm1REstm lefsest.zred3ndrm1REltm lefsest.zred3ndrm1REsp lefsest.zred3ndrm1REver lefsest.zred3ndrm1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndrM2fest;
MERGE lefsest.zred3ndrm2FEcap lefsest.zred3ndrm2FEefm lefsest.zred3ndrm2FEefi lefsest.zred3ndrm2FEefa lefsest.zred3ndrm2FEefd lefsest.zred3ndrm2FEefr lefsest.zred3ndrm2FEstm lefsest.zred3ndrm2FEltm lefsest.zred3ndrm2FEsp lefsest.zred3ndrm2FEver lefsest.zred3ndrm2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrM2rest;
MERGE lefslib.Baseline lefsest.zred3ndrm2REcap lefsest.zred3ndrm2REefm lefsest.zred3ndrm2REefi lefsest.zred3ndrm2REefa lefsest.zred3ndrm2REefd lefsest.zred3ndrm2REefr lefsest.zred3ndrm2REstm lefsest.zred3ndrm2REltm lefsest.zred3ndrm2REsp lefsest.zred3ndrm2REver lefsest.zred3ndrm2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndrM3fest;
MERGE lefsest.zred3ndrm3FEcap lefsest.zred3ndrm3FEefm lefsest.zred3ndrm3FEefi lefsest.zred3ndrm3FEefa lefsest.zred3ndrm3FEefd lefsest.zred3ndrm3FEefr lefsest.zred3ndrm3FEstm lefsest.zred3ndrm3FEltm lefsest.zred3ndrm3FEsp lefsest.zred3ndrm3FEver lefsest.zred3ndrm3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrM3rest;
MERGE lefslib.Baseline lefsest.zred3ndrm3REcap lefsest.zred3ndrm3REefm lefsest.zred3ndrm3REefi lefsest.zred3ndrm3REefa lefsest.zred3ndrm3REefd lefsest.zred3ndrm3REefr lefsest.zred3ndrm3REstm lefsest.zred3ndrm3REltm lefsest.zred3ndrm3REsp lefsest.zred3ndrm3REver lefsest.zred3ndrm3REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3ndr*est as .csv;
PROC EXPORT data = lefslib.zred3ndrM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* INCLUDE CN and MCI group predictor;
* Create macro to loop cognitive test variables (Operate on lefslib.zred3ndrgnimain);;

/* Code dementia = 1;
data lefslib.zred3ndrgnimain; set lefslib.zred3ndrnimain;
if donset = . then dg = 0;
if donset ~= . then dg = 1;
run; */

* Code dementia = 0;
data lefslib.zred3ndrgnimain; set lefslib.zred3ndrnimain;
if donset = . then dg = 1;
if donset ~= . then dg = 0;
run; 

PROC EXPORT data = lefslib.zred3ndrgnimain
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgnimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

%macro mixedmodelzred3ndrg(varnames);
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndrgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval dg dg*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrgm1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrgm1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndrgm1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrgm1FE&dv;
		PROC SORT DATA = lefsest.zred3ndrgm1FE&dv; by Effect; RUN;

		* Only rename estimates for random effects;
		DATA lefsest.zred3ndrgm1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrgm1RE&dv;
		PROC SORT DATA = lefsest.zred3ndrgm1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndrgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval dg dg*cbaseage dg*interval dg*cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrgm2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrgm2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.zred3ndrgm2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrgm2FE&dv;
		PROC SORT DATA = lefsest.zred3ndrgm2FE&dv; by Effect; RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.zred3ndrgm2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrgm2RE&dv;
		PROC SORT DATA = lefsest.zred3ndrgm2RE&dv; by id; RUN;

		* MODEL 3 - for testing individual interval effects with dementia as covariate;
		PROC MIXED data=lefslib.zred3ndrgnimain METHOD = reml covtest;
			CLASS id;
			MODEL &dv = dg/ DDFM = kr SOLUTION;
			RANDOM intercept interval / TYPE=un SUBJECT=id SOLUTION;*/
			
			* Select estimates and store in database;
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.zred3ndrgm3FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.zred3ndrgm3RE&dv;
			
		RUN;

		* Rename variables;
		DATA lefsest.zred3ndrgm3RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.zred3ndrgm3RE&dv;

		DATA lefsest.zred3ndrgm3FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.zred3ndrgm3FE&dv;


		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;

* CHOOSE VARIABLES TO RUN MIXEDMODEL;
* Complete, default;
 %mixedmodelzred3ndrg(cap efm efi efa efd efr stm ltm sp ver vis);

* Merge variables into lefslib;
DATA lefslib.zred3ndrgM1fest;
MERGE lefsest.zred3ndrgm1FEcap lefsest.zred3ndrgm1FEefm lefsest.zred3ndrgm1FEefi lefsest.zred3ndrgm1FEefa lefsest.zred3ndrgm1FEefd lefsest.zred3ndrgm1FEefr lefsest.zred3ndrgm1FEstm lefsest.zred3ndrgm1FEltm lefsest.zred3ndrgm1FEsp lefsest.zred3ndrgm1FEver lefsest.zred3ndrgm1FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrgM1rest;
MERGE lefslib.Baseline lefsest.zred3ndrgm1REcap lefsest.zred3ndrgm1REefm lefsest.zred3ndrgm1REefi lefsest.zred3ndrgm1REefa lefsest.zred3ndrgm1REefd lefsest.zred3ndrgm1REefr lefsest.zred3ndrgm1REstm lefsest.zred3ndrgm1REltm lefsest.zred3ndrgm1REsp lefsest.zred3ndrgm1REver lefsest.zred3ndrgm1REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndrgM2fest;
MERGE lefsest.zred3ndrgm2FEcap lefsest.zred3ndrgm2FEefm lefsest.zred3ndrgm2FEefi lefsest.zred3ndrgm2FEefa lefsest.zred3ndrgm2FEefd lefsest.zred3ndrgm2FEefr lefsest.zred3ndrgm2FEstm lefsest.zred3ndrgm2FEltm lefsest.zred3ndrgm2FEsp lefsest.zred3ndrgm2FEver lefsest.zred3ndrgm2FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrgM2rest;
MERGE lefslib.Baseline lefsest.zred3ndrgm2REcap lefsest.zred3ndrgm2REefm lefsest.zred3ndrgm2REefi lefsest.zred3ndrgm2REefa lefsest.zred3ndrgm2REefd lefsest.zred3ndrgm2REefr lefsest.zred3ndrgm2REstm lefsest.zred3ndrgm2REltm lefsest.zred3ndrgm2REsp lefsest.zred3ndrgm2REver lefsest.zred3ndrgm2REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.zred3ndrgM3fest;
MERGE lefsest.zred3ndrgm3FEcap lefsest.zred3ndrgm3FEefm lefsest.zred3ndrgm3FEefi lefsest.zred3ndrgm3FEefa lefsest.zred3ndrgm3FEefd lefsest.zred3ndrgm3FEefr lefsest.zred3ndrgm3FEstm lefsest.zred3ndrgm3FEltm lefsest.zred3ndrgm3FEsp lefsest.zred3ndrgm3FEver lefsest.zred3ndrgm3FEvis;
BY Effect;
RUN;

DATA lefslib.zred3ndrgM3rest;
MERGE lefslib.Baseline lefsest.zred3ndrgm3REcap lefsest.zred3ndrgm3REefm lefsest.zred3ndrgm3REefi lefsest.zred3ndrgm3REefa lefsest.zred3ndrgm3REefd lefsest.zred3ndrgm3REefr lefsest.zred3ndrgm3REstm lefsest.zred3ndrgm3REltm lefsest.zred3ndrgm3REsp lefsest.zred3ndrgm3REver lefsest.zred3ndrgm3REvis;
BY id;
IF Effect ~= '';
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.zred3ndrg*est as .csv;
PROC EXPORT data = lefslib.zred3ndrgM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrgM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrgM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrgM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrgM3fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM3fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.zred3ndrgM3rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrgM3rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
