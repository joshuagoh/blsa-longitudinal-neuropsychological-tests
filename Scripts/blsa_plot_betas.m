function X = blsa_plot_betas(B,V,yvar,xvar,dement,dem,S)

% Usage X = blsa_plot_betas(B,V,yvar,xvar,dement,dem,S)
%
% Plot scatterplot and histogram of slopes of test (yvar) by another test (xvar)
% or age of first test, for specified battery test.
%
% X      - Output X variable values
% B      - Matrix of slopes from blsa_extract_regression_est or
%          blsa_extract_csv_batter_SAS_est.
% V      - Cell array of test names in B.
% yvar   - String to specify neuropsych test in y-axis V to plot (see V).
% xvar   - String to specify neuropsych test in x-axis.
% dement - subj id for CI.
% dem    - Flag xvar as demographic (1) or not (0). e.g. 'age', 'sex',
%          'donset' etc., default 0.
% S      - Output from blsa_plot_spaghetti_pertest or
%          blsa_read_csv_battery_data.
%
% Created by Joshua Goh 29 Oct 2010.
% Modified by Joshua Goh 13 Dec 2010 - added dementia diagnosis.
% Modified by Joshua Goh 03032011 - moved dementia vector to
%                                   blsa_print_figures.m

% Allocations
k = strmatch(yvar,V);
if nargin<6; dem = 0; end
if dem
    if nargin<7
        error('S variable required for demographic x variables');
    else
        Vs = S.varnames;
        j  = strmatch(xvar,Vs);
        xname = Vs(j);
        scode = vertcat(S.subj.id);
    end
else
    j = strmatch(xvar,V);
    xname = ['Delta ' V(j)];
end

% Loop subjects
for i = 1:size(B,1)
    
    if dem;
        s = find(scode==B(i,1));
        X(i) = S.subj(s).variable(j).visvalue(1); % Get demographic x value at first test.
    else
        X(i) = B(i,j);
    end
    
    % Plot scatter
    subplot(2,1,1);
    if any(dement==B(i,1))
        color = 'r'; lw = 2;
    else
        color = 'k'; lw = 1;
    end
    h1 = plot(X(i),B(i,k),'o',...
        'MarkerEdgeColor',color,...
        'MarkerSize',4,...
        'MarkerFaceColor',color);
    
    set(h1,'Tag',['SNum: ' num2str(i) ' BLSA id: ' num2str(B(i,1))]);
    hold on;
    
end

% Scatter plot visual properties
title('Scatterplot','FontName','Arial','FontWeight','bold','FontSize',14);
ylabel(['Delta ' V(k)],...
    'FontName','Arial','FontWeight','bold');
xlabel(xname,'FontName','Arial','FontWeight','bold');
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@blsa_plot_spaghetti_update_datacursor);

hold off;

% Plot histogram
subplot(2,1,2);
hist(B(:,k),30);
title('Histogram','FontName','Arial','FontWeight','bold','FontSize',14);
xlabel([V(k) ' Score'],...
    'FontName','Arial','FontWeight','bold');
ylabel([V(k) ' Frequency'],'FontName','Arial','FontWeight','bold');
h2 = findobj(gca,'Type','patch');
set(h2,'FaceColor','k');

% Overall plot properties
set(gcf,'Color',[1 1 1]);