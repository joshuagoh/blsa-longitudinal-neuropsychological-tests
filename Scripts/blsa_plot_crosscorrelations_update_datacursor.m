function txt = blsa_plot_crosscorrelations_update_datacursor(empt,cobj)

% Graph datacursor properties
P = get(cobj,'Position');
RData = get(gco,'CData');
Tag = get(gco,'UserData');

txt = {['X: ', Tag{P(1)}],...
       ['Y: ', Tag{P(2)}],...
       ['R: ', num2str(RData(P(1),P(2)))]};