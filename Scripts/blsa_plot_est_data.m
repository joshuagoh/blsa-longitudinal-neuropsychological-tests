function blsa_plot_est_data(odata,eno,varinfo,v,poptions,mc,est)

% Plots horizontal bar graphs of an effect from SAS models in
% blsa_mixedmodel_cbaseage_interval.sas, by specified order (vector v opt).
%
% Usage blsa_plot_est_data(odata,eno,varinfo,v,mc,est)
%
% odata    - output matrix of estimates from blsa_reorg_est_csv.m
% eno      - effect no. 1 (intercept), 2, 3, etc. to plot. Depends on model.
% varinfo  - structure with odataV (variable labels) and varnames (variable
%            names)
% v        - vector of order of test variables.
% poptions - plot options structure with fields gca, gcf.
% mc       - multiple comparison correction - 'none' or 'bonf' (default).
% est      - specify 'beta' or 't' scores, default 't'.
%
% Modified from blsa_plot_est_zred3data.m by Josh Goh 03032011.

if nargin<6
    mc = 'bonf';
    est = 't';
elseif nargin<7
    est = 't';
end

% Allocations
% Parameters in odata.
sparams = {'Est' 'se' 'DF' 't' 'pt'};
nparams = length(sparams);
neffects = size(odata,2)/nparams;
t = 4+(eno-1)*5; % Get t column.
p = t + 1; % Get p column.
b = t - 3; % Get beta column.
bse = b + 1; % Get se column.
df = b + 2; % Get df column.

% odata variable original order.
odataV = varinfo.odataV;
varnames = varinfo.varnames;
varvec = varinfo.varvec;

% General plot options
if ~isempty(poptions.gca.Ylim),Ylim = poptions.gca.Ylim;else Ylim = [0 varvec(2)-varvec(1)+2]; end;
if ~isempty(poptions.gca.Position),aPos = poptions.gca.Position;else aPos = [.05 .04 .57 .89]; end;
if ~isempty(poptions.gcf.Position),fPos = poptions.gcf.Position;else fPos = [0 300 350 500]; end;

D = [];
for i = 1:length(v)
    V(i) = odataV(v(i));
    D(i,:) = odata(v(i),:);
    Vnames(i) = varnames(v(i));
end

% Reverse for horizontal bar plot
if strcmp('beta',est);
    c = b;
else
    c = t;
end
Y = D(length(v):-1:1,c); % Get beta or t values for plotting.
P = D(length(v):-1:1,p); % Get p values for labeling.
SE = D(length(v):-1:1,bse); % Get b se values.
DF = D(length(v):-1:1,df); % Get df values of betas.
TC = tinv(.975,DF); % 95% T Critical values.
V = V(length(v):-1:1);
vn = [length(v):-1:1];

% Plot horizontal bars
H = barh(Y,.5,'FaceColor',[0 .3 .7]);
set(gcf,'Color',[1 1 1]);
set(gcf,'Position',fPos);
set(gca,'XAxisLocation','top');
set(gca,'YTick',[]');
set(gca,'Position',aPos);

if strcmp('beta',est);
    hold on
    addpath /Users/gohjo/Research/Scripts/Matlab/
    CI = TC.*SE;
    errorb(Y,CI,'horizontal',...
        'LineWidth',1);
    title(['Beta values'],'FontName','Arial','FontWeight','bold','FontSize',14);
    
    % Specific plot options
    if ~isempty(poptions.gca.sld),sld = poptions.gca.sld;else sld = .08;end;
    if ~isempty(poptions.gca.tld),tld = poptions.gca.tld;else tld = .05; end;
    if ~isempty(poptions.gca.Xlim),Xlim = poptions.gca.Xlim;else Xlim = get(gca,'Xlim'); end;
    if ~isempty(poptions.gca.XTick),XTick = poptions.gca.XTick;else XTick = get(gca,'XTick'); end;
    if ~isempty(poptions.gca.YTick),YTick = poptions.gca.YTick;else YTick = get(gca,'YTick'); end;
    
    set(gca,'Xlim',Xlim,'XTick',XTick,'Ylim',Ylim,'YTick',YTick);
    
    for i = 1:length(v)
        
        % Variable on Axis
        h(i) = text(Xlim(2)+tld*Xlim(2),vn(i),Vnames{i},'FontName','Arial','FontSize',10);
        
        % Label significance with uncorrected alpha
        if Y(vn(i)) < 0
            halign = 'right';
        else
            halign = 'left';
        end
        if strcmp('none',mc)
            if P(vn(i))<.01
                text(Y(vn(i))+sign(Y(vn(i)))*CI(vn(i))+sld*sign(Y(vn(i)))*CI(vn(i)),vn(i)-.1,'**','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',10,'Color',[1 0 0]);
            elseif P(vn(i))<.05;
                text(Y(vn(i))+sign(Y(vn(i)))*CI(vn(i))+sld*sign(Y(vn(i)))*CI(vn(i)),vn(i)-.1,'*','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',10,'Color',[1 .5 0]);
            end
            
        elseif strcmp('bonf',mc)
            
            % Label significance with bonferroni corrected alpha
            if P(vn(i))<(.01/length(v));
                text(Y(vn(i))+sign(Y(vn(i)))*CI(vn(i))+sld*sign(Y(vn(i)))*CI(vn(i)),vn(i)-.1,'**','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',9,'Color',[1 0 0]);
            elseif P(vn(i))<(.05/length(v));
                text(Y(vn(i))+sign(Y(vn(i)))*CI(vn(i))+sld*sign(Y(vn(i)))*CI(vn(i)),vn(i)-.1,'*','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',9,'Color',[1 .5 0]);
            end
        end
        
    end
    
else
    title(['t values'],'FontName','Arial','FontWeight','bold','FontSize',14);
    axis([-10 10 0 length(v)+1]);
    
    Xlim = get(gca,'Xlim');
    
    for i = 1:length(v)
        h(i) = text(Xlim(2)+1,vn(i),Vnames{i},'FontName','Arial','FontSize',10);
        
        % Label significance with uncorrected alpha
        if Y(vn(i)) < 0
            halign = 'right';
        else
            halign = 'left';
        end
        
        if strcmp('none',mc)
            if P(vn(i))<.01
                text(Y(vn(i))+.9*sign(Y(vn(i))),vn(i)-.1,'**','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',10,'Color',[1 0 0]);
            elseif P(vn(i))<.05;
                text(Y(vn(i))+.9*sign(Y(vn(i))),vn(i)-.1,'*','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',10,'Color',[1 .5 0]);
            end
            
        elseif strcmp('bonf',mc)
            
            % Label significance with bonferroni corrected alpha
            if P(vn(i))<(.01/length(v));
                text(Y(vn(i))+.9*sign(Y(vn(i))),vn(i)-.1,'**','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',9,'Color',[1 0 0]);
            elseif P(vn(i))<(.05/length(v));
                text(Y(vn(i))+.9*sign(Y(vn(i))),vn(i)-.1,'*','HorizontalAlignment',halign,'FontName','Arial','FontWeight','bold','FontSize',9,'Color',[1 .5 0]);
            end
        end
        
    end
    
end

set(gca,'Ylim',Ylim);

hold off


