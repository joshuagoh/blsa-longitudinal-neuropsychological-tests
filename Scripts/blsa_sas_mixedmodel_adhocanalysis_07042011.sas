* RUN MIXED MODEL ON COMPZREDNDALLDATA;
%macro mixedmodelcompzredndalldata(varnames); * COMPZREDNDALLDATA;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");

		* MODEL 1 - for testing interval effect;
		PROC MIXED data=lefslib.compzredndalldata METHOD = reml covtest;
			CLASS id;
			MODEL &dv = interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndalldatam1FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndalldatam1RE&dv;

		RUN;

		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndalldatam1FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndalldatam1FE&dv;
		PROC SORT DATA = lefsest.compzredndalldatam1FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndalldatam1FE&dv;
		SET lefsest.compzredndalldatam1FE&dv;
		FORMAT pt&dv 8.6;
		RUN;

		* Only rename estimates for random effects;
		DATA lefsest.compzredndalldatam1RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndalldatam1RE&dv;
		PROC SORT DATA = lefsest.compzredndalldatam1RE&dv; by id; RUN;


		* MODEL 2 - for testing centered baseline age and baseline age x interval effects;
		PROC MIXED data=lefslib.compzredndalldata METHOD = reml covtest;
			CLASS id;
			MODEL &dv = cbaseage interval cbaseage*interval/DDFM = kr SOLUTION;
			RANDOM intercept interval/ TYPE=un SUBJECT=id SOLUTION;

			/* Select estimates and store in database;*/
			ODS SELECT Mixed.SolutionF Mixed.SolutionR;
			ODS OUTPUT Mixed.SolutionF = lefsest.compzredndalldatam2FE&dv;
			ODS OUTPUT Mixed.SolutionR = lefsest.compzredndalldatam2RE&dv;
			
		RUN;
		
		* Rename variables;
		* Keep all stats for fixed effects;
		DATA lefsest.compzredndalldatam2FE&dv (RENAME = (Estimate=Est&dv StdErr=se&dv DF=DF&dv tValue=t&dv Probt=pt&dv));
		SET lefsest.compzredndalldatam2FE&dv;
		PROC SORT DATA = lefsest.compzredndalldatam2FE&dv; by Effect; RUN;
		
		* Format < .0001 in pt;
		DATA lefsest.compzredndalldatam2FE&dv;
		SET lefsest.compzredndalldatam2FE&dv;
		FORMAT pt&dv 8.6;
		RUN;
		
		* Only rename estimates for random effects;
		DATA lefsest.compzredndalldatam2RE&dv (RENAME = (Estimate=Est&dv));
		SET lefsest.compzredndalldatam2RE&dv;
		PROC SORT DATA = lefsest.compzredndalldatam2RE&dv; by id; RUN;

		DM 'CLEAR LOG';
		DM 'CLEAR OUTPUT';

		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%mixedmodelcompzredndalldata(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);

* Merge variables into lefslib;
DATA lefslib.compzredndalldataM1fest;
MERGE lefsest.compzredndalldatam1FEabstr lefsest.compzredndalldatam1FEcap lefsest.compzredndalldatam1FEchunk lefsest.compzredndalldatam1FEdisc lefsest.compzredndalldatam1FEinhib lefsest.compzredndalldatam1FEmanip lefsest.compzredndalldatam1FEpretr lefsest.compzredndalldatam1FEsretr lefsest.compzredndalldatam1FEswtch lefsest.compzredndalldatam1FEltm lefsest.compzredndalldatam1FEstm lefsest.compzredndalldatam1FEspd lefsest.compzredndalldatam1FEvocab lefsest.compzredndalldatam1FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndalldataM1rest;
MERGE lefslib.alldataBaseline lefsest.compzredndalldatam1REabstr lefsest.compzredndalldatam1REcap lefsest.compzredndalldatam1REchunk lefsest.compzredndalldatam1REdisc lefsest.compzredndalldatam1REinhib lefsest.compzredndalldatam1REmanip lefsest.compzredndalldatam1REpretr lefsest.compzredndalldatam1REsretr lefsest.compzredndalldatam1REswtch lefsest.compzredndalldatam1REltm lefsest.compzredndalldatam1REstm lefsest.compzredndalldatam1REspd lefsest.compzredndalldatam1REvocab lefsest.compzredndalldatam1REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

DATA lefslib.compzredndalldataM2fest;
MERGE lefsest.compzredndalldatam2FEabstr lefsest.compzredndalldatam2FEcap lefsest.compzredndalldatam2FEchunk lefsest.compzredndalldatam2FEdisc lefsest.compzredndalldatam2FEinhib lefsest.compzredndalldatam2FEmanip lefsest.compzredndalldatam2FEpretr lefsest.compzredndalldatam2FEsretr lefsest.compzredndalldatam2FEswtch lefsest.compzredndalldatam2FEltm lefsest.compzredndalldatam2FEstm lefsest.compzredndalldatam2FEspd lefsest.compzredndalldatam2FEvocab lefsest.compzredndalldatam2FEvis;
BY Effect;
RUN;

DATA lefslib.compzredndalldataM2rest;
MERGE lefslib.alldataBaseline lefsest.compzredndalldatam2REabstr lefsest.compzredndalldatam2REcap lefsest.compzredndalldatam2REchunk lefsest.compzredndalldatam2REdisc lefsest.compzredndalldatam2REinhib lefsest.compzredndalldatam2REmanip lefsest.compzredndalldatam2REpretr lefsest.compzredndalldatam2REsretr lefsest.compzredndalldatam2REswtch lefsest.compzredndalldatam2REltm lefsest.compzredndalldatam2REstm lefsest.compzredndalldatam2REspd lefsest.compzredndalldatam2REvocab lefsest.compzredndalldatam2REvis;
BY id;
DROP StdErrPred DF tValue Probt;
RUN;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.compzredndalldataM1fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndalldataM1fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndalldataM1rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndalldataM1rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndalldataM2fest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndalldataM2fest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

PROC EXPORT data = lefslib.compzredndalldataM2rest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzredndalldataM2rest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
