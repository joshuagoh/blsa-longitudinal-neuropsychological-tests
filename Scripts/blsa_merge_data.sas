option nofmterr; run; * Switch off format errors ;

/* Tests needed for LEFS (Longitudinal Executive Function Study):
		- Trails Making (COGLIB.NEUTRAIL)
			- TRATS
			- TRBTS
			- TRBA
		- Alpha Span (COGLIB.NEUOTHER)
			- ALPHAS
		- Similarities (COGLIB.SIM)
			- SIMTOT
		- CVLT (COGLIB.CVL) 
			- CVLTCA
			- CVLCA1
			- CVLCA2
			- CVLCA3
			- CVLCA4
			- CVLCA5
			- CVLTCB
			- CVLFRS
			- CVLCRS
			- CVLFRL
			- CVLCRL
			- CVLRS1
			- CVLRS2
			- CVLRS3
			- CVLRS4
			- CVLRS5
			- CVLRST
			- CVLRX1
			- CVLRX2
			- CVLRX3
			- CVLRX4
			- CVLRX5
			- CVLRXT
			- CVLBET
			- CVLTPE
			- CVLTIF
			- CVLTIC
			- CVLDIS (note: this is now d' for LEFS)
			- CVLRBI (note: this is now C for LEFS)
			- CVLSV5
			- CVLLV5
			- CVLSVS
			- CVLLVS
			- CVLLVL
		- Verbal Fluency (COGLIB.NEUFLU)
			- FLUCAT
			- FLULET
			- FLUCTW
			- FLULTW
			- FLUCTI
			- FLULTI
			- FLUCTP
			- FLULTP
			- FLUCFW
			- FLUCFW
			- FLUCFP
			- FLUCFI
			- FLUCAW
			- FLUCAP
			- FLUCAI
			- FLUCVW
			- FLUCVP
			- FLUCVI
			- FLULFW
			- FLULFP
			- FLULFI
			- FLULAW
			- FLULAP
			- FLULAI
			- FLULSW
			- FLULSP
			- FLULSI
		- PMA (COGLIB.PMAVOC) 
			- PMATO3
		- Digit Span (COGLIB.DISPAN)
			- DIGBAC
			- DIGFOR
		- Card Rotations (COGLIB.CRDROT)
			- CRDROT
		- Boston Naming Test (COGLIB.NEUDAT)
			- BOSCOR
		- BVRT (COGLIB.BVR)
			- BVRTOT 
		- Dementia Diagonsis (DEMENT.DEMENT2010)
            - DONSET
            - DODX
            - DX
        - Daily Function (COGLIB.NEUDAT)
            - PFAQ01
            - PFAQ02
            - PFAQ03
            - PFAQ04
            - PFAQ05
            - PFAQ06
            - PFAQ07
            - PFAQ08
            - PFAQ09
            - PFAQ10

Goal: Set LEFS libraries for NI sample;
Script created by Josh Goh 30 Sept 2010;
Script modified by Josh Goh 3 Oct 2010 - include CSR;
Script modified by Josh Goh 19 Oct 2010 - major modification of domain organization;
Script modified by Josh Goh 25 Oct 2010 - Added dementia diagnosis and daily function (Pfeffer);
Script modified by Josh Goh 1 Nov 2010 - Added Sex, created Baseline Age and Interval. 
Script modified by Josh Goh 10 Nov 2010 - Transformed Baseline Age relative to minimum (Centering to make 0 Baseline Age meningful).
Script modified by Josh Goh 16 Nov 2010 - Added Verbal Flueny #words, #persev, #instrus. 
Script modified by Josh Goh 18 Nov 2010 - Added Boston Naming BOSCOR.
Script modified by Josh Goh 30 Nov 2010 - Added fluctw flultw flucti fluctp flulti flultp
Script modified by Josh Goh 1 Dec 2010 - Added trba, cvlsv5, cvllv5, cvlsvs, cvllvs, cvllvl; Removed trate, trbte, cvlbei.
Script modified by Josh Goh 6 Dec 2010 - Changed formula for cvldis and cvlrbi to signal detection theory based calculations.
Script modified by Josh Goh 9 Dec 2010 - Added standardized composite scores from LEFS_red3.xlsx and also export main.csv.
Script modified by Josh Goh 13 Dec 2010 - Added dementia classification to standardized composite scores.
Script modified by Josh Goh 14 Dec 2010 - Changed dementia database to dement.dement2010.
Script modified by Josh Goh 26 Jan 2011 - Renamed zred3nd and zred3ndg domains - Switching = Manipulation, Episodic Memory = Short-Term Memory;
										  Removed CVLT List B; Moved CVLT Short Free Recall from Retrieval to Short-Term Memory; Remove Bias, and Stimuli Dependence domain. */

* Setup LEFS library in file system;
libname lefslib 'Z:\Research\Projects\BLSA\Battery\Data\lefslib';

* Input data into LEFS
* Trails Making;
data lefslib.trails; set coglib.neutrail;
trba = trbts - trats;
keep id vi TRATS TRBTS TRBA; run;
proc sort data = lefslib.trails; by id vi; run;

* Alpha Span;
data lefslib.alphas; set coglib.neuother;
keep id vi alphas; run;
proc sort data = lefslib.alphas; by id vi; run;

* Similarities;
data lefslib.sim; set coglib.sim;
keep id vi simtot; run;
proc sort data = lefslib.sim; by id vi; run;

* CVLT;
data lefslib.cvlt; set coglib.cvl;
p_cvlhit = cvlhit/16;
if cvlhit = 16 then p_cvlhit = 1 - 1/32;
zcvlhit = probit(p_cvlhit);
p_cvlxfp = cvlxfp/28;
if cvlxfp = 0 then p_cvlxfp = 1/56;
zcvlxfp = probit(p_cvlxfp);
cvldis = zcvlhit - zcvlxfp;
cvlrbi = -0.5 * (zcvlhit + zcvlxfp);
keep id vi dot age ageyrs cvltca cvlca1	cvlca2	cvlca3	cvlca4	cvlca5	cvltcb	cvlfrs	cvlcrs	cvlfrl	cvlcrl	cvlrs1	cvlrs2	cvlrs3	cvlrs4	cvlrs5	cvlrst	cvlrx1	cvlrx2	cvlrx3	cvlrx4	cvlrx5	cvlrxt	cvlbet	cvltpe	cvltif	cvltic cvlhit cvlxfp cvldis	cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl;
drop cvlhit cvlxfp;
run;
proc sort data = lefslib.cvlt; by id vi; run;

* Verbal Fluency, Script modified by Josh Goh 30 Nov 2010 - Added fluctw flultw flucti fluctp flulti flultp;
data lefslib.vfluency; set coglib.neuflu;
fluctw = flucfw + flucaw + flucvw;
flultw = flulfw + flulaw + flulsw;
flucti = flucfi + flucai + flucvi;
fluctp = flucfp + flucap + flucvp;
flulti = flulfi + flulai + flulsi;
flultp = flulfp + flulap + flulsp;
keep id vi flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi; run;
proc sort data = lefslib.vfluency; by id vi; run;

* PMA;
data lefslib.pma; set coglib.pmavoc;
keep id vi dov pmato3; run; * switched back from pmatat to pmato3 20 Oct 2010;
proc sort data = lefslib.pma; by id vi; run;

* Stroop (NOTE: dropped from LEFS, see below);
data lefslib.stroop; set coglib.neustr; run; * keep all;
proc sort data = lefslib.stroop; by id vi; run;

* Digit Span;
data lefslib.dspan; set coglib.dispan;
keep id vi dov age ageyrs digfor digbac; run;
proc sort data = lefslib.dspan; by id vi; run;

* Calculations (NOTE: dropped from LEFS, see below);
data lefslib.calc; set coglib.neucal; run; * keep all;
proc sort data = lefslib.calc; by id vi; run;

* Card Rotations;
data lefslib.crdrot; set coglib.crdrot;
keep id vi dov crdrot; run;
proc sort data = lefslib.crdrot; by id vi; run;

* Boston Naming;
data lefslib.bnt; set coglib.neudat;
keep id vi boscor; run;
proc sort data = lefslib.bnt; by id vi; run;

* BVRT;
data lefslib.bvrt; set coglib.bvr;
keep id vi dot age ageyrs bvrtot; run;
proc sort data = lefslib.bvrt; by id vi; run;

* CSR;
* - included in script 3 Oct 2010, also updated merge and reorder below;
* - excluded from LEFS 20 Oct 2010 - tests not entirely relevant;
data lefslib.csr; set coglib.neucsr;
keep id vi csrdec; run;
proc sort data = lefslib.csr; by id vi; run;

* Still searching for MIS, Digit Symbol data;

* Dementia Diagnosis;
* - included in script 25 Oct 2010;
* - NOTE: no vi variable;
* - Changed database from coglib to dement.;
data lefslib.dementia; set dement.dement2010;
keep id donset dodx dx; run;
proc sort data = lefslib.dementia; by id; run;

* Daily Function (Pfeffer);
* - included in script 25 Oct 2010;
data lefslib.pfeffer; set coglib.neudat;
keep id vi pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10; run;
proc sort data = lefslib.pfeffer; by id vi; run;

/* Merge Dementia Diagnosis, Daily Function as temp Diagnostic data;
data lefslib.diagnostic;
merge lefslib.dementia lefslib.pfeffer;
by id;
run;
proc sort data = lefslib.diagnostic; by id; run;*/

* Merge LEFS cognitive libraries into one main table.;
data lefslib.main;
merge lefslib.trails lefslib.alphas lefslib.sim lefslib.cvlt lefslib.vfluency lefslib.pma lefslib.dspan lefslib.crdrot lefslib.bnt lefslib.bvrt lefslib.pfeffer;
*merge lefslib.trails lefslib.alphas lefslib.sim lefslib.cvlt lefslib.vfluency lefslib.pma lefslib.dspan lefslib.crdrot lefslib.bnt lefslib.bvrt lefslib.diagnostic;
by id vi;

* Sex - includeded in script 1 Nov 2010;
if id < 5000 then sex = 1;
if id >= 5000 then sex = 0;

run;

* Merge dementia onsets modified 14 Dec 2010;
data lefslib.main;
merge lefslib.main lefslib.dementia;
by id;
run;
proc sort data = lefslib.main; by id vi; run;
proc sort data = emslib.emsvis; by id vi; run;

* Reorder lefslib.main to make more sense;
data lefslib.main;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10; set lefslib.main;
run;

* Export lefslib.nimain as .csv;
PROC EXPORT data = lefslib.main 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\main.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Select NI subset from lefslib.main into lefslib.nimain;
data lefslib.nimain;
merge emslib.emsvis lefslib.main;
by id vi;
if emsvi ~= . and dot ~= .; * select only cognitive data for scan sessions;
*if inemvis = 1 and inmain = 1; * select only cognitive data for scan sessions;
drop psorder blsayr protocol fhxdone pettask status; * remove unnecessary info from nimain;
run;

* Create Baseline Age and Interval variables (for emsvis only). - Added Josh Goh 2 Nov 2010;
data lefslib.baseline; set lefslib.nimain;
by id;
baseage = age;
if first.id;
keep id baseage; run;
proc sort data = lefslib.baseline; by id; run;
data lefslib.nimain;
merge lefslib.nimain lefslib.baseline;
by id;
interval = age - baseage;
cbaseage = baseage - 49.0937500; * Center baseline age to minimum;
run;

* Reorder lefslib.nimain to make more sense;
data lefslib.nimain;
retain id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10;
set lefslib.nimain;
run;

* Export lefslib.nimain as .csv;
PROC EXPORT data = lefslib.nimain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\nimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

/* Reduce, Standardize, and form Composite Scores*/
* Reduce tests;
data lefslib.zred3nimain; set lefslib.nimain;
yr = year(DOV);
keep id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot donset yr;
*set lefslib.zred3nimain;
format trats alphas digbac digfor boscor bvrtot;
run;

* New database lefslib.zred3ndnimain with data from dementia onwards removed.;
data lefslib.zred3ndnimain; set lefslib.zred3nimain;
if yr < donset | donset = .;
run;

* Standardize lefslib.zred3nimain scores;
PROC STANDARD DATA=lefslib.zred3nimain MEAN=0 STD=1 OUT=lefslib.zred3nimain;
	VAR	trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot;
RUN;

* Composite scores from lefslib.zred3nimain (Averaged: equal weighting from each sub-test, Sign changed so lower score is poorer performance);
data lefslib.zred3nimain; set lefslib.zred3nimain;

bias = cvlrbi;
cap = digfor;
efa = (simtot + cvlrst)/2;
efd = (-cvltif - cvlsv5 + cvldis)/3;
efi = (-cvltpe - fluctp - flultp)/3;
efr = (cvlfrs + cvlcrs + cvlcrl + fluctw)/4;
efs = (alphas + digbac - trba)/3;
em  = (cvlca1 + cvltca + cvltcb)/3;
ltm = (cvlfrl - cvllv5 - cvllvs)/3;
sd  = (-cvlsvs - cvllvl - cvlrxt)/3;
sp  = -trats;
ver = (boscor + pmato3 + flultw)/3;
vis = (-bvrtot + crdrot)/2;

label 	bias = 'bias'
		cap  = 'capacity'
		efa  = 'abstraction'
		efd  = 'discrimination'
		efi  = 'inhibition'
		efr  = 'retrieval'
		efs  = 'switching'
		em   = 'episodic memory'
		ltm  = 'long-term memory'
		sd   = 'stimulus dependency'
		sp   = 'speed'
		ver  = 'verbal'
		vis  = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot yr;
run;

data lefslib.zred3nimain;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval bias cap efa efd efi efr efs em ltm sd sp ver vis donset;
set lefslib.zred3nimain;
run;

* Export lefslib.zred3nimain as .csv;
PROC EXPORT data = lefslib.zred3nimain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3nimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

* Standardize lefslib.zred3ndnimain scores;
PROC STANDARD DATA=lefslib.zred3ndnimain MEAN=0 STD=1 OUT=lefslib.zred3ndnimain;
	VAR	trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot;
RUN;

* New database lefslib.zred3ndrnimain for revised domains.;
data lefslib.zred3ndrnimain; set lefslib.zred3ndnimain;
run;

* Composite scores from lefslib.zred3ndnimain (Averaged: equal weighting from each sub-test, Sign changed so lower score is poorer performance);
data lefslib.zred3ndnimain; set lefslib.zred3ndnimain;

bias = cvlrbi;
cap = digfor;
efa = (simtot + cvlrst)/2;
efd = (-cvltif - cvlsv5 + cvldis)/3;
efi = (-cvltpe - fluctp - flultp)/3;
efr = (cvlfrs + cvlcrs + cvlcrl + fluctw)/4;
efs = (alphas + digbac - trba)/3;
em  = (cvlca1 + cvltca + cvltcb)/3;
ltm = (cvlfrl - cvllv5 - cvllvs)/3;
sd  = (-cvlsvs - cvllvl - cvlrxt)/3;
sp  = -trats;
ver = (boscor + pmato3 + flultw)/3;
vis = (-bvrtot + crdrot)/2;

label 	bias = 'bias'
		cap  = 'capacity'
		efa  = 'abstraction'
		efd  = 'discrimination'
		efi  = 'inhibition'
		efr  = 'retrieval'
		efs  = 'switching'
		em   = 'episodic memory'
		ltm  = 'long-term memory'
		sd   = 'stimulus dependency'
		sp   = 'speed'
		ver  = 'verbal'
		vis  = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot yr;
run;

data lefslib.zred3ndnimain;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval bias cap efa efd efi efr efs em ltm sd sp ver vis donset;
set lefslib.zred3ndnimain;
run;

* Export lefslib.zred3ndnimain as .csv;
PROC EXPORT data = lefslib.zred3ndnimain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndnimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;


* Revised composite scores from lefslib.zred3ndnimain (Averaged: equal weighting from each sub-test, Sign changed so lower score is poorer performance);
data lefslib.zred3ndrnimain; set lefslib.zred3ndrnimain;

cap = digfor;
efa = (simtot + cvlrst)/2;
efd = (-cvltif - cvlsv5 + cvldis)/3;
efi = (-cvltpe - fluctp - flultp)/3;
efr = (cvlcrs + cvlcrl + fluctw)/3;
efm = (alphas + digbac - trba)/3;
stm = (cvlfrs + cvlca1 + cvltca)/3;
ltm = (cvlfrl - cvllv5 - cvllvs)/3;
sp  = -trats;
ver = (boscor + pmato3 + flultw)/3;
vis = (-bvrtot + crdrot)/2;

label 	cap  = 'capacity'
		efa  = 'abstraction'
		efd  = 'discrimination'
		efi  = 'inhibition'
		efr  = 'retrieval'
		efm  = 'manipulation'
		stm  = 'short-term memory'
		ltm  = 'long-term memory'
		sp   = 'speed'
		ver  = 'verbal'
		vis  = 'visual';

drop EMSid emsfn EMSwave DOV DOT mridone petdone trats trba alphas simtot cvltca cvlca1 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrst cvlrxt cvltpe cvltif cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl fluctw flultw fluctp flultp pmato3 digbac digfor crdrot boscor bvrtot yr;
run;

data lefslib.zred3ndrnimain;
retain id sex vi emsvi baseage age cbaseage AgeYrs interval cap efm efi efa efd efr stm ltm sp ver vis donset;
set lefslib.zred3ndrnimain;
run;

* Export lefslib.zred3ndrnimain as .csv;
PROC EXPORT data = lefslib.zred3ndrnimain 
            OUTFILE= "Z:\Research\Projects\BLSA\Battery\Data\lefslib\zred3ndrnimain.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
