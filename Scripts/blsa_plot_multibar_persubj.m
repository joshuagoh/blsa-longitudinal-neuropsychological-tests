function P = blsa_plot_multibar_persubj(S,subj,bartype,tp)

% Usage: P = blsa_plot_multibar_persubj(S,subj,bartype,tp)
%
% Plot multivariate bars of scores across all neuropsych tests
% for a specific subject, for a specific length of timepoints.
%
% P    - output data over tp.
% S    - S structure variable containing data (default generated from
%        blsa_read_csv_battery_data)
% subj - BLSA id number
% bartype - 'GroupedBars'
%           'StackedBars'
%           'Lines'
%           '3dBarPlot'
%           'SubPlots'
%           'Glyphs' - requires Statistics toolbox
% tp   - vector of timepoint(s)/visit(s) to plot, default is total length
%        of visits for the subject.
%
% Created by Josh Goh 1 Oct 2010
% Modified by Josh Goh 2 Nov 2010 - included sex, baseage, interval.

% Allocations
V = S.varnames;
nvar = length(V);
scode = vertcat(S.subj.id);
I = find(scode==subj);
cogstart = strmatch('trats',V); % Start of cognitive variables column index.
cogend = strmatch('bvrtot',V);

if nargin<4
    tp=1:length(S.subj(I).variable(1).visvalue);
end

% Loop timepoints
P = [];
for t = 1:length(tp);
    ti = tp(t);
    ki=1;
    for k = cogstart:cogend;
        P(t,ki) = S.subj(I).variable(k).visvalue(ti);
        ki=ki+1;
    end
end

% Plot
switch bartype
    case 'GroupedBars'
        bar(P);
        xlabel('Visit','FontName','Arial','FontWeight','bold');
        ylabel('Score','FontName','Arial','FontWeight','bold');
        title(sprintf('BLSA ID %d',subj),'FontName','Arial','FontWeight','bold','FontSize',14);
        legend(V(cogstart:cogend),'Location','EastOutside');
        
    case 'StackedBars'
        bar(P,'stacked');
        xlabel('Visit','FontName','Arial','FontWeight','bold');
        ylabel('Score','FontName','Arial','FontWeight','bold');
        title(sprintf('BLSA ID %d',subj),'FontName','Arial','FontWeight','bold','FontSize',14);
        legend(V(cogstart:cogend),'Location','EastOutside');
        
    case 'Lines'
        plot(P,'-o',...
            'LineWidth',2,...
            'MarkerSize',5);
        xlabel('Visit','FontName','Arial','FontWeight','bold');
        ylabel('Score','FontName','Arial','FontWeight','bold');
        title(sprintf('BLSA ID %d',subj),'FontName','Arial','FontWeight','bold','FontSize',14);
        legend(V(cogstart:cogend),'Location','EastOutside');
        
        
    case '3dBarPlot'
        bar3(P);
        xlabel('Variables','FontName','Arial','FontWeight','bold');
        % set(gca,'XTickLabel',V(13:end));
        ylabel('Visit','FontName','Arial','FontWeight','bold');
        zlabel('Score','FontName','Arial','FontWeight','bold');
        title(sprintf('BLSA ID %d',subj),'FontName','Arial','FontWeight','bold','FontSize',14);
        legend(V(cogstart:cogend),'Location','EastOutside');
        
    case 'SubPlots'
        r = ceil(length(tp)/3);
        for t=1:length(tp);
            subplot(r,3,t),
            plot(P(t,:),'-ko','LineWidth',2,'MarkerSize',5,'MarkerEdgeColor','k','MarkerFaceColor','k');
        end
        
    case 'Glyphs'
        P(isnan(P))=0;
        glyphplot(P,'Standardize','matrix','VarLabels',V(cogstart:cogend),'Color','black')
        title(sprintf('BLSA ID %d',subj),'FontName','Arial','FontWeight','bold','FontSize',14);
end

set(gcf,'Color',[1 1 1]);