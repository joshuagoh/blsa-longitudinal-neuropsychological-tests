function blsa_print_figures(db,flag)

if nargin < 1
    db = 1; % Specify database, or nimain1 default.
elseif nargin < 2;
    db = 1; % Specify database, or nimain1 default.
    flag = 'all';
end

dbase = {'nimain1',...
         'zredndnimain1',...
         'compzredndnimain1',...
         'zredndg1nimain1',...
         'compzredndg1nimain1',...
         'zredndg2nimain1',...
         'compzredndg2nimain1'};

%% ALLOCATIONS
style = hgexport('readstyle','custom');
addpath /Users/gohjo/Research/Projects/BLSA/Battery/Scripts/
cd('/Users/gohjo/Research/Projects/BLSA/Battery/Analysis');

load(sprintf('%s_DATA.mat',dbase{db}))
load(sprintf('%s_ESTIMATE.mat',dbase{db}))
plotfolder = sprintf('/Users/gohjo/Research/Projects/BLSA/Battery/Analysis/Plots/%s/',dbase{db});
mkdir(plotfolder);
cd(plotfolder);

varvec = O.varvec;
varnames = O.varnames;
svar = 3; % variable start column for V.
evar = svar + varvec(2)-varvec(1); % variable end column for V.
v = [1:(evar-2)]; % Variable order;

% Plot options
varinfo.odataV = S.varnames(varvec(1):varvec(2));
varinfo.varnames = varnames;
varinfo.varvec = varvec;

dement = [38,106,120,358,395,414,415,553,559,781,1479,1485,1492,1535,...
          1543,1562,1566,1568,1582,1587,1622,5022,5045,5097,5242,5300,...
          5355,5421,5507,5513,5665,5672,5714,5963];
%dement = [];

% Bonferroni thresholds.
nt = length(v);
ncomp = (nt-1)^2+nt;
bp05 = .05/ncomp;
bp01 = .01/ncomp;
p = bp05;

% Computations
% Sum Fixed and Random effects slopes without dg in model
m1B1b1 = [zeros(size(m1rb1,1),2) repmat(m1fB1,size(m1rb1,1),1)] + m1rb1;
if db > 3    
    % Sum Fixed and Random effects slopes with dg in model
    m1B2b1 = [zeros(size(m1rb1,1),2) repmat(m1fB2,size(m1rb1,1),1)] + m1rb1;
end

%% SPAGHETTI PLOTS, HISTOGRAMS, AND BETA SCATTERPLOTS
if strcmp('all',flag) || strcmp('scatterplots',flag)

for i=svar:evar
    S = blsa_plot_spaghetti_pertest(S,'all',m2rV(i),'age','predlin','s',dement);
    set(gca,'XLim',[55 95]);
    hgexport(1,[m2rV{i} ' by age modeli predicted lines'],style);
end;

close all

for i=svar:evar
    S = blsa_plot_spaghetti_pertest(S,'all',m2rV(i),'age','raw','sh',dement);
    hgexport(1,[m2rV{i} ' raw x age scatter histo'],style);
end;

close all

for i=svar:evar
    X = blsa_plot_betas(m1rb1,m1rV,m2rV(i),'age',dement,1,S);
    hgexport(1,[m2rV{i} ' slopes(r) x age scatter histo'],style);
end;

close all

for i=svar:evar
    blsa_plot_mixedmodel_spaghetti([m2fB1;m2fB2;m2fB3]',m2fB0,m2rb1,m2rb0,m2rV,S,m2rV(i),dement);
    set(gca,'XLim',[55 95]);
    hgexport(1,[m2rV{i} ' by age model2 predicted lines'],style);
end

close all

end

%% PLOT BETA (95% CI) VALUE BAR GRAPHS
%% Estimate effect sizes for domains (see Neuropsych_Domains.xlsx, LEFS_red2).
if strcmp('all',flag) || strcmp('betas',flag)
    
if db <= 3 % Whole Group
    
    blsa_plot_est_data(m1fo,2,varinfo,v,O.poptions.i.est,'none','beta')
    hgexport(1,'Model1IntervalEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,2,varinfo,v,O.poptions.a.est,'none','beta')
    hgexport(1,'Model2BaseageEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,3,varinfo,v,O.poptions.ia.est,'none','beta')
    hgexport(1,'Model2BaseagexIntervalEffectbetavals_uncorr',style);
    
else
    
    % Cognitively Normal (CN) or Cognitively Impaired (CI) mean effects
    blsa_plot_est_data(m1fo,3,varinfo,v,O.poptions.i.est,'none','beta')
    hgexport(1,'Model1IntervalEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,2,varinfo,v,O.poptions.a.est,'none','beta')
    hgexport(1,'Model2BaseageEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,4,varinfo,v,O.poptions.ia.est,'none','beta')
    hgexport(1,'Model2BaseagexIntervalEffectbetavals_uncorr',style);
    
    % Difference effects (interaction of mean effects) between CN and CI
    blsa_plot_est_data(m1fo,4,varinfo,v,O.poptions.i.est,'none','beta')
    hgexport(1,'Model1_CNvsCI_IntervalEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,3,varinfo,v,O.poptions.a.est,'none','beta')
    hgexport(1,'Model2_CNvsCI_BaseageEffectbetavals_uncorr',style);
    
    blsa_plot_est_data(m2fo,5,varinfo,v,O.poptions.ia.est,'none','beta')
    hgexport(1,'Model2_CNvsCI_BaseagexIntervalEffectbetavals_uncorr',style);
    
end

close all

end

%% PLOT CROSS CORRELATIONS
% Set export style to OpenGL for cross-correlation figures.
style.renderer = 'zbuffer';
style.FontSizeMin = 6;

if strcmp('all',flag) || strcmp('correlations',flag)
    
    pv = num2str(p);

% Whole Group
if db <= 3
    
    % Plot correlations for interval(r) from Model 1.
    blsa_crosscorrelations(m1B1b1,varinfo,v,O.poptions.R,1,p,'all');
    hgexport(1,['Model1IntervalEffectCrossCorrelations_p' pv(3:4)],style);
    
    % Plot correlations for cross-sectional age.
    blsa_crosscorrelations(B,varinfo,v,O.poptions.R,1,p,'pairwise');
    hgexport(1,['CrossSectionalAgeEffectCrossCorrelations_p' pv(3:4)],style);
    
    % Plot mixed diagonal correlations for cross-sectional and longitudinal matrices.
    blsa_plot_mixeddiag_correlations(m1B1b1,B,varinfo,v,O.poptions.R,0,p);
    hgexport(1,['CrossSectionalAgeIntervalEffectCrossCorrelations_p' pv(3:4)],style);

% Split CN/CI group
else
    
    % Get dementia group per subject
    g = strmatch('dg',C.varnames);
    dg = [C.variable(1).data(:,1) C.variable(g).data(:,1)];
    m1B2b1cn = m1B2b1(find(dg(:,2)==0),:);
    m1B2b1ci = m1B2b1(find(dg(:,2)==1),:);
    Bcn = B(find(dg(:,2)==0),:);
    Bci = B(find(dg(:,2)==1),:);
    
    % CN
    % Interval
    blsa_crosscorrelations(m1B2b1cn,varinfo,v,O.poptions.R,1,p,'all');
    hgexport(1,['Model1IntervalEffectCrossCorrelations_p' pv(3:4) '_cn'],style);
    
    % Age
    blsa_crosscorrelations(Bcn,varinfo,v,O.poptions.R,1,p,'pairwise');
    hgexport(1,['CrossSectionalAgeEffectCrossCorrelations_p' pv(3:4) '_cn'],style);
    
    % Plot mixed diagonal correlations for cross-sectional and longitudinal matrices.
    blsa_plot_mixeddiag_correlations(m1B2b1cn,Bcn,varinfo,v,O.poptions.R,0,p);
    hgexport(1,['CrossSectionalAgeIntervalEffectCrossCorrelations_p' pv(3:4) '_cn'],style);
    
    % DG
    % Interval
    blsa_crosscorrelations(m1B2b1ci,varinfo,v,O.poptions.R,1,p,'all');
    hgexport(1,['Model1IntervalEffectCrossCorrelations_p' pv(3:4) '_ci'],style);
    
    % Age
    blsa_crosscorrelations(Bci,varinfo,v,O.poptions.R,1,p,'pairwise');
    hgexport(1,['CrossSectionalAgeEffectCrossCorrelations_p' pv(3:4) '_ci'],style);
    
    % Plot mixed diagonal correlations for cross-sectional and longitudinal matrices.
    blsa_plot_mixeddiag_correlations(m1B2b1ci,Bci,varinfo,v,O.poptions.R,0,p);
    hgexport(1,['CrossSectionalAgeIntervalEffectCrossCorrelations_p' pv(3:4) '_ci'],style);
    
    close all
    
end
end

close all