function odata = blsa_reorg_est_csv(csvfname,db)

% Reorganize fixed effects estimates from SAS models.
% csvfname - e.g. 'M1fest.csv' or 'M2fest.csv'.
%
% Usage odata = blsa_reorg_est_csv(csvfname,db)
%
% Created by Joshua Goh 03032011.

O = blsa_read_csv_switcher(csvfname,db,'fest');

idata = O.idata;
stats = O.stats;
varnames = O.V;
Effect = O.effect;
sparams = {'Est' 'se' 'DF' 't' 'pt'};
m = 1;
odata = [];

for i = 1:length(varnames);
    n=1;
    for j = 1:length(Effect);
        for k = 1:length(sparams);
            var = strmatch([sparams{k} varnames{i}],stats) - 1;
            odata(m,n) = idata(j,var);
            n = n + 1;
        end
    end
    m = m + 1;
end

[fpath,fname,fext] = fileparts(csvfname);

csvwrite([fpath '/o' fname fext],odata);