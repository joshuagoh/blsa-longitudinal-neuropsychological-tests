* Merge scratchpad

* Merge random effect est and baseage;
data lefslib.bagereest;
merge lefslib.baseline lefslib.reest;
by id;
run;

* Export lefslib.*est as .csv;
PROC EXPORT data = lefslib.bagereest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\bageREest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;

