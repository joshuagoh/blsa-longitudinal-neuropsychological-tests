function blsa_plot_mixedmodel_spaghetti(fB,fA,rB,rA,rV,S,test,dement)

% Usage blsa_plot_mixedmodel_spaghetti(fB,fA,rB,rA,rV,S,test,dement)
%
% Plot predicted mixed model slopes by intervals (starting at centered baseages)
% for specified battery test.
% 
% fB     - (m x r) matrix of fixed effects slopes.
% fA     - (r x m) matrix of fixed effects intercepts.
% rB     - (n x m+2) matrix of random effects slopes (+1 for id and baseage
%          columns leftmost), right now only set for one random effect variable.
% rA     - (n x m+2) matrix of random effects intercepts (+1 id and baseage
%          columns leftmost), right now only set for one random effect variable.
% rV     - (1 x m+2) Cell array of id, baseage and test names.
% test   - String to specify neuropsych test in rV to plot (see rV).
% S      - Output from blsa_plot_spaghetti_pertest or
%          blsa_read_csv_battery_data.
% dement - subj id for CI.
%
% Created by Joshua Goh 29 Oct 2010.
% Modified by Joshua Goh 1 Nov 2010 - elaborate yhat model.
% Modified by Joshua Goh 17 Nov 2010 - adapted for blsa models 2.
% Modified by Joshua Goh 13 Dec 2010 - added dementia diagnosis.
% Modified by Joshua Goh 03032011 - moved dementia vector to
%                                   blsa_print_figures.m

scode = vertcat(S.subj.id);
V = S.varnames;
k = strmatch(test,rV);
bav = strmatch('baseage',V); % Get baseage variable no.
cbav = strmatch('cbaseage',V); % Get centered baseage variable no.
iv = strmatch('interval',V); % Get interval variable no.

for s = 1:size(rB,1)
    
    subj = strmatch(rB(s,1),scode);

    % Get subject X values (centered baseage, interval)
    x(1,:) = S.subj(subj).variable(cbav).visvalue;
    x(2,:) = S.subj(subj).variable(iv).visvalue;
    
    % Compute yhat
    B = [fB(k-2,:) rB(s,k)];
    X = [x(1,:);x(2,:);x(1,:).*x(2,:);x(2,:)]; % blsa model 2.
    C = fA(k-2) + rA(s,k);
    Yhat = B*X + C;
    
    % Make x-axis values using baseage
    bag = S.subj(subj).variable(bav).visvalue(1);
    age = x(2,:)+bag;
    
    % Plot
    if any(dement==rB(s,1))
        h = plot(age,Yhat,'-r','LineWidth',2);
    else
        h = plot(age,Yhat,'-k','LineWidth',1);
    end
    set(h,'Tag',['SNum: ' num2str(subj) ' BLSA id: ' num2str(scode(s))]);
    hold on
    
    clear x
    
end

% Graph visual properties
set(gcf,'Color',[1 1 1]);
title(rV(k),'FontName','Arial','FontWeight','bold','FontSize',14);
ylabel([rV(k)],'FontName','Arial','FontWeight','bold');
xlabel('Age','FontName','Arial','FontWeight','bold');
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@blsa_plot_spaghetti_update_datacursor);

% set(gca,'FontName','Arial','FontWeight','bold');
hold off;