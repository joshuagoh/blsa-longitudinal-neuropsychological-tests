function [B V] = blsa_extract_csv_battery_SAS_est(csvfname,db,rand,eff)

% Reads .csv file containing mixed model estimates computed from SAS proc
% mixed and outputs a matrix of model parameter estimates, B.
%
% Usage: [B V] = blsa_extract_csv_battery_SAS_est(csvfname,db,rand,eff)
%
% csvfname - string of filename containing SAS estimate output (e.g. 
%            'M3rest.csv' or 'M3fest.csv').
% db       - database no.
% rand     - string 'rand' or 'fixed' specifying .csv estimate type.
% eff      - string specifying effect to extract.
%
%                 Non-ndg models have these effects:
%                 1. 'Intercept' - fixed or random intercept.
%                 2. 'cbaseage' - fixed centered baseline age.
%                 3. 'interval' - fixed or random interval. 
%                 4. 'cbaseage*interval' - fixed interaction.
%
%                 ndg models have these effects:
%                 1. 'Intercept' - fixed or random intercept.
%                 2. 'cbaseage' - fixed centered baseline age.
%                 3. 'cbaseage*dg' - fixed interaction.
%                 4. 'cbaseage*interval' - fixed interaction.
%                 5. 'cbaseage*interval*dg' - fixed interaction.
%                 6. 'dg' - fixed dementia classification.
%                 7. 'interval' - fixed or random interval. 
%                 8. 'interval*dg' - fixed interaction.
% B        - output n x m matrix of estimates (n: no. subj; m: no. 
%            variables, 1st column is id, 2nd column is baseage).
% V        - output 1 x m cell of column variable names.
%
% Created by Joshua Goh 03032011.

if strcmp('rand',rand)    
    O = blsa_read_csv_switcher(csvfname,db,'rest');
else % if 'fixed'
    O = blsa_read_csv_switcher(csvfname,db,'fest');
end

effect = O.effect;
data   = O.data;
V      = O.V;

% Loop rows
I = strcmp(eff,effect);
B = [];
b = 1;
for s = 1:size(data,1)
    if I(s)
        B(b,:) = data(s,:); b = b + 1; % Extract slopes, for random effects, 1st column is id, 2nd column is baseage.
    end
end