function txt = blsa_plot_spaghetti_update_datacursor(empt,cobj)

% Graph datacursor properties
P = get(cobj,'Position');
T = get(gco,'Tag');

txt = {[T],...
    ['Age: ', num2str(P(1))],...
    ['Score: ', num2str(P(2))]};