function O = blsa_read_csv_switcher(csvfname,db,type)

% Reads csv files with preset format and encodes preset info for subsequent processing.
% csvfname - string of csv filename
% db       - database no. (see blsa_process_batter_*.m)
% type     - type of csv file: 'battery','icc','fest','rest'
% O        - variable output structure

clear O;

% For BATTERY
switch type
    case 'battery'
        switch db
            
            % Extract
            case 1 % nimain1
                [id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10] = textread(csvfname,'%d%s%s%s%s%s%s%f%f%f%d%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot donset dodx dx pfaq01 pfaq02 pfaq03 pfaq04 pfaq05 pfaq06 pfaq07 pfaq08 pfaq09 pfaq10};
                V = {'id','EMSid','emsfn','sex','vi','emsvi','EMSwave','baseage','age','cbaseage','AgeYrs','interval','DOV','DOT','mridone','petdone','trats','trbts','trba','alphas','simtot','cvltca','cvlca1','cvlca2','cvlca3','cvlca4','cvlca5','cvltcb','cvlfrs','cvlcrs','cvlfrl','cvlcrl','cvlrs1','cvlrs2','cvlrs3','cvlrs4','cvlrs5','cvlrst','cvlrx1','cvlrx2','cvlrx3','cvlrx4','cvlrx5','cvlrxt','cvlbet','cvltpe','cvltif','cvltic','cvldis','cvlrbi','cvlsv5','cvllv5','cvlsvs','cvllvs','cvllvl','flucat','flulet','fluctw','flultw','flucti','fluctp','flulti','flultp','flucfw','flucfp','flucfi','flucaw','flucap','flucai','flucvw','flucvp','flucvi','flulfw','flulfp','flulfi','flulaw','flulap','flulai','flulsw','flulsp','flulsi','pmato3','digbac','digfor','crdrot','boscor','bvrtot','donset','dodx','dx','pfaq01','pfaq02','pfaq03','pfaq04','pfaq05','pfaq06','pfaq07','pfaq08','pfaq09','pfaq10'};
                varvec(1) = strmatch('trats',V);
                varvec(2) = strmatch('bvrtot',V);
                varvec(3) = 8; % baseage
                varnames = {'Trails A Time','Trails B Time','Trails B-A Time','Alpha Span','Similarities','CVLT Total Recall','CVLT Recall A1','CVLT Recall A2','CVLT Recall A3','CVLT Recall A4','CVLT Recall A5','CVLT Recall B','CVLT Free Recall Short','CVLT Cued Recall Short','CVLT Free Recall Long','CVLT Cued Recall Long','CVLT Semantic Clustering 1','CVLT Semantic Clustering 2','CVLT Semantic Clustering 3','CVLT Semantic Clustering 4','CVLT Semantic Clustering 5','CVLT Semantic Clustering Total','CVLT Serial Clustering 1','CVLT Serial Clustering 2','CVLT Serial Clustering 3','CVLT Serial Clustering 4','CVLT Serial Clustering 5','CVLT Serial Clustering Total','CVLT Recall Slope','CVLT Total Perseverations','CVLT Free Recall Total Intrusions','CVLT Cued Recall Total Intrusions','CVLT Recognition Discrimination','CVLT Recognition Bias','CVLT Short vs A5 Recall','CVLT Long vs A5 Recall','CVLT Short Free vs Cued','CVLT Free Long vs Short','CVLT Long Free vs Cued','Category Fluency','Letter Fluency','Category Fluency Total Words','Letter Fluency Total Words','Category Fluency Total Intrusions','Category Fluency Total Perseverations','Letter Fluency Total Intrusions','Letter Fluency Total Perseverations','Category Fluency Fruit Words','Category Fluency Fruit Perseverations','Category Fluency Fruit Intrusions','Category Fluency Animals Words','Category Fluency Animals Perseverations','Category Fluency Animals Intrusions','Category Fluency Vegetables Words','Category Fluency Vegetables Perseverations','Category Fluency Vegetables Intrusions','Letter Fluency F Words','Letter Fluency F Perseverations','Letter Fluency F Intrusions','Letter Fluency A Words','Letter Fluency A Perseverations','Letter Fluency A Intrusions','Letter Fluency S Words','Letter Fluency S Perseverations','Letter Fluency S Intrusions','Primary Mental Abilities','Digit Backwards','Digit Forwards','Card Rotations','Boston Naming','Benton Visual Retention'};
                Rvarnames = [];
                Rv = [];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [];
                poptions.i.est.gca.XTick = [];
                poptions.i.est.gca.Position = [.03 .01 .45 .95];
                poptions.i.est.gca.sld = .08;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 500 950];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [-4 1];
                poptions.a.est.gca.XTick = [-4:1:1];
                poptions.a.est.gca.Position = [.03 .01 .45 .95];
                poptions.a.est.gca.sld = .08;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 500 950];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [-.5 .25];
                poptions.ia.est.gca.XTick = [-.5:.25:.25];
                poptions.ia.est.gca.Position = [.03 .01 .45 .95];
                poptions.ia.est.gca.sld = .05;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 500 950];
                poptions.R.gca.Position = [.24,.03,.65,.65];
                poptions.R.gca.vnxy = [-.7 -.2];
                poptions.R.gca.vnfs = 6;
                poptions.R.gca.titleY = 30;
                poptions.R.gcf.Position = [0,300,750,750];
                poptions.R.cbarh.Position = [.905 .2 .01 .4];
                poptions.R.cbarh.Rxy = [73 5];
                
            case 2 % zredndnimain1
                [id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr] = textread(csvfname,'%d%s%s%s%s%s%s%f%f%f%d%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr};
                V = {'id','EMSid','emsfn','sex','vi','emsvi','EMSwave','baseage','age','cbaseage','AgeYrs','interval','DOV','DOT','mridone','petdone','trats','trba','alphas','simtot','cvltca','cvlfrs','cvlfrl','cvlrst','cvldis','cvlsv5','flucat','flulet','fluctp','flultp','pmato3','digfor','digbac','crdrot','boscor','bvrtot','donset','yr'};
                varvec(1) = strmatch('trats',V); % start of cog variables
                varvec(2) = strmatch('bvrtot',V); % end of cog variables
                varvec(3) = 8; % baseage
                varnames = {'Similarities','Digit Forwards','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency Perseverations','Letter Fluency Perseverations','Alpha Span','Digit Backwards','Letter Fluency','Category Fluency','Boston Naming','Trails Making B-A','CVL Long Delay Free Recall','CVL Short Delay Free Recall','CVL Total Recall','Trails Making A','Primary Mental Abilities','Benton Visual Retention','Card Rotations'};
                Rvarnames = {'Trails Making A','Trails Making B-A','Alpha Span','Similarities','CVL Total Recall','CVL Short Delay Free Recall','CVL Long Delay Free Recall','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency','Letter Fluency','Category Fluency Perseverations','Letter Fluency Perseverations','Primary Mental Abilities','Digit Forwards','Digit Backwards','Card Rotations','Boston Naming','Benton Visual Retention'};
                Rv = [4 16 8 9 10 13 14 3 17 12 19 11 2 7 6 5 1 15 20 18];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [];
                poptions.i.est.gca.XTick = [];
                poptions.i.est.gca.Position = [.05 .04 .43 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 450 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [];
                poptions.a.est.gca.XTick = [];
                poptions.a.est.gca.Position = [.05 .04 .43 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 450 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [];
                poptions.ia.est.gca.XTick = [];
                poptions.ia.est.gca.Position = [.05 .04 .43 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 450 500];
                poptions.R.gca.Position = [.35,.03,.55,.55];
                poptions.R.gca.vnxy = [-.2 -.2];
                poptions.R.gca.vnfs = 8;
                poptions.R.gca.titleY = 12.5;
                poptions.R.gcf.Position = [0,300,550,550];
                poptions.R.cbarh.Position = [.915 .1 .01 .4];
                poptions.R.cbarh.Rxy = [21 2];
                           
            case 3 % compzredndnimain1
                [id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset] = textread(csvfname,'%d%s%s%s%f%f%f%f%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset};
                V = {'id','sex','vi','emsvi','baseage','age','cbaseage','AgeYrs','interval','abstr','cap','chunk','disc','inhib','manip','pretr','sretr','swtch','ltm','stm','spd','vocab','vis','donset'};
                varvec(1) = strmatch('abstr',V); % start of cog variables
                varvec(2) = strmatch('vis',V); % end of cog variables
                varvec(3) = 5; % baseage
                varnames = {'Abstraction' 'Capacity' 'Chunking' 'Discrimination' 'Inhibition' 'Manipulation' 'Phonological Retrieval' 'Semantic Retrieval' 'Switching' 'Long-Term Memory' 'Short-Term Memory' 'Speed' 'Vocabulary' 'Visual'};
                Rvarnames = [];
                Rv = [];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [];
                poptions.i.est.gca.XTick = [];
                poptions.i.est.gca.Position = [.05 .04 .57 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 350 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [];
                poptions.a.est.gca.XTick = [];
                poptions.a.est.gca.Position = [.05 .04 .57 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 350 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [];
                poptions.ia.est.gca.XTick = [];
                poptions.ia.est.gca.Position = [.05 .04 .57 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 350 500];                
                poptions.R.gca.Position = [.29,.03,.63,.63];
                poptions.R.gca.vnxy = [-.05 -.05];
                poptions.R.gca.vnfs = 10;
                poptions.R.gca.titleY = 6.8;
                poptions.R.gcf.Position = [0,300,500,500];
                poptions.R.cbarh.Position = [.935 .2 .01 .4];
                poptions.R.cbarh.Rxy = [14.8 1];
                
            case 4 % zredndg1nimain1
                [id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr dg] = textread(csvfname,'%d%s%s%s%s%s%s%f%f%f%d%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr dg};
                V = {'id','EMSid','emsfn','sex','vi','emsvi','EMSwave','baseage','age','cbaseage','AgeYrs','interval','DOV','DOT','mridone','petdone','trats','trba','alphas','simtot','cvltca','cvlfrs','cvlfrl','cvlrst','cvldis','cvlsv5','flucat','flulet','fluctp','flultp','pmato3','digfor','digbac','crdrot','boscor','bvrtot','donset','yr','dg'};
                varvec(1) = strmatch('trats',V); % start of cog variables
                varvec(2) = strmatch('bvrtot',V); % end of cog variables
                varvec(3) = 8; % baseage
                varnames = {'Similarities','Digit Forwards','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency Perseverations','Letter Fluency Perseverations','Alpha Span','Digit Backwards','Letter Fluency','Category Fluency','Boston Naming','Trails Making B-A','CVL Long Delay Free Recall','CVL Short Delay Free Recall','CVL Total Recall','Trails Making A','Primary Mental Abilities','Benton Visual Retention','Card Rotations'};
                Rvarnames = {'Trails Making A','Trails Making B-A','Alpha Span','Similarities','CVL Total Recall','CVL Short Delay Free Recall','CVL Long Delay Free Recall','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency','Letter Fluency','Category Fluency Perseverations','Letter Fluency Perseverations','Primary Mental Abilities','Digit Forwards','Digit Backwards','Card Rotations','Boston Naming','Benton Visual Retention'};
                Rv = [4 16 8 9 10 13 14 3 17 12 19 11 2 7 6 5 1 15 20 18];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [];
                poptions.i.est.gca.XTick = [];
                poptions.i.est.gca.Position = [.05 .04 .43 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 450 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [];
                poptions.a.est.gca.XTick = [];
                poptions.a.est.gca.Position = [.05 .04 .43 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 450 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [];
                poptions.ia.est.gca.XTick = [];
                poptions.ia.est.gca.Position = [.05 .04 .43 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 450 500];
                poptions.R.gca.Position = [.35,.03,.55,.55];
                poptions.R.gca.vnxy = [-.2 -.2];
                poptions.R.gca.vnfs = 8;
                poptions.R.gca.titleY = 12.5;
                poptions.R.gcf.Position = [0,300,550,550];
                poptions.R.cbarh.Position = [.915 .1 .01 .4];
                poptions.R.cbarh.Rxy = [21 2];
                
            case 5 % compzredndg1nimain1
                [id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset dg] = textread(csvfname,'%d%s%s%s%f%f%f%f%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset dg};
                V = {'id','sex','vi','emsvi','baseage','age','cbaseage','AgeYrs','interval','abstr','cap','chunk','disc','inhib','manip','pretr','sretr','swtch','ltm','stm','spd','vocab','vis','donset','dg'};
                varvec(1) = strmatch('abstr',V); % start of cog variables
                varvec(2) = strmatch('vis',V); % end of cog variables
                varvec(3) = 5; % baseage
                varnames = {'Abstraction' 'Capacity' 'Chunking' 'Discrimination' 'Inhibition' 'Manipulation' 'Phonological Retrieval' 'Semantic Retrieval' 'Switching' 'Long-Term Memory' 'Short-Term Memory' 'Speed' 'Vocabulary' 'Visual'};
                Rvarnames = [];
                Rv = [];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [-.2 .2];
                poptions.i.est.gca.XTick = [-.2:.1:.2];
                poptions.i.est.gca.Position = [.05 .04 .57 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 350 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [];
                poptions.a.est.gca.XTick = [];
                poptions.a.est.gca.Position = [.05 .04 .57 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 350 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [];
                poptions.ia.est.gca.XTick = [];
                poptions.ia.est.gca.Position = [.05 .04 .57 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 350 500];
                poptions.R.gca.Position = [.27,.03,.63,.63];
                poptions.R.gca.vnxy = [-.05 -.05];
                poptions.R.gca.vnfs = 10;
                poptions.R.gca.titleY = 6.5;
                poptions.R.gcf.Position = [0,300,500,500];
                poptions.R.cbarh.Position = [.935 .2 .01 .4];
                poptions.R.cbarh.Rxy = [14.7 1];
                
            case 6 % zredndg2nimain1
               [id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr dg] = textread(csvfname,'%d%s%s%s%s%s%s%f%f%f%d%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id EMSid emsfn sex vi emsvi EMSwave baseage age cbaseage AgeYrs interval DOV DOT mridone petdone trats trba alphas simtot cvltca cvlfrs cvlfrl cvlrst cvldis cvlsv5 flucat flulet fluctp flultp pmato3 digfor digbac crdrot boscor bvrtot donset yr dg};
                V = {'id','EMSid','emsfn','sex','vi','emsvi','EMSwave','baseage','age','cbaseage','AgeYrs','interval','DOV','DOT','mridone','petdone','trats','trba','alphas','simtot','cvltca','cvlfrs','cvlfrl','cvlrst','cvldis','cvlsv5','flucat','flulet','fluctp','flultp','pmato3','digfor','digbac','crdrot','boscor','bvrtot','donset','yr','dg'};
                varvec(1) = strmatch('trats',V); % start of cog variables
                varvec(2) = strmatch('bvrtot',V); % end of cog variables
                varvec(3) = 8; % baseage
                varnames = {'Similarities','Digit Forwards','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency Perseverations','Letter Fluency Perseverations','Alpha Span','Digit Backwards','Letter Fluency','Category Fluency','Boston Naming','Trails Making B-A','CVL Long Delay Free Recall','CVL Short Delay Free Recall','CVL Total Recall','Trails Making A','Primary Mental Abilities','Benton Visual Retention','Card Rotations'};
                Rvarnames = {'Trails Making A','Trails Making B-A','Alpha Span','Similarities','CVL Total Recall','CVL Short Delay Free Recall','CVL Long Delay Free Recall','CVL Semantic Clustering','CVL Discrimination','CVL Short Delay vs Recall 5','Category Fluency','Letter Fluency','Category Fluency Perseverations','Letter Fluency Perseverations','Primary Mental Abilities','Digit Forwards','Digit Backwards','Card Rotations','Boston Naming','Benton Visual Retention'};
                Rv = [4 16 8 9 10 13 14 3 17 12 19 11 2 7 6 5 1 15 20 18];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [];
                poptions.i.est.gca.XTick = [];
                poptions.i.est.gca.Position = [.05 .04 .43 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 450 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [-.2 .2];
                poptions.a.est.gca.XTick = [-.2:.1:.2];
                poptions.a.est.gca.Position = [.05 .04 .43 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 450 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [-.03 .03];
                poptions.ia.est.gca.XTick = [-.03:.015:.03];
                poptions.ia.est.gca.Position = [.05 .04 .43 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 450 500];
                poptions.R.gca.Position = [.35,.03,.55,.55];
                poptions.R.gca.vnxy = [-.2 -.2];
                poptions.R.gca.vnfs = 8;
                poptions.R.gca.titleY = 12.5;
                poptions.R.gcf.Position = [0,300,550,550];
                poptions.R.cbarh.Position = [.915 .1 .01 .4];
                poptions.R.cbarh.Rxy = [21 2];
                
            case 7 % compzredndg2nimain1
                [id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset dg] = textread(csvfname,'%d%s%s%s%f%f%f%f%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s','headerlines',1,'delimiter',',','emptyvalue',NaN);
                D = {id sex vi emsvi baseage age cbaseage AgeYrs interval abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis donset dg};
                V = {'id','sex','vi','emsvi','baseage','age','cbaseage','AgeYrs','interval','abstr','cap','chunk','disc','inhib','manip','pretr','sretr','swtch','ltm','stm','spd','vocab','vis','donset','dg'};
                varvec(1) = strmatch('abstr',V); % start of cog variables
                varvec(2) = strmatch('vis',V); % end of cog variables
                varvec(3) = 5; % baseage
                varnames = {'Abstraction' 'Capacity' 'Chunking' 'Discrimination' 'Inhibition' 'Manipulation' 'Phonological Retrieval' 'Semantic Retrieval' 'Switching' 'Long-Term Memory' 'Short-Term Memory' 'Speed' 'Vocabulary' 'Visual'};
                Rvarnames = [];
                Rv = [];
                % Plot options
                poptions.i.est.gca.Ylim = [];
                poptions.i.est.gca.YTick = [];
                poptions.i.est.gca.Xlim = [-.3 .3];
                poptions.i.est.gca.XTick = [-.3:.1:.3];
                poptions.i.est.gca.Position = [.05 .04 .57 .89];
                poptions.i.est.gca.sld = .1;
                poptions.i.est.gca.tld = .05;
                poptions.i.est.gcf.Position = [0 300 350 500];
                poptions.a.est.gca.Ylim = [];
                poptions.a.est.gca.YTick = [];
                poptions.a.est.gca.Xlim = [-.2 .2];
                poptions.a.est.gca.XTick = [-.2:.1:.2];
                poptions.a.est.gca.Position = [.05 .04 .57 .89];
                poptions.a.est.gca.sld = .1;
                poptions.a.est.gca.tld = .05;
                poptions.a.est.gcf.Position = [0 300 350 500];
                poptions.ia.est.gca.Ylim = [];
                poptions.ia.est.gca.YTick = [];
                poptions.ia.est.gca.Xlim = [];
                poptions.ia.est.gca.XTick = [];
                poptions.ia.est.gca.Position = [.05 .04 .57 .89];
                poptions.ia.est.gca.sld = .1;
                poptions.ia.est.gca.tld = .05;
                poptions.ia.est.gcf.Position = [0 300 350 500];               
                poptions.R.gca.Position = [.27,.03,.63,.63];
                poptions.R.gca.vnxy = [-.05 -.05];
                poptions.R.gca.vnfs = 10;
                poptions.R.gca.titleY = 6.5;
                poptions.R.gcf.Position = [0,300,500,500];
                poptions.R.cbarh.Position = [.935 .2 .01 .4];
                poptions.R.cbarh.Rxy = [14.7 1];
                
        end
        
        % Compile to O
        O.id = id;
        O.D  = D;
        O.V  = V;
        O.varvec = varvec;
        O.varnames = varnames;
        O.poptions = poptions;
        
    case 'icc'
        
        % Extract (Note for icc there are only 3 cases)
        switch db
            case 1 % nimain1
                [effect trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1,'emptyvalue',NaN);
                D = {trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot};
                
            case 2 % zredndnimain1
                [effect simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1,'emptyvalue',NaN);
                D = {simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot};
                
            case 3 % compzredndnimain1
                [effect abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1,'emptyvalue',NaN);
                D = {abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis};
                
        end
        
        % Compile to O
        O.D = D;
        
    case 'fest'
        
        % Extract
        switch db
            case 1 % nimain1
                [effect Esttrats setrats DFtrats ttrats pttrats Esttrbts setrbts DFtrbts ttrbts pttrbts Esttrba setrba DFtrba ttrba pttrba Estalphas sealphas DFalphas talphas ptalphas Estsimtot sesimtot DFsimtot tsimtot ptsimtot Estcvltca secvltca DFcvltca tcvltca ptcvltca Estcvlca1 secvlca1 DFcvlca1 tcvlca1 ptcvlca1 Estcvlca2 secvlca2 DFcvlca2 tcvlca2 ptcvlca2 Estcvlca3 secvlca3 DFcvlca3 tcvlca3 ptcvlca3 Estcvlca4 secvlca4 DFcvlca4 tcvlca4 ptcvlca4 Estcvlca5 secvlca5 DFcvlca5 tcvlca5 ptcvlca5 Estcvltcb secvltcb DFcvltcb tcvltcb ptcvltcb Estcvlfrs secvlfrs DFcvlfrs tcvlfrs ptcvlfrs Estcvlcrs secvlcrs DFcvlcrs tcvlcrs ptcvlcrs Estcvlfrl secvlfrl DFcvlfrl tcvlfrl ptcvlfrl Estcvlcrl secvlcrl DFcvlcrl tcvlcrl ptcvlcrl Estcvlrs1 secvlrs1 DFcvlrs1 tcvlrs1 ptcvlrs1 Estcvlrs2 secvlrs2 DFcvlrs2 tcvlrs2 ptcvlrs2 Estcvlrs3 secvlrs3 DFcvlrs3 tcvlrs3 ptcvlrs3 Estcvlrs4 secvlrs4 DFcvlrs4 tcvlrs4 ptcvlrs4 Estcvlrs5 secvlrs5 DFcvlrs5 tcvlrs5 ptcvlrs5 Estcvlrst secvlrst DFcvlrst tcvlrst ptcvlrst Estcvlrx1 secvlrx1 DFcvlrx1 tcvlrx1 ptcvlrx1 Estcvlrx2 secvlrx2 DFcvlrx2 tcvlrx2 ptcvlrx2 Estcvlrx3 secvlrx3 DFcvlrx3 tcvlrx3 ptcvlrx3 Estcvlrx4 secvlrx4 DFcvlrx4 tcvlrx4 ptcvlrx4 Estcvlrx5 secvlrx5 DFcvlrx5 tcvlrx5 ptcvlrx5 Estcvlrxt secvlrxt DFcvlrxt tcvlrxt ptcvlrxt Estcvlbet secvlbet DFcvlbet tcvlbet ptcvlbet Estcvltpe secvltpe DFcvltpe tcvltpe ptcvltpe Estcvltif secvltif DFcvltif tcvltif ptcvltif Estcvltic secvltic DFcvltic tcvltic ptcvltic Estcvldis secvldis DFcvldis tcvldis ptcvldis Estcvlrbi secvlrbi DFcvlrbi tcvlrbi ptcvlrbi Estcvlsv5 secvlsv5 DFcvlsv5 tcvlsv5 ptcvlsv5 Estcvllv5 secvllv5 DFcvllv5 tcvllv5 ptcvllv5 Estcvlsvs secvlsvs DFcvlsvs tcvlsvs ptcvlsvs Estcvllvs secvllvs DFcvllvs tcvllvs ptcvllvs Estcvllvl secvllvl DFcvllvl tcvllvl ptcvllvl Estflucat seflucat DFflucat tflucat ptflucat Estflulet seflulet DFflulet tflulet ptflulet Estfluctw sefluctw DFfluctw tfluctw ptfluctw Estflultw seflultw DFflultw tflultw ptflultw Estflucti seflucti DFflucti tflucti ptflucti Estfluctp sefluctp DFfluctp tfluctp ptfluctp Estflulti seflulti DFflulti tflulti ptflulti Estflultp seflultp DFflultp tflultp ptflultp Estflucfw seflucfw DFflucfw tflucfw ptflucfw Estflucfp seflucfp DFflucfp tflucfp ptflucfp Estflucfi seflucfi DFflucfi tflucfi ptflucfi Estflucaw seflucaw DFflucaw tflucaw ptflucaw Estflucap seflucap DFflucap tflucap ptflucap Estflucai seflucai DFflucai tflucai ptflucai Estflucvw seflucvw DFflucvw tflucvw ptflucvw Estflucvp seflucvp DFflucvp tflucvp ptflucvp Estflucvi seflucvi DFflucvi tflucvi ptflucvi Estflulfw seflulfw DFflulfw tflulfw ptflulfw Estflulfp seflulfp DFflulfp tflulfp ptflulfp Estflulfi seflulfi DFflulfi tflulfi ptflulfi Estflulaw seflulaw DFflulaw tflulaw ptflulaw Estflulap seflulap DFflulap tflulap ptflulap Estflulai seflulai DFflulai tflulai ptflulai Estflulsw seflulsw DFflulsw tflulsw ptflulsw Estflulsp seflulsp DFflulsp tflulsp ptflulsp Estflulsi seflulsi DFflulsi tflulsi ptflulsi Estpmato3 sepmato3 DFpmato3 tpmato3 ptpmato3 Estdigbac sedigbac DFdigbac tdigbac ptdigbac Estdigfor sedigfor DFdigfor tdigfor ptdigfor Estcrdrot secrdrot DFcrdrot tcrdrot ptcrdrot Estboscor seboscor DFboscor tboscor ptboscor Estbvrtot sebvrtot DFbvrtot tbvrtot ptbvrtot] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','headerlines',1,'delimiter',',');
                idata = [Esttrats setrats DFtrats ttrats pttrats Esttrbts setrbts DFtrbts ttrbts pttrbts Esttrba setrba DFtrba ttrba pttrba Estalphas sealphas DFalphas talphas ptalphas Estsimtot sesimtot DFsimtot tsimtot ptsimtot Estcvltca secvltca DFcvltca tcvltca ptcvltca Estcvlca1 secvlca1 DFcvlca1 tcvlca1 ptcvlca1 Estcvlca2 secvlca2 DFcvlca2 tcvlca2 ptcvlca2 Estcvlca3 secvlca3 DFcvlca3 tcvlca3 ptcvlca3 Estcvlca4 secvlca4 DFcvlca4 tcvlca4 ptcvlca4 Estcvlca5 secvlca5 DFcvlca5 tcvlca5 ptcvlca5 Estcvltcb secvltcb DFcvltcb tcvltcb ptcvltcb Estcvlfrs secvlfrs DFcvlfrs tcvlfrs ptcvlfrs Estcvlcrs secvlcrs DFcvlcrs tcvlcrs ptcvlcrs Estcvlfrl secvlfrl DFcvlfrl tcvlfrl ptcvlfrl Estcvlcrl secvlcrl DFcvlcrl tcvlcrl ptcvlcrl Estcvlrs1 secvlrs1 DFcvlrs1 tcvlrs1 ptcvlrs1 Estcvlrs2 secvlrs2 DFcvlrs2 tcvlrs2 ptcvlrs2 Estcvlrs3 secvlrs3 DFcvlrs3 tcvlrs3 ptcvlrs3 Estcvlrs4 secvlrs4 DFcvlrs4 tcvlrs4 ptcvlrs4 Estcvlrs5 secvlrs5 DFcvlrs5 tcvlrs5 ptcvlrs5 Estcvlrst secvlrst DFcvlrst tcvlrst ptcvlrst Estcvlrx1 secvlrx1 DFcvlrx1 tcvlrx1 ptcvlrx1 Estcvlrx2 secvlrx2 DFcvlrx2 tcvlrx2 ptcvlrx2 Estcvlrx3 secvlrx3 DFcvlrx3 tcvlrx3 ptcvlrx3 Estcvlrx4 secvlrx4 DFcvlrx4 tcvlrx4 ptcvlrx4 Estcvlrx5 secvlrx5 DFcvlrx5 tcvlrx5 ptcvlrx5 Estcvlrxt secvlrxt DFcvlrxt tcvlrxt ptcvlrxt Estcvlbet secvlbet DFcvlbet tcvlbet ptcvlbet Estcvltpe secvltpe DFcvltpe tcvltpe ptcvltpe Estcvltif secvltif DFcvltif tcvltif ptcvltif Estcvltic secvltic DFcvltic tcvltic ptcvltic Estcvldis secvldis DFcvldis tcvldis ptcvldis Estcvlrbi secvlrbi DFcvlrbi tcvlrbi ptcvlrbi Estcvlsv5 secvlsv5 DFcvlsv5 tcvlsv5 ptcvlsv5 Estcvllv5 secvllv5 DFcvllv5 tcvllv5 ptcvllv5 Estcvlsvs secvlsvs DFcvlsvs tcvlsvs ptcvlsvs Estcvllvs secvllvs DFcvllvs tcvllvs ptcvllvs Estcvllvl secvllvl DFcvllvl tcvllvl ptcvllvl Estflucat seflucat DFflucat tflucat ptflucat Estflulet seflulet DFflulet tflulet ptflulet Estfluctw sefluctw DFfluctw tfluctw ptfluctw Estflultw seflultw DFflultw tflultw ptflultw Estflucti seflucti DFflucti tflucti ptflucti Estfluctp sefluctp DFfluctp tfluctp ptfluctp Estflulti seflulti DFflulti tflulti ptflulti Estflultp seflultp DFflultp tflultp ptflultp Estflucfw seflucfw DFflucfw tflucfw ptflucfw Estflucfp seflucfp DFflucfp tflucfp ptflucfp Estflucfi seflucfi DFflucfi tflucfi ptflucfi Estflucaw seflucaw DFflucaw tflucaw ptflucaw Estflucap seflucap DFflucap tflucap ptflucap Estflucai seflucai DFflucai tflucai ptflucai Estflucvw seflucvw DFflucvw tflucvw ptflucvw Estflucvp seflucvp DFflucvp tflucvp ptflucvp Estflucvi seflucvi DFflucvi tflucvi ptflucvi Estflulfw seflulfw DFflulfw tflulfw ptflulfw Estflulfp seflulfp DFflulfp tflulfp ptflulfp Estflulfi seflulfi DFflulfi tflulfi ptflulfi Estflulaw seflulaw DFflulaw tflulaw ptflulaw Estflulap seflulap DFflulap tflulap ptflulap Estflulai seflulai DFflulai tflulai ptflulai Estflulsw seflulsw DFflulsw tflulsw ptflulsw Estflulsp seflulsp DFflulsp tflulsp ptflulsp Estflulsi seflulsi DFflulsi tflulsi ptflulsi Estpmato3 sepmato3 DFpmato3 tpmato3 ptpmato3 Estdigbac sedigbac DFdigbac tdigbac ptdigbac Estdigfor sedigfor DFdigfor tdigfor ptdigfor Estcrdrot secrdrot DFcrdrot tcrdrot ptcrdrot Estboscor seboscor DFboscor tboscor ptboscor Estbvrtot sebvrtot DFbvrtot tbvrtot ptbvrtot];
                stats = {'Effect','Esttrats','setrats','DFtrats','ttrats','pttrats','Esttrbts','setrbts','DFtrbts','ttrbts','pttrbts','Esttrba','setrba','DFtrba','ttrba','pttrba','Estalphas','sealphas','DFalphas','talphas','ptalphas','Estsimtot','sesimtot','DFsimtot','tsimtot','ptsimtot','Estcvltca','secvltca','DFcvltca','tcvltca','ptcvltca','Estcvlca1','secvlca1','DFcvlca1','tcvlca1','ptcvlca1','Estcvlca2','secvlca2','DFcvlca2','tcvlca2','ptcvlca2','Estcvlca3','secvlca3','DFcvlca3','tcvlca3','ptcvlca3','Estcvlca4','secvlca4','DFcvlca4','tcvlca4','ptcvlca4','Estcvlca5','secvlca5','DFcvlca5','tcvlca5','ptcvlca5','Estcvltcb','secvltcb','DFcvltcb','tcvltcb','ptcvltcb','Estcvlfrs','secvlfrs','DFcvlfrs','tcvlfrs','ptcvlfrs','Estcvlcrs','secvlcrs','DFcvlcrs','tcvlcrs','ptcvlcrs','Estcvlfrl','secvlfrl','DFcvlfrl','tcvlfrl','ptcvlfrl','Estcvlcrl','secvlcrl','DFcvlcrl','tcvlcrl','ptcvlcrl','Estcvlrs1','secvlrs1','DFcvlrs1','tcvlrs1','ptcvlrs1','Estcvlrs2','secvlrs2','DFcvlrs2','tcvlrs2','ptcvlrs2','Estcvlrs3','secvlrs3','DFcvlrs3','tcvlrs3','ptcvlrs3','Estcvlrs4','secvlrs4','DFcvlrs4','tcvlrs4','ptcvlrs4','Estcvlrs5','secvlrs5','DFcvlrs5','tcvlrs5','ptcvlrs5','Estcvlrst','secvlrst','DFcvlrst','tcvlrst','ptcvlrst','Estcvlrx1','secvlrx1','DFcvlrx1','tcvlrx1','ptcvlrx1','Estcvlrx2','secvlrx2','DFcvlrx2','tcvlrx2','ptcvlrx2','Estcvlrx3','secvlrx3','DFcvlrx3','tcvlrx3','ptcvlrx3','Estcvlrx4','secvlrx4','DFcvlrx4','tcvlrx4','ptcvlrx4','Estcvlrx5','secvlrx5','DFcvlrx5','tcvlrx5','ptcvlrx5','Estcvlrxt','secvlrxt','DFcvlrxt','tcvlrxt','ptcvlrxt','Estcvlbet','secvlbet','DFcvlbet','tcvlbet','ptcvlbet','Estcvltpe','secvltpe','DFcvltpe','tcvltpe','ptcvltpe','Estcvltif','secvltif','DFcvltif','tcvltif','ptcvltif','Estcvltic','secvltic','DFcvltic','tcvltic','ptcvltic','Estcvldis','secvldis','DFcvldis','tcvldis','ptcvldis','Estcvlrbi','secvlrbi','DFcvlrbi','tcvlrbi','ptcvlrbi','Estcvlsv5','secvlsv5','DFcvlsv5','tcvlsv5','ptcvlsv5','Estcvllv5','secvllv5','DFcvllv5','tcvllv5','ptcvllv5','Estcvlsvs','secvlsvs','DFcvlsvs','tcvlsvs','ptcvlsvs','Estcvllvs','secvllvs','DFcvllvs','tcvllvs','ptcvllvs','Estcvllvl','secvllvl','DFcvllvl','tcvllvl','ptcvllvl','Estflucat','seflucat','DFflucat','tflucat','ptflucat','Estflulet','seflulet','DFflulet','tflulet','ptflulet','Estfluctw','sefluctw','DFfluctw','tfluctw','ptfluctw','Estflultw','seflultw','DFflultw','tflultw','ptflultw','Estflucti','seflucti','DFflucti','tflucti','ptflucti','Estfluctp','sefluctp','DFfluctp','tfluctp','ptfluctp','Estflulti','seflulti','DFflulti','tflulti','ptflulti','Estflultp','seflultp','DFflultp','tflultp','ptflultp','Estflucfw','seflucfw','DFflucfw','tflucfw','ptflucfw','Estflucfp','seflucfp','DFflucfp','tflucfp','ptflucfp','Estflucfi','seflucfi','DFflucfi','tflucfi','ptflucfi','Estflucaw','seflucaw','DFflucaw','tflucaw','ptflucaw','Estflucap','seflucap','DFflucap','tflucap','ptflucap','Estflucai','seflucai','DFflucai','tflucai','ptflucai','Estflucvw','seflucvw','DFflucvw','tflucvw','ptflucvw','Estflucvp','seflucvp','DFflucvp','tflucvp','ptflucvp','Estflucvi','seflucvi','DFflucvi','tflucvi','ptflucvi','Estflulfw','seflulfw','DFflulfw','tflulfw','ptflulfw','Estflulfp','seflulfp','DFflulfp','tflulfp','ptflulfp','Estflulfi','seflulfi','DFflulfi','tflulfi','ptflulfi','Estflulaw','seflulaw','DFflulaw','tflulaw','ptflulaw','Estflulap','seflulap','DFflulap','tflulap','ptflulap','Estflulai','seflulai','DFflulai','tflulai','ptflulai','Estflulsw','seflulsw','DFflulsw','tflulsw','ptflulsw','Estflulsp','seflulsp','DFflulsp','tflulsp','ptflulsp','Estflulsi','seflulsi','DFflulsi','tflulsi','ptflulsi','Estpmato3','sepmato3','DFpmato3','tpmato3','ptpmato3','Estdigbac','sedigbac','DFdigbac','tdigbac','ptdigbac','Estdigfor','sedigfor','DFdigfor','tdigfor','ptdigfor','Estcrdrot','secrdrot','DFcrdrot','tcrdrot','ptcrdrot','Estboscor','seboscor','DFboscor','tboscor','ptboscor','Estbvrtot','sebvrtot','DFbvrtot','tbvrtot','ptbvrtot'};
                data = [Esttrats Esttrbts Esttrba Estalphas Estsimtot Estcvltca Estcvlca1 Estcvlca2 Estcvlca3 Estcvlca4 Estcvlca5 Estcvltcb Estcvlfrs Estcvlcrs Estcvlfrl Estcvlcrl Estcvlrs1 Estcvlrs2 Estcvlrs3 Estcvlrs4 Estcvlrs5 Estcvlrst Estcvlrx1 Estcvlrx2 Estcvlrx3 Estcvlrx4 Estcvlrx5 Estcvlrxt Estcvlbet Estcvltpe Estcvltif Estcvltic Estcvldis Estcvlrbi Estcvlsv5 Estcvllv5 Estcvlsvs Estcvllvs Estcvllvl Estflucat Estflulet Estfluctw Estflultw Estflucti Estfluctp Estflulti Estflultp Estflucfw Estflucfp Estflucfi Estflucaw Estflucap Estflucai Estflucvw Estflucvp Estflucvi Estflulfw Estflulfp Estflulfi Estflulaw Estflulap Estflulai Estflulsw Estflulsp Estflulsi Estpmato3 Estdigbac Estdigfor Estcrdrot Estboscor Estbvrtot];
                V = {'trats' 'trbts' 'trba' 'alphas' 'simtot' 'cvltca' 'cvlca1' 'cvlca2' 'cvlca3' 'cvlca4' 'cvlca5' 'cvltcb' 'cvlfrs' 'cvlcrs' 'cvlfrl' 'cvlcrl' 'cvlrs1' 'cvlrs2' 'cvlrs3' 'cvlrs4' 'cvlrs5' 'cvlrst' 'cvlrx1' 'cvlrx2' 'cvlrx3' 'cvlrx4' 'cvlrx5' 'cvlrxt' 'cvlbet' 'cvltpe' 'cvltif' 'cvltic' 'cvldis' 'cvlrbi' 'cvlsv5' 'cvllv5' 'cvlsvs' 'cvllvs' 'cvllvl' 'flucat' 'flulet' 'fluctw' 'flultw' 'flucti' 'fluctp' 'flulti' 'flultp' 'flucfw' 'flucfp' 'flucfi' 'flucaw' 'flucap' 'flucai' 'flucvw' 'flucvp' 'flucvi' 'flulfw' 'flulfp' 'flulfi' 'flulaw' 'flulap' 'flulai' 'flulsw' 'flulsp' 'flulsi' 'pmato3' 'digbac' 'digfor' 'crdrot' 'boscor' 'bvrtot'};
            
            case {2,4,6} % zredndnimain1 or zredndg1nimain1 or zredndg2nimain1
                [effect Estsimtot sesimtot DFsimtot tsimtot ptsimtot Estdigfor sedigfor DFdigfor tdigfor ptdigfor Estcvlrst secvlrst DFcvlrst tcvlrst ptcvlrst Estcvldis secvldis DFcvldis tcvldis ptcvldis Estcvlsv5 secvlsv5 DFcvlsv5 tcvlsv5 ptcvlsv5 Estfluctp sefluctp DFfluctp tfluctp ptfluctp Estflultp seflultp DFflultp tflultp ptflultp Estalphas sealphas DFalphas talphas ptalphas Estdigbac sedigbac DFdigbac tdigbac ptdigbac Estflulet seflulet DFflulet tflulet ptflulet Estflucat seflucat DFflucat tflucat ptflucat Estboscor seboscor DFboscor tboscor ptboscor Esttrba setrba DFtrba ttrba pttrba Estcvlfrl secvlfrl DFcvlfrl tcvlfrl ptcvlfrl Estcvlfrs secvlfrs DFcvlfrs tcvlfrs ptcvlfrs Estcvltca secvltca DFcvltca tcvltca ptcvltca Esttrats setrats DFtrats ttrats pttrats Estpmato3 sepmato3 DFpmato3 tpmato3 ptpmato3 Estbvrtot sebvrtot DFbvrtot tbvrtot ptbvrtot Estcrdrot secrdrot DFcrdrot tcrdrot ptcrdrot] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','headerlines',1,'delimiter',',');
                idata = [Estsimtot sesimtot DFsimtot tsimtot ptsimtot Estdigfor sedigfor DFdigfor tdigfor ptdigfor Estcvlrst secvlrst DFcvlrst tcvlrst ptcvlrst Estcvldis secvldis DFcvldis tcvldis ptcvldis Estcvlsv5 secvlsv5 DFcvlsv5 tcvlsv5 ptcvlsv5 Estfluctp sefluctp DFfluctp tfluctp ptfluctp Estflultp seflultp DFflultp tflultp ptflultp Estalphas sealphas DFalphas talphas ptalphas Estdigbac sedigbac DFdigbac tdigbac ptdigbac Estflulet seflulet DFflulet tflulet ptflulet Estflucat seflucat DFflucat tflucat ptflucat Estboscor seboscor DFboscor tboscor ptboscor Esttrba setrba DFtrba ttrba pttrba Estcvlfrl secvlfrl DFcvlfrl tcvlfrl ptcvlfrl Estcvlfrs secvlfrs DFcvlfrs tcvlfrs ptcvlfrs Estcvltca secvltca DFcvltca tcvltca ptcvltca Esttrats setrats DFtrats ttrats pttrats Estpmato3 sepmato3 DFpmato3 tpmato3 ptpmato3 Estbvrtot sebvrtot DFbvrtot tbvrtot ptbvrtot Estcrdrot secrdrot DFcrdrot tcrdrot ptcrdrot];
                stats = {'Effect','Estsimtot','sesimtot','DFsimtot','tsimtot','ptsimtot','Estdigfor','sedigfor','DFdigfor','tdigfor','ptdigfor','Estcvlrst','secvlrst','DFcvlrst','tcvlrst','ptcvlrst','Estcvldis','secvldis','DFcvldis','tcvldis','ptcvldis','Estcvlsv5','secvlsv5','DFcvlsv5','tcvlsv5','ptcvlsv5','Estfluctp','sefluctp','DFfluctp','tfluctp','ptfluctp','Estflultp','seflultp','DFflultp','tflultp','ptflultp','Estalphas','sealphas','DFalphas','talphas','ptalphas','Estdigbac','sedigbac','DFdigbac','tdigbac','ptdigbac','Estflulet','seflulet','DFflulet','tflulet','ptflulet','Estflucat','seflucat','DFflucat','tflucat','ptflucat','Estboscor','seboscor','DFboscor','tboscor','ptboscor','Esttrba','setrba','DFtrba','ttrba','pttrba','Estcvlfrl','secvlfrl','DFcvlfrl','tcvlfrl','ptcvlfrl','Estcvlfrs','secvlfrs','DFcvlfrs','tcvlfrs','ptcvlfrs','Estcvltca','secvltca','DFcvltca','tcvltca','ptcvltca','Esttrats','setrats','DFtrats','ttrats','pttrats','Estpmato3','sepmato3','DFpmato3','tpmato3','ptpmato3','Estbvrtot','sebvrtot','DFbvrtot','tbvrtot','ptbvrtot','Estcrdrot','secrdrot','DFcrdrot','tcrdrot','ptcrdrot'};
                data = [Estsimtot Estdigfor Estcvlrst Estcvldis Estcvlsv5 Estfluctp Estflultp Estalphas Estdigbac Estflulet Estflucat Estboscor Esttrba Estcvlfrl Estcvlfrs Estcvltca Esttrats Estpmato3 Estbvrtot Estcrdrot];
                V = {'simtot' 'digfor' 'cvlrst' 'cvldis' 'cvlsv5' 'fluctp' 'flultp' 'alphas' 'digbac' 'flulet' 'flucat' 'boscor' 'trba' 'cvlfrl' 'cvlfrs' 'cvltca' 'trats' 'pmato3' 'bvrtot' 'crdrot'};
                
            case {3,5,7} % compzredndnimain1 or compzredndg1nimain1 or compzredndg2nimain1
                [effect Estabstr seabstr DFabstr tabstr ptabstr Estcap secap DFcap tcap ptcap Estchunk sechunk DFchunk tchunk ptchunk Estdisc sedisc DFdisc tdisc ptdisc Estinhib seinhib DFinhib tinhib ptinhib Estmanip semanip DFmanip tmanip ptmanip Estpretr sepretr DFpretr tpretr ptpretr Estsretr sesretr DFsretr tsretr ptsretr Estswtch seswtch DFswtch tswtch ptswtch Estltm seltm DFltm tltm ptltm Eststm sestm DFstm tstm ptstm Estspd sespd DFspd tspd ptspd Estvocab sevocab DFvocab tvocab ptvocab Estvis sevis DFvis tvis ptvis] = textread(csvfname,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','headerlines',1,'delimiter',',');
                idata = [Estabstr seabstr DFabstr tabstr ptabstr Estcap secap DFcap tcap ptcap Estchunk sechunk DFchunk tchunk ptchunk Estdisc sedisc DFdisc tdisc ptdisc Estinhib seinhib DFinhib tinhib ptinhib Estmanip semanip DFmanip tmanip ptmanip Estpretr sepretr DFpretr tpretr ptpretr Estsretr sesretr DFsretr tsretr ptsretr Estswtch seswtch DFswtch tswtch ptswtch Estltm seltm DFltm tltm ptltm Eststm sestm DFstm tstm ptstm Estspd sespd DFspd tspd ptspd Estvocab sevocab DFvocab tvocab ptvocab Estvis sevis DFvis tvis ptvis];
                stats = {'Effect','Estabstr','seabstr','DFabstr','tabstr','ptabstr','Estcap','secap','DFcap','tcap','ptcap','Estchunk','sechunk','DFchunk','tchunk','ptchunk','Estdisc','sedisc','DFdisc','tdisc','ptdisc','Estinhib','seinhib','DFinhib','tinhib','ptinhib','Estmanip','semanip','DFmanip','tmanip','ptmanip','Estpretr','sepretr','DFpretr','tpretr','ptpretr','Estsretr','sesretr','DFsretr','tsretr','ptsretr','Estswtch','seswtch','DFswtch','tswtch','ptswtch','Estltm','seltm','DFltm','tltm','ptltm','Eststm','sestm','DFstm','tstm','ptstm','Estspd','sespd','DFspd','tspd','ptspd','Estvocab','sevocab','DFvocab','tvocab','ptvocab','Estvis','sevis','DFvis','tvis','ptvis'};
                data = [Estabstr Estcap Estchunk Estdisc Estinhib Estmanip Estpretr Estsretr Estswtch Estltm Eststm Estspd Estvocab Estvis];
                V = {'abstr' 'cap' 'chunk' 'disc' 'inhib' 'manip' 'pretr' 'sretr' 'swtch' 'ltm' 'stm' 'spd' 'vocab' 'vis'};
                                               
        end
        
        % Compile to O
        O.effect = effect;
        O.data   = data;
        O.idata  = idata;
        O.V      = V;
        O.stats  = stats;
        
    case 'rest'
        
        % Extract
        switch db
            case 1 % nimain1
                [id baseage effect trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot] = textread(csvfname,'%d%f%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1);
                data = [id baseage trats trbts trba alphas simtot cvltca cvlca1 cvlca2 cvlca3 cvlca4 cvlca5 cvltcb cvlfrs cvlcrs cvlfrl cvlcrl cvlrs1 cvlrs2 cvlrs3 cvlrs4 cvlrs5 cvlrst cvlrx1 cvlrx2 cvlrx3 cvlrx4 cvlrx5 cvlrxt cvlbet cvltpe cvltif cvltic cvldis cvlrbi cvlsv5 cvllv5 cvlsvs cvllvs cvllvl flucat flulet fluctw flultw flucti fluctp flulti flultp flucfw flucfp flucfi flucaw flucap flucai flucvw flucvp flucvi flulfw flulfp flulfi flulaw flulap flulai flulsw flulsp flulsi pmato3 digbac digfor crdrot boscor bvrtot];
                V = {'id' 'baseage' 'trats' 'trbts' 'trba' 'alphas' 'simtot' 'cvltca' 'cvlca1' 'cvlca2' 'cvlca3' 'cvlca4' 'cvlca5' 'cvltcb' 'cvlfrs' 'cvlcrs' 'cvlfrl' 'cvlcrl' 'cvlrs1' 'cvlrs2' 'cvlrs3' 'cvlrs4' 'cvlrs5' 'cvlrst' 'cvlrx1' 'cvlrx2' 'cvlrx3' 'cvlrx4' 'cvlrx5' 'cvlrxt' 'cvlbet' 'cvltpe' 'cvltif' 'cvltic' 'cvldis' 'cvlrbi' 'cvlsv5' 'cvllv5' 'cvlsvs' 'cvllvs' 'cvllvl' 'flucat' 'flulet' 'fluctw' 'flultw' 'flucti' 'fluctp' 'flulti' 'flultp' 'flucfw' 'flucfp' 'flucfi' 'flucaw' 'flucap' 'flucai' 'flucvw' 'flucvp' 'flucvi' 'flulfw' 'flulfp' 'flulfi' 'flulaw' 'flulap' 'flulai' 'flulsw' 'flulsp' 'flulsi' 'pmato3' 'digbac' 'digfor' 'crdrot' 'boscor' 'bvrtot'};
                
            case {2,4,6} % zredndnimain1 or zredndg1nimain1 or zredndg2nimain1
                [id baseage effect simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot] = textread(csvfname,'%d%f%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1,'emptyvalue',NaN);
                data = [id baseage simtot digfor cvlrst cvldis cvlsv5 fluctp flultp alphas digbac flulet flucat boscor trba cvlfrl cvlfrs cvltca trats pmato3 bvrtot crdrot];
                V = {'id' 'baseage' 'simtot' 'digfor' 'cvlrst' 'cvldis' 'cvlsv5' 'fluctp' 'flultp' 'alphas' 'digbac' 'flulet' 'flucat' 'boscor' 'trba' 'cvlfrl' 'cvlfrs' 'cvltca' 'trats' 'pmato3' 'bvrtot' 'crdrot'};
                
            case {3,5,7} % compzredndnimain1 or compzredndg1nimain1 or compzredndg2nimain1
                [id baseage effect abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis] = textread(csvfname,'%d%f%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',',','headerlines',1,'emptyvalue',NaN);
                data = [id baseage abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis];
                 V = {'id' 'baseage' 'abstr' 'cap' 'chunk' 'disc' 'inhib' 'manip' 'pretr' 'sretr' 'swtch' 'ltm' 'stm' 'spd' 'vocab' 'vis'};
                
        end
        
        % Compile to O
        O.effect = effect;
        O.data   = data;
        O.V      = V; 
        
end