addpath /Users/gohjo/Research/Projects/BLSA/Battery/Scripts/
cd('/Users/gohjo/Research/Projects/BLSA/Battery/Analysis');
load zred3ndrEST_DATA
load zred3ndrRAW_DATA
m1B1b1 = [zeros(size(m1rb1,1),2) repmat(m1fB1,size(m1rb1,1),1)] + m1rb1;

% Setup data matrices
DC = B(:,3:end);
DL = m1B1b1(:,3:end);
RC = corr(DC);
RL = corr(DL);
covC = cov(DC);
covL = cov(DL);
sdC = sqrt(covC);
sdL = sqrt(covL);
nM = eye(length(RC));
RCrI = RC - nM;
RLrI = RL - nM;
tlRCrI = tril(RCrI) + (RCrI.*nM);
nztlRCrI = tlRCrI(find(tlRCrI~=0));
tlRLrI = tril(RLrI) + (RLrI.*nM);
nztlRLrI = tlRLrI(find(tlRLrI~=0));


% Cross-sectional fit
RMRC = blsa_rmr(sdC,sdC.*nM,'raw'),
mnRCrI = mean(nztlRCrI),
mdRCrI = median(nztlRCrI),
[csqcovC1,dfcovC1] = blsa_chisquare(covC.*nM,covC,149,size(covC,1)),
[csqcovC2,dfcovC2] = blsa_chisquare(covC,covC.*nM,149,size(covC,1)),

% Longitudinal fit
RMRL = blsa_rmr(sdL,sdL.*nM,'raw'),
mnRLrI = mean(nztlRLrI),
mdRLrI = median(nztlRLrI),
[csqcovL1,dfcovL1] = blsa_chisquare(covL.*nM,covL,149,size(covL,1)),
[csqcovL2,dfcovL2] = blsa_chisquare(covL,covL.*nM,149,size(covL,1)),

clear all