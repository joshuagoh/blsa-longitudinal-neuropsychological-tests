function [A Ar T H R F] = blsa_fa(X,m,ftype,rtype)

% Perform factor analysis on X with m approximating factors.
%
% Usage [A Ar T H R F] = blsa_fa(X,m,ftype,rtype)
%
% X     - Data.
% m     - No. of factors to estimate.
% ftype - Method of factor analysis to do:
%         'PFA' - Principal Factor Analysis
%         'MINRES' - Minimal Residuals
%         'ML' - Maximum Likelihood
% rtype - Rotation method:
%         'none','promax','varimax','quartimax','equamax','parsimax','orthomax'
% A     - Unrotated factor loadings (MINRES).
% Ar    - Rotated factor loadings.
% T     - Rotation matrix.
% H     - Communalities.
% R     - Approximated correlation matrix.
% F     - Rotated factor scores.
%
% The method used here is minres (Hayman, 1966, Psychometrika). This deals
% with Heywood cases.
%
% Created by Josh Goh 8 Dec 2010.


% Allocations
addpath /Users/gohjo/Research/Scripts/Matlab/stats/ % For minresfac.m
Z = zscore(X);

% Compute factor loadings
switch ftype
    
    % Principal Factors
    case 'PFA'        
        [U S V] = svd(corr(Z));
        A = V(:,1:m)*S(1:m,1:m)^.5;
        
    % Minimal Residuals    
    case 'MINRES'
        A = minresfac(corr(Z),m);
        
    % Maximum Likelihood    
    case 'ML'
        A = factoran(Z,m,'rotate','none');

end

H = sum(A.^2,2);        % Communalities
R = A*A' + diag(1-H);   % Approximated correlation matrix

% Rotate factors
if strcmp('none',rtype)
    Ar = A;
    T = eye(m);
else
    [Ar T] = rotatefactors(A,'Method',rtype);
end
    
Phi = inv(T)*inv(T)';   % Correlation matrix between factors

% Factor scores using regression  method
F = Z*inv(corr(Z))*Ar*Phi;