function blsa_plot_factor_loadings(A,V)

% Plots bar graphs of loadings, A, on factors with test labels in V.
% 
% Usage blsa_plot_factor_loadings(A,V)
%
% Created by Josh Goh 17 Dec 2010.

for i=1:size(A,2)
    subplot(1,size(A,2),i);
    H = barh(A(:,i),.5,'FaceColor',[0 .3 .7]);
    title(['F' num2str(i)],'FontName','Arial','FontWeight','bold','FontSize',14);
    set(gcf,'Color',[1 1 1]);
    set(gcf,'Position',[0,300,900,800]);
    set(gca,'XAxisLocation','top');
    set(gca,'YTick',[]');
    %set(gca,'Position',[0.05,.04,.57,.89]);
    xminmax = get(gca,'Xlim');
    yminmax = get(gca,'Ylim');
    
    axis([-1 1 0 length(V)+1]);
    xminmax = get(gca,'Xlim');
      
    hold on;
end

for i = 1:length(V)
        h(i) = text(xminmax(2)+.5,i,V{i},'FontName','Arial','FontSize',10);   
end

hold off