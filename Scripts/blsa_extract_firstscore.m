function B = blsa_extract_firstscore(C,varvec)

% Extracts first available scores from C (from
% blsa_read_csv_battery_data.m) into B, an n x m matrix compatible for
% blsa_crosscorrelations.m.
%
% Usage: B = blsa_extract_firstscore(C,varvec)
%
% C      - Structure with neuropsych data as matrices from
%          blsa_read_csv_battery.m
% varvec - 3 x 1 vector of start, end column no. of cognitive variables
%          and baseage
%
% Created by Josh Goh 03032011.

% Allocations
t1 = varvec(1);
tn = varvec(2);
ta = varvec(3);

% Extract first available scores from C into Dt.
k=1;
for t=[1 ta t1:tn] % Get id, baseage, and cognitive tests only.
    data=C.variable(1,t).data;
    for s=1:size(data,1)
        j=1;
        while isnan(data(s,j)) && j<=size(data,2)
            j=j+1;
        end
        B(s,k)=data(s,j);
    end
    k=k+1;
end