function [D S C O] = blsa_read_csv_battery_data(csvfname,db,saveflag)

% Usage: [D S C O] = blsa_read_csv_battery_data(csvfname,db,saveflag)
% 
% Read database .csv file exported from SAS into variables.
%
% csvfname  - database .csv filename.
% db        - database case no. (refer to 
%             blsa_process_battery_filesetup.m).
% saveflag  - save (1) or don't save (0; default).
% D         - output neuropsych data as a cell array
% S         - output neuropsych data as a structure (per subject, per 
%             variable, per visit)
% C         - output neuropsych data as matrices (subject x visit) within a
%             structure (per variable)
% O         - Options structure containing varvec, varnames and other plot
%             options.
%
% Created by Josh Goh 02032011.

if nargin<3
    saveflag=0;
elseif nargin<2
    error('Database needs to be specified');
end

% Specify database variables
O = blsa_read_csv_switcher(csvfname,db,'battery');
id = O.id;
D = O.D;
V = O.V;
varvec = O.varvec;
varnames = O.varnames;

% Begin reorganization
cogstart = varvec(1);
S.varnames = V;
C.varnames = V;

% Get unique subj
sid = unique(id);
nsubj = length(sid);

% Preallocations
a = 1; % Row counter
vispersid = zeros(nsubj);

% Loop visits per subj
for i=1:nsubj; % Subj loop
    
    vispersid(i) = length(find(id==sid(i)));
    S.subj(i).id = D{1,1}(a);
    
    for j=1:vispersid(i); % Visit loop  
        for k = 1:length(V); % Variable loop
            d = D{1,k}(a);
            if k>=cogstart % End demographic and start cognitive variables.
            if isempty(str2num(char(d)));
                d = NaN; % Remove text codes in data
                D{1,k}(a) = {d};
            else
                d = str2num(char(d)); % Format values as numbers
                D{1,k}(a) = {d};
            end
            end
            S.subj(i).variable(k).visvalue(j) = d;
            C.variable(k).data(i,j) = d;
            
        end
        a = a+1; % Advance one subject visit row
    end
    
end

if saveflag
save D D
save S S
save C C
end

