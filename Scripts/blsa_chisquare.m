function [chisq,df] = blsa_chisquare(COV1,COV2,N,nfe)

% Computes chi-square statistic between COV1 and COV2.
% COV1 - Covariance matrix of restricted model.
% COV2 - Covariance matrix of sample.
% N - number of subjects.
% nfe - number of free elements.

nvar = size(COV1,1);

% Determinants
dCOV1 = det(COV1);
dCOV2 = det(COV2);

chisq = (N-1)*(log(dCOV1)+trace(inv(COV1)*COV2)-log(dCOV2)-nvar);
df = .5*nvar*(nvar+1)-nfe;