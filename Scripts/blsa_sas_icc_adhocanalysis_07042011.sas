* CHECK ICC OF SCORES SANS 1ST VISIT;
* CREATE COMPZREDND2NIMIAIN1 DATABASE (VISIT 2 ONWARDS ONLY FROM COMPZREDNDNIMAIN1);
data lefslib.compzrednd2nimain1; set lefslib.compzredndnimain1; * CREATE COMPZREDND2NIMAIN1 DATABASE;
if interval > 0;
run;

* COMPZREDNDNIMAIN1;
%macro icccompzrednd2nimain1(varnames); * OPERATE ON COMPZREDND2NIMAIN1;
	%let k = 1;
	%let dv = %scan(&varnames, &k);
	%do %while ("&dv" NE "");
		PROC MIXED data=lefslib.compzrednd2nimain1 METHOD = reml covtest;
			CLASS id;
			MODEL &dv = /DDFM = kr SOLUTION;
			RANDOM intercept / TYPE=un SUBJECT=id SOLUTION; * Complete, default;
			ODS SELECT Mixed.CovParms;
			ODS OUTPUT Mixed.CovParms = lefsest.compzrednd2nimain1ICCcov&dv;
		RUN;

		* Rename variables;
		DATA lefsest.compzrednd2nimain1ICCcov&dv (RENAME = (Estimate=E&dv));
		SET lefsest.compzrednd2nimain1ICCcov&dv;
		PROC SORT data = lefsest.compzrednd2nimain1ICCcov&dv; by CovParm; RUN;
	
		* Increment;
		%let k = %eval(&k + 1);
		%let dv = %scan(&varnames, &k);
	%end;
%mend;
%icccompzrednd2nimain1(abstr cap chunk disc inhib manip pretr sretr swtch ltm stm spd vocab vis);
DATA lefslib.compzrednd2nimain1ICCest;
MERGE lefsest.compzrednd2nimain1ICCcovabstr lefsest.compzrednd2nimain1ICCcovcap lefsest.compzrednd2nimain1ICCcovchunk lefsest.compzrednd2nimain1ICCcovdisc lefsest.compzrednd2nimain1ICCcovinhib lefsest.compzrednd2nimain1ICCcovmanip lefsest.compzrednd2nimain1ICCcovpretr lefsest.compzrednd2nimain1ICCcovsretr lefsest.compzrednd2nimain1ICCcovswtch lefsest.compzrednd2nimain1ICCcovltm lefsest.compzrednd2nimain1ICCcovstm lefsest.compzrednd2nimain1ICCcovspd lefsest.compzrednd2nimain1ICCcovvocab lefsest.compzrednd2nimain1ICCcovvis;
BY CovParm;
DROP Subject StdErr ZValue ProbZ;
IF CovParm = 'UN(1,1)' THEN CovParm = 'Intercept';
RUN;
	
* Export lefslib.compzrednd2nimain1ICC*est as .csv;
PROC EXPORT data = lefslib.compzrednd2nimain1ICCest
			OUTFILE = "Z:\Research\Projects\BLSA\Battery\Data\lefslib\compzrednd2nimain1ICCest.csv" 
            DBMS=CSV REPLACE; PUTNAMES=YES;
RUN;
