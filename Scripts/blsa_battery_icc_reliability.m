function R = blsa_battery_icc_reliability(csvfname,db)

% Computes icc values for each test across all sessions based on SAS mixed
% model output.
%
% Usage: R = blsa_battery_icc_reliability(csvfname,db)
%
% csvfname - .csv output from SAS, string.
% db       - Database no.
% R        - vector of icc values.
%
% Created by Josh Goh 03030211.

% Specify database variables
O = blsa_read_csv_switcher(csvfname,db,'icc');
D = O.D;

D = cell2mat(D);
R = D(2,:)./(D(2,:)+D(1,:));