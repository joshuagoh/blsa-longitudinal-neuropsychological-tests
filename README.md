# BLSA Longitudinal Neuropsychological Tests

Repo for Goh, J. O., An, Y., & Resnick, S. M. (2012). Differential trajectories of age-related changes in components of executive and memory processes. Psychology and Aging, 27(3), 707–719. https://doi.org/10.1037/a0026715

- Only essential scripts and info are on this public repo.
- Data are maintained on private storage.

Issues might be addressed to joshuagoh@ntu.edu.tw.
